﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Kendo.Mvc;

namespace Inventory
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
			config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        [PopulateSiteMap(SiteMapName = "sample", ViewDataKey = "sample")]
        public static void RegisterMenuSiteMap()
        {
            if (!SiteMapManager.SiteMaps.ContainsKey("sample"))
            {
                SiteMapManager.SiteMaps.Register<XmlSiteMap>("sample", sitmap => sitmap.LoadFrom("~/sitemap.config"));
            }
            SiteMapBase map = SiteMapManager.SiteMaps["sample"];
        }
    }
}
