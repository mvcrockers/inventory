﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using Moq;
using Inventory.Domain;

namespace Inventory
{
    public class ControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;
        public ControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)ninjectKernel.Get(controllerType);
        }
        private void AddBindings()
        {
            new UserRepoMapping().AddBindings(ninjectKernel);
        }

    }

    
}