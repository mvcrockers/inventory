﻿using Ninject;
using Inventory.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory
{
    public class UserRepoMapping
    {
        public void AddBindings(IKernel ninjectKernel)
        {
            ninjectKernel.Bind<IUserTypeRepository>().To<EFUserTypeRepository>();
            ninjectKernel.Bind<IUserGroupRepository>().To<UserGroupRepository>();
            ninjectKernel.Bind<IPermissionRepository>().To<PermissionRepository>();
            ninjectKernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            
        }
    }
}