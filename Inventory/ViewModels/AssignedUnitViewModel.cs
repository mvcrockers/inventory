﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.ViewModels
{
    public class AssignedUnitViewModel
    {
        public int UnitID { get; set; }
        public string Name { get; set; }
        public bool Assigned { get; set; }
    }
}