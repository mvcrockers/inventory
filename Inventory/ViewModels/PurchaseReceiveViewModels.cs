﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Inventory.Domain;

namespace Inventory.WebUI
{
    public class DirectPurchaseReceiveViewModel
    {

        public int Id { get;set; }

        [Required]
        [Display(Name = "Purchase No")]
        public string PurchaseNo { get; set; }

        [Required]
        public int SupplierId { get; set; }

        [Required]
        [Display(Name = "Receive as")]
        public ReceivedStockTypeEnum ReceivedStockType { get; set; }

        [Required]
        public DateTime ReceiveDate { get; set; }

        public string Remarks { get; set; }
    }

    //public class PurchaseReceiveItemViewModel
    //{
    //    public long Id { get; set; }

    //    public int index { get; set; }

    //    [Required]
    //    public long ItemId { get; set; }

    //    [Required]
    //    public long UnitOfMeasureId { get; set; }

    //    [Required]
    //    public long ItemQty { get; set; }

    //    public string ItemName { get; set; }

    //    public string UnitName { get; set; }
    //    public decimal ConversionFactor { get; set; }

    //}

    public class ItemComboViewModel
    {
        public long ItemId { get; set; }

        public string ItemName { get; set; }
    }
}