﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.WebUI
{
    public class UpdateUserViewModel
    {

        public int UserId
        {
            get;
            set;
        }

        [Required]
        [Display(Name = "User name")]
        public string UserName
        {
            get;
            set;
        }

        [Required]
        [Display(Name = "Email address")]
        //[DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string EmailAddress
        {
            get;
            set;
        }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword
        {
            get;
            set;
        }

        [Display(Name = "First name")]
        public string FirstName
        {
            get;
            set;
        }

        [Display(Name = "Last name")]
        public string LastName
        {
            get;
            set;
        }

        [Required]
        [Display(Name = "Role")]
        public int RoleId
        {
            get;
            set;
        }

        public DateTime TestDate
        {
            get;
            set;
        }
    }

    public class RoleViewModel
    {
        public int RoleId
        {
            get;
            set;
        }

        public string RoleName
        {
            get;
            set;
        }
    }
}