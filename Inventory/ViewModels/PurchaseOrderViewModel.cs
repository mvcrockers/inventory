﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Inventory.Domain;

namespace Inventory.WebUI
{
    public class PurchaseOrderViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Order Code")]
        public string OrderCode { get; set; }

        [Required]
        [Display(Name = "Order Date")]
        public DateTime OrderDate { get; set; }

        [Display(Name = "Received Date")]
        public DateTime? ReceivedDate { get; set; }

        public Boolean IsReceived { get; set; }

        public virtual List<PurchaseOrderItemViewModel> PurchaseOrderItems { get; set; }
    }

    public class PurchaseOrderItemViewModel
    {
        public long Id { get; set; }

        public long PurchaseOrderId { get; set; }

        public int Index { get; set; }

        [Required]
        public int ItemId { get; set; }

        [Required]
        public int UnitOfMeasureId { get; set; }

        [Required]
        public long ItemQty { get; set; }

        public string ItemName { get; set; }

        public string UnitName { get; set; }
        public decimal ConversionFactor { get; set; }

        [Required]
        public int SupplierId { get; set; }

        public string SupplierName { get; set; }
    }

    public class SupplierComboViewModel
    {
        public int SupplierId { get; set; }

        public string SupplierName { get; set; }
    }

    public class VMModelMapper
    {
        public PurchaseOrder MapPurchaseOrderVM(PurchaseOrderViewModel vm)
        {
            PurchaseOrder purchaseOrder = new PurchaseOrder();

            if (vm != null)
            {
                purchaseOrder.ID = vm.Id;
                purchaseOrder.OrderCode = vm.OrderCode;
                purchaseOrder.OrderDate = vm.OrderDate;
                purchaseOrder.IsReceived = vm.IsReceived;
                purchaseOrder.ReceivedDate = vm.IsReceived ? vm.ReceivedDate : null;

                purchaseOrder.PurchaseOrderItems = this.MapPurchaseOrderItemFromVM(vm.PurchaseOrderItems);
            }

            return purchaseOrder;
        }

        public List<PurchaseOrderItem> MapPurchaseOrderItemFromVM(List<PurchaseOrderItemViewModel> items)
        {
            if (items != null && items.Count > 0)
            {
                List<PurchaseOrderItem> purchaseOrderItems = new List<PurchaseOrderItem>();

                foreach (var item in items)
                {
                    PurchaseOrderItem pItem = new PurchaseOrderItem();
                    pItem.ID = item.Id;
                    pItem.PurchaseOrderID = item.PurchaseOrderId;
                    pItem.SupplierID = item.SupplierId;
                    pItem.ItemID = item.ItemId;
                    pItem.ItemQty = (int)item.ItemQty;
                    pItem.UnitOfMeasureID = item.UnitOfMeasureId;
                    pItem.ConversionFactor = item.ConversionFactor;
                    purchaseOrderItems.Add(pItem);
                }

                return purchaseOrderItems;
            }

            return null;
        }

        public List<PurchaseOrderItemViewModel> MapPurchaseOrderItemToVM(ICollection<PurchaseOrderItem> items)
        {
            if (items != null && items.Count > 0)
            {
                List<PurchaseOrderItemViewModel> pItems = new List<PurchaseOrderItemViewModel>();

                foreach (var item in items)
                {
                    PurchaseOrderItemViewModel pItem = new PurchaseOrderItemViewModel();

                    pItem.Id = item.ID;
                    pItem.PurchaseOrderId = item.PurchaseOrderID;
                    pItem.ItemId = item.ItemID;
                    pItem.ItemQty = item.ItemQty;
                    pItem.SupplierId = item.SupplierID;
                    pItem.ConversionFactor = item.ConversionFactor;
                    pItem.UnitOfMeasureId = item.UnitOfMeasureID;

                    pItems.Add(pItem);
                }

                return pItems;
            }

            return null;
        }
    }
}