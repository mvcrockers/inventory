﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.WebUI
{
    public class DateTimeViewModel
    {
        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }
    }
}