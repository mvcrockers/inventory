﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventory.Domain;

namespace Inventory.Controllers
{
    [Authorize]
    public class UnitOfMeasureController : Controller
    {
        private UnitOfWork _unitOfWork;

        public UnitOfMeasureController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: /UnitOfMeasure/
        public ActionResult Index()
        {
            return View(_unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures().ToList());
        }

        // GET: /UnitOfMeasure/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnitOfMeasure unitofmeasure = _unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasureByID(id.Value);
            if (unitofmeasure == null)
            {
                return HttpNotFound();
            }
            return View(unitofmeasure);
        }

        // GET: /UnitOfMeasure/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /UnitOfMeasure/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name")] UnitOfMeasure unitofmeasure)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.UnitOfMeasureRepository.InsertUnitOfMeasure(unitofmeasure);
                    _unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message + " Unable to save data!");
            }
            return View();
        }

        // GET: /UnitOfMeasure/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UnitOfMeasure uom = _unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasureByID(id.Value);

            if (uom == null)
            {
                return HttpNotFound();
            }
            return View(uom);
        }

        // POST: /UnitOfMeasure/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name")] UnitOfMeasure unitofmeasure)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.UnitOfMeasureRepository.UpdateUnitOfMeasure(unitofmeasure);
                    _unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message + " Unable to save changes!");
            }
            return View(unitofmeasure);
        }

        // GET: /UnitOfMeasure/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UnitOfMeasure unitofmeasure = _unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasureByID(id.Value);
            if (unitofmeasure == null)
            {
                return HttpNotFound();
            }
            return View(unitofmeasure);
        }

        // POST: /UnitOfMeasure/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _unitOfWork.UnitOfMeasureRepository.DeleteUnitOfMeasure(id);
            _unitOfWork.Save();
            return RedirectToAction("Index");
        }

        public JsonResult GetItemWiseUnits(string ItemId)
        {
            long longItemId = Convert.ToInt64(ItemId);
            Item sItem = _unitOfWork.ItemRepository.GetItemByID((int)longItemId);

            var sUOMs = _unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures()
                                   .Where(x => (sItem.LargerUnitId!=null && x.Id == sItem.LargerUnitId.Value) || x.Id == sItem.SmallerUnitId)
                                   .Select(x => new { Id = x.Id, Name = x.Name })
                                   .ToList();

            return Json(sUOMs);
        }
    }
}
