﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inventory.Domain;

namespace Inventory.Controllers
{
    [Authorize]
    public class UserTypeController : Controller
    {
        public IUserTypeRepository UserTypeRepository;
        public UserTypeController(IUserTypeRepository userTypeRepository)
        {
            this.UserTypeRepository = userTypeRepository;
        }
        //
        // GET: /UserType/
        public ActionResult Index()
        {  
            return View(UserTypeRepository.UserTypes);
        }
	}
}