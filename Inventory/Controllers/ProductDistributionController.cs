﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventory.Domain;
using Inventory.WebUI;
using Microsoft.Reporting.WebForms;
using System.IO;

namespace Inventory.Controllers
{
    [Authorize]
    public class ProductDistributionController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private List<Unit> _units;
        private List<Department> _departments;
        public ProductDistributionController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        // GET: /ProductDistribution/
        public ActionResult Index(DateTime? fromDate, DateTime? toDate)
        {
            List<ProductDistribution> distributions;

            if ((fromDate.HasValue && fromDate != DateTime.MinValue) && (toDate.HasValue && toDate != DateTime.MinValue))
            {
                distributions = _unitOfWork.ProductDistributionRepository.GetProductDistributions()
                    .Where(x => x.TranDate >= fromDate.Value && x.TranDate <= toDate.Value).ToList();
            }
            else
            {
                distributions = _unitOfWork.ProductDistributionRepository.GetProductDistributions()
                    .Where(x => x.TranDate == DateTime.Today).ToList();
            }
            distributions.ForEach(x => x.TotalAmount = x.ProductDistributionItems.Sum(item => item.TotalPrice));
            
            return View(distributions);
        }

        public ActionResult Report(string id)
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "Distribution.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }

            InventoryDataSet InDataSet = new InventoryDataSet();

            InventoryDataSetTableAdapters.spDistributionTableAdapter InTableAdapter =
                new InventoryDataSetTableAdapters.spDistributionTableAdapter();

            InTableAdapter.Fill(InDataSet.spDistribution,null,null,null,null,null, null);

            ReportDataSource rd = new ReportDataSource("Distribution", InDataSet.Tables["spDistribution"]);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtention;

            string deviceInfo =
                    "<DeviceInfo>" +
                    "<OutputFormat>" + id + "</OutputFormat>" +
                    "<PageWidth>8.5in</PageWidth>" +
                    "<PageHeight>11in</PageHeight>" +
                    "<MarginTop>0.5in</MarginTop>" +
                    "<MarginLeft>1in</MarginLeft>" +
                    "<MarginRight>1in</MarginRight>" +
                    "<MarginBottom>0.5in</MarginBottom>" +
                    "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderredBytes;

            renderredBytes = lr.Render(reportType,
                                deviceInfo,
                                out mimeType,
                                out encoding,
                                out fileNameExtention,
                                out streams,
                                out warnings);


            return File(renderredBytes, mimeType);
        }

        // GET: /ProductDistribution/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductDistribution oProductDistribution = _unitOfWork.ProductDistributionRepository.GetProductDistributionByID(id.Value);

            if (oProductDistribution == null)
            {
                return HttpNotFound();
            }


            _units = this._unitOfWork.UnitRepository.GetUnits().ToList();
            List<Department> depts = _unitOfWork.DepartmentRepository.GetDepartments().ToList();
            List<Item> ItemList = _unitOfWork.ItemRepository.GetItems(string.Empty).ToList();
            List<UnitOfMeasure> UOMList = _unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures().ToList();

            int count = 1;
            foreach (var item in oProductDistribution.ProductDistributionItems)
            {
                item.index = count++;
                item.ItemName = ItemList.First(x => x.Id == item.ItemId).Name;
                item.UnitMessureName = UOMList.First(x => x.Id == item.UnitOfMeasureID).Name;
                item.UnitName = _units.First(x => x.Id == item.UnitID).Name;
                item.DeptName = item.DepartmentID != null ? depts.First(x => x.Id == item.DepartmentID.Value).Name : "";
            }

            ViewBag.ProductDistributionItems = oProductDistribution.ProductDistributionItems.ToList();

            return View(oProductDistribution);
        }

        // GET: /ProductDistribution/Create
        public ActionResult Create()
        {
            _units = this._unitOfWork.UnitRepository.GetUnits().ToList();
            //_departments = this._unitOfWork.DepartmentRepository.GetDepartments().ToList();

            ViewBag.UnitId = new SelectList(_units, "Id", "Name");
            ViewBag.DepartmentId = new SelectList(new List<Department>(), "Id", "Name");

            ViewBag.Items = _unitOfWork.ItemRepository.GetItems(string.Empty).Select(x => new ItemComboViewModel() { ItemId = x.Id, ItemName = x.Name });

            Session["ProductDistributionItems"] = ViewBag.ProductDistributionItems = new List<ProductDistributionItem>();

            return View();
        }

        public JsonResult GetDeptByUnit(int unitId)
        {
            var departments = this._unitOfWork.UnitRepository.GetUnits().Where(u => u.Id == unitId).FirstOrDefault().Departments.ToList();
            var obj = Json(new SelectList(departments, "Id", "Name"), JsonRequestBehavior.AllowGet);
            return obj;
        }

        // POST: /ProductDistribution/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductDistribution productdistribution)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    productdistribution.ProductDistributionItems = Session["ProductDistributionItems"] as List<ProductDistributionItem>;
                    Session["ProductDistributionItems"] = null;
                    Save(productdistribution);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _unitOfWork.RollBackTransaction();
                    ModelState.AddModelError("", ex.Message);
                }

            }

            _units = this._unitOfWork.UnitRepository.GetUnits().ToList();
            //_departments = this._unitOfWork.DepartmentRepository.GetDepartments().ToList();

            ViewBag.UnitId = new SelectList(_units, "Id", "Name");
            ViewBag.DepartmentId = new SelectList(new List<Department>(), "Id", "Name");

            ViewBag.Items = _unitOfWork.ItemRepository.GetItems(string.Empty).Select(x => new ItemComboViewModel() { ItemId = x.Id, ItemName = x.Name });

            ViewBag.ProductDistributionItems = Session["ProductDistributionItems"] as List<ProductDistributionItem>;


            return View(productdistribution);
        }

        private void Save(ProductDistribution productdistribution)
        {
            _unitOfWork.BeginTransaction();
            ICollection<StockTranItem> stranItems = new List<StockTranItem>();
            ProductDistribution savedModel = _unitOfWork.ProductDistributionRepository.InsertProductDistribution(productdistribution);

            foreach (var item in savedModel.ProductDistributionItems)
            {
                stranItems.Add(new StockTranItem()
                {
                    ParentTranID = item.ProductDistributionId,
                    ParentTranNo = savedModel.InvoiceNo,
                    Trandate = savedModel.TranDate,
                    TranID = savedModel.ID,
                    TransactionType = TransactionTypeEnum.Distrution,
                    StockId = item.StockID,
                    QuantityInSmallerUnit = item.Quantity * item.ConversionFactor,
                    TranSide = TranSideEnum.Decrease
                });
            }

            _unitOfWork.StockRepository.Insert(stranItems);

            _unitOfWork.Save();

            _unitOfWork.CommitTransaction();
        }
        // GET: /ProductDistribution/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductDistribution productdistribution = _unitOfWork.ProductDistributionRepository.GetProductDistributionByID(id.Value);

            if (productdistribution == null)
            {
                return HttpNotFound();
            }

            _units = this._unitOfWork.UnitRepository.GetUnits().ToList();
            //_departments = this._unitOfWork.DepartmentRepository.GetDepartments().ToList();
            List<Department> depts = _unitOfWork.DepartmentRepository.GetDepartments().ToList();

            //            Session["ProductDistributionItems"] = ViewBag.ProductDistributionItems = new List<ProductDistributionItem>();


            List<Item> ItemList = _unitOfWork.ItemRepository.GetItems(string.Empty).ToList();
            List<UnitOfMeasure> UOMList = _unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures().ToList();

            ViewBag.UnitId = new SelectList(_units, "Id", "Name");
            ViewBag.DepartmentId = new SelectList(new List<Department>(), "Id", "Name");

            ViewBag.Items = ItemList.Select(x => new ItemComboViewModel() { ItemId = x.Id, ItemName = x.Name });

            int count = 1;

            List<ProductDistributionItem> PurchaseReceiveList = new List<ProductDistributionItem>();
            foreach (var item in productdistribution.ProductDistributionItems)
            {
                var selectedItem = ItemList.First(x => x.Id == item.ItemId);
                ProductDistributionItem newProductDistributionItem = new ProductDistributionItem();
                newProductDistributionItem.ID = item.ID;
                newProductDistributionItem.ItemId = item.ItemId;
                newProductDistributionItem.Quantity = item.Quantity;
                //PurchaseReceiveId = item.PurchaseReceiveId,
                newProductDistributionItem.UnitOfMeasureID = item.UnitOfMeasureID;
                newProductDistributionItem.ConversionFactor = item.ConversionFactor;
                newProductDistributionItem.index = count++;
                newProductDistributionItem.ItemName = selectedItem.Name;
                newProductDistributionItem.UnitMessureName = UOMList.First(x => x.Id == item.UnitOfMeasureID).Name;
                newProductDistributionItem.UnitName = _units.First(x => x.Id == item.UnitID).Name;
                newProductDistributionItem.DeptName = item.DepartmentID != null ? depts.First(x => x.Id == item.DepartmentID.Value).Name : "";
                newProductDistributionItem.UnitID = item.UnitID;
                newProductDistributionItem.DepartmentID = item.DepartmentID;
                newProductDistributionItem.StockID = item.StockID;
                newProductDistributionItem.UnitPrice = item.UnitPrice;
                newProductDistributionItem.TotalPrice = item.TotalPrice;
                PurchaseReceiveList.Add(newProductDistributionItem);

            }


            Session["ProductDistributionItems"] = ViewBag.ProductDistributionItems = PurchaseReceiveList;

            // = new List<PurchaseReceiveItem>();

            return View(productdistribution);
        }

        // POST: /ProductDistribution/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductDistribution productdistribution)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    productdistribution.ProductDistributionItems = Session["ProductDistributionItems"] as List<ProductDistributionItem>;

                    foreach (var item in productdistribution.ProductDistributionItems)
                    {
                        item.ProductDistributionId = productdistribution.ID;
                    }

                    ICollection<StockTranItem> stranItems = new List<StockTranItem>();



                    _unitOfWork.BeginTransaction();

                    ProductDistribution savedModel = _unitOfWork.ProductDistributionRepository.UpdateProductDistribution(productdistribution);

                    foreach (var item in savedModel.ProductDistributionItems)
                    {
                        stranItems.Add(new StockTranItem()
                        {
                            ParentTranID = (Int32)savedModel.ID,
                            ParentTranNo = savedModel.InvoiceNo,
                            QuantityInSmallerUnit = item.Quantity * item.ConversionFactor,
                            StockId = item.StockID,
                            Trandate = savedModel.TranDate,
                            TranID = (int)item.ID,
                            TransactionType = TransactionTypeEnum.Distrution,
                            TranSide = TranSideEnum.Decrease
                        });
                    }

                    _unitOfWork.StockRepository.Update(stranItems);

                    _unitOfWork.Save();

                    _unitOfWork.CommitTransaction();
                    Session["ProductDistributionItems"] = null;
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _unitOfWork.RollBackTransaction();
                    ModelState.AddModelError("", ex.Message);
                }

            }

            _units = this._unitOfWork.UnitRepository.GetUnits().ToList();
            ViewBag.UnitId = new SelectList(_units, "Id", "Name");
            ViewBag.DepartmentId = new SelectList(new List<Department>(), "Id", "Name");

            ViewBag.Items = _unitOfWork.ItemRepository.GetItems(string.Empty).Select(x => new ItemComboViewModel() { ItemId = x.Id, ItemName = x.Name });

            ViewBag.ProductDistributionItems = Session["ProductDistributionItems"] as List<ProductDistributionItem>;


            return View(productdistribution);
        }

        // GET: /ProductDistribution/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductDistribution oProductDistribution = _unitOfWork.ProductDistributionRepository.GetProductDistributionByID(id.Value);

            if (oProductDistribution == null)
            {
                return HttpNotFound();
            }


            _units = this._unitOfWork.UnitRepository.GetUnits().ToList();
            List<Department> depts = _unitOfWork.DepartmentRepository.GetDepartments().ToList();
            List<Item> ItemList = _unitOfWork.ItemRepository.GetItems(string.Empty).ToList();
            List<UnitOfMeasure> UOMList = _unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures().ToList();

            int count = 1;
            foreach (var item in oProductDistribution.ProductDistributionItems)
            {
                item.index = count++;
                item.ItemName = ItemList.First(x => x.Id == item.ItemId).Name;
                item.UnitMessureName = UOMList.First(x => x.Id == item.UnitOfMeasureID).Name;
                item.UnitName = _units.First(x => x.Id == item.UnitID).Name;
                item.DeptName = item.DepartmentID != null ? depts.First(x => x.Id == item.DepartmentID.Value).Name : "";
            }

            ViewBag.ProductDistributionItems = oProductDistribution.ProductDistributionItems.ToList();

            return View(oProductDistribution);
        }

        // POST: /ProductDistribution/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                ProductDistribution rcv = _unitOfWork.ProductDistributionRepository.GetProductDistributionByID(id);
                _unitOfWork.BeginTransaction();
                _unitOfWork.ProductDistributionRepository.DeleteProductDistribution(id);
                _unitOfWork.Save();
                _unitOfWork.StockRepository.Delete(TransactionTypeEnum.Distrution, id);
                _unitOfWork.Save();
                _unitOfWork.CommitTransaction();

            }
            catch (Exception ex)
            {
                _unitOfWork.RollBackTransaction();
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult DistributeFromPurchase(string billNo, string UnitId, string DeptId)
        {
            JsonResult result = new JsonResult();
            result.Data = "Error Distribution";

            PurchaseReceive oPurchaseReceive =
                _unitOfWork.PurchaseReceiveRepository.GetPurchaseReceives().Where(x => x.PurchaseNo == billNo.Trim()).FirstOrDefault();

            ProductDistribution oProductDist = new ProductDistribution();

            oProductDist.InvoiceNo = oPurchaseReceive.PurchaseNo;
            oProductDist.TranDate = oPurchaseReceive.ReceiveDate;
            oProductDist.Remarks = oPurchaseReceive.Remarks;

            foreach (PurchaseReceiveItem purchaseItem in oPurchaseReceive.PurchaseReceiveItems)
            {
                ProductDistributionItem distItem = new ProductDistributionItem();

                distItem.UnitID = Convert.ToInt32(UnitId);
                if (DeptId != null)
                    distItem.DepartmentID = Convert.ToInt32(DeptId);

                distItem.StockID = purchaseItem.StockID;

                Stock Stock = _unitOfWork.StockRepository.GetStock(distItem.StockID);
                distItem.ItemId = Stock.ItemId;
                distItem.Quantity = purchaseItem.ItemQty;
                distItem.UnitOfMeasureID = purchaseItem.UnitOfMeasureId;
                distItem.ConversionFactor = purchaseItem.ConversionFactor;
                distItem.UnitPrice = purchaseItem.UnitPrice;
                distItem.TotalPrice = purchaseItem.Price;

                oProductDist.ProductDistributionItems.Add(distItem);
            }

            try
            {
                Save(oProductDist);
                result.Data="Distributed Successfully";
            }
            catch (Exception ex)
            {
                _unitOfWork.RollBackTransaction();
                ModelState.AddModelError("", ex.Message);
            }
            
            return result;
        }

        public JsonResult AddProductDistributionItem(string UnitId, string DeptId, string ItemId, string uomId, string qty, string UnitName, string DeptName, string StockID)
        {
            ProductDistributionItem ItemModel = new ProductDistributionItem();
            List<ProductDistributionItem> ItemModels = Session["ProductDistributionItems"] as List<ProductDistributionItem>;
            ItemModel.index = ItemModels.Count + 1;
            ItemModel.ItemId = Convert.ToInt32(ItemId);
            ItemModel.UnitName = UnitName;
            ItemModel.DeptName = DeptName;
            ItemModel.UnitID = Convert.ToInt32(UnitId);
            if (DeptId != null)
                ItemModel.DepartmentID = Convert.ToInt32(DeptId);


            ItemModel.index = ItemModels.Count + 1;
            ItemModel.StockID = Convert.ToInt32(StockID);
            ItemModel.Quantity = Convert.ToInt64(qty);
            ItemModel.UnitOfMeasureID = Convert.ToInt32(uomId);
            
            Stock Stock = _unitOfWork.StockRepository.GetStock(ItemModel.StockID);
            ItemModel.ConversionFactor = (Stock.LargerUnitId.HasValue && ItemModel.UnitOfMeasureID == Stock.LargerUnitId.Value) ? Stock.ConversionFactor.Value : 1;
            ItemModel.UnitPrice = Helper.CalculateUnitPrice(ItemModel.UnitOfMeasureID, Stock);
            ItemModel.TotalPrice = ItemModel.UnitPrice * ItemModel.Quantity;
           
          

            Item oItem = _unitOfWork.ItemRepository.GetItemByID(ItemModel.ItemId);
            UnitOfMeasure oUOM = _unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasureByID(ItemModel.UnitOfMeasureID);
            ItemModel.ItemName = oItem.Name;
            ItemModel.UnitMessureName = oUOM.Name;

            ItemModels.Add(ItemModel);

            Session["ProductDistributionItems"] = ItemModels;

            ProductDistributionItem viewModel = new ProductDistributionItem() 
                                                        {
                                                            index=ItemModel.index,
                                                            UnitName=ItemModel.UnitName,
                                                            DeptName=ItemModel.DeptName,
                                                            ItemName=ItemModel.ItemName,
                                                            Quantity=ItemModel.Quantity,
                                                            UnitMessureName=ItemModel.UnitMessureName,
                                                            TotalPrice=ItemModel.TotalPrice
                                                        };

            return Json(viewModel);
        }
        public JsonResult DeleteProductDistributionItem(string Index)
        {
            List<ProductDistributionItem> ItemModels = Session["ProductDistributionItems"] as List<ProductDistributionItem>;
            var deletedVModel = ItemModels.SingleOrDefault(x => x.index == Convert.ToInt32(Index));
            if (deletedVModel != null)
                ItemModels.Remove(deletedVModel);

            int count = 1;
            ItemModels.ForEach(x => x.index = (count++));

            Session["ProductDistributionItems"] = ItemModels;


            List<ProductDistributionItem> viewModels = new List<ProductDistributionItem>();
            ItemModels.ForEach(ItemModel =>

                                viewModels.Add(new ProductDistributionItem()
                                                    {
                                                        index = ItemModel.index,
                                                        UnitName = ItemModel.UnitName,
                                                        DeptName = ItemModel.DeptName,
                                                        ItemName = ItemModel.ItemName,
                                                        Quantity = ItemModel.Quantity,
                                                        UnitMessureName = ItemModel.UnitMessureName,
                                                        TotalPrice = ItemModel.TotalPrice
                                                    }
                                                )
                                );
            return Json(viewModels);
        }

        public JsonResult GetItemWiseStocks(string ItemId)
        {
            var sStocks = _unitOfWork.StockRepository.GetStocks(Convert.ToInt32(ItemId)).ToList();
            return Json(sStocks);
        }
    }
}
