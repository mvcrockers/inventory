﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventory.Domain;
using Inventory.WebUI;
using Microsoft.Reporting.WebForms;
using System.IO;

namespace Inventory.Controllers
{
    [Authorize]
    public class UserManageController : Controller
    {
        private IUnitOfWork unitOfWork;
        private List<Role> roles;

        public UserManageController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: /UserManage/
        public ActionResult Index()
        {
            //return "Not Implemented";
            return View(unitOfWork
                        .UserRepository
                        .GetUsers()
                        // This Where clause discards the current logged in SuperUser
                        .Where(x=>x.Username.Trim().ToLower()!= User.Identity.Name.Trim().ToLower())
                        .ToList());
            
        }

        // GET: /UserManage/Details/5
        public ActionResult Details(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = unitOfWork.UserRepository.GetUserByID(id.Value);
            if(user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: /UserManage/Create
        public ActionResult Create()
        {
            // return "Not Implemented";
            roles = unitOfWork.RoleRepository.GetRoles().ToList();
            ViewBag.RoleId = new SelectList(roles, "RoleId", "RoleName", string.Empty);
            ViewBag.RoleList = roles.Select(x => new RoleViewModel()
            {
                RoleId = x.RoleId,
                RoleName = x.RoleName
            });
            return View();
        }

        // POST: /UserManage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UpdateUserViewModel userViewModel)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    User user = new User();

                    user.Username = userViewModel.UserName.Trim();
                    user.FirstName = userViewModel.FirstName;
                    user.LastName = userViewModel.LastName;
                    user.Email = userViewModel.EmailAddress.Trim();
                    if(!string.IsNullOrEmpty(userViewModel.Password))
                        user.Password = userViewModel.Password.Trim();
                    user.Roles = new List<Role>();
                    user.Roles.Add(new Role()
                    {
                        RoleId = userViewModel.RoleId
                    });

                    unitOfWork.UserRepository.InsertUser(user);
                    unitOfWork.Save();
                    return RedirectToAction("Index");

                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            //ViewBag.RoleList = roles;

            //if(userViewModel.RoleId!=0)
            roles = unitOfWork.RoleRepository.GetRoles().ToList();
            ViewBag.RoleId = new SelectList(roles, "RoleId", "RoleName", userViewModel.RoleId);
            ViewBag.RoleList = roles.Select(x => new RoleViewModel()
            {
                RoleId = x.RoleId,
                RoleName = x.RoleName
            });
            //else
            //    ViewBag.RoleId = new SelectList(roles, "RoleId", "RoleName");
            return View(userViewModel);
        }

        // GET: /UserManage/Edit/5
        public ActionResult Edit(int? id)
        {
           //  return "Not Implemented";
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = unitOfWork.UserRepository.GetUserByID(id.Value);

            if(user == null)
            {
                return HttpNotFound();
            }

            UpdateUserViewModel UUVModel = new UpdateUserViewModel();
            UUVModel.UserId = user.UserId;
            UUVModel.UserName = user.Username;
            UUVModel.FirstName = user.FirstName;
            UUVModel.LastName = user.LastName;
            UUVModel.EmailAddress = user.Email;
            if(user.Roles.Count > 0)
                UUVModel.RoleId = user.Roles.ToList()[0].RoleId;

            roles = unitOfWork.RoleRepository.GetRoles().ToList();
            ViewBag.RoleId = new SelectList(roles, "RoleId", "RoleName", UUVModel.RoleId);

            ViewBag.RoleList = roles.Select(x => new RoleViewModel()
            {
                                RoleId = x.RoleId, RoleName = x.RoleName });
            
            return View(UUVModel);
        }

        // POST: /UserManage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UpdateUserViewModel userViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    User user = new User();

                    user.UserId = userViewModel.UserId;
                    user.Username = userViewModel.UserName.Trim();
                    user.FirstName = userViewModel.FirstName;
                    user.LastName = userViewModel.LastName;
                    user.Email = userViewModel.EmailAddress.Trim();
                    if(!string.IsNullOrEmpty(userViewModel.Password))
                        user.Password =  userViewModel.Password.Trim();
                    user.Roles = new List<Role>();
                    user.Roles.Add(new Role()
                    {
                        RoleId = userViewModel.RoleId
                    });

                    unitOfWork.UserRepository.UpdateUser(user);
                    unitOfWork.Save();
                    return RedirectToAction("Index");

                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            //ViewBag.RoleList = roles;
            
            //if(userViewModel.RoleId!=0)
            roles = unitOfWork.RoleRepository.GetRoles().ToList();
            ViewBag.RoleId = new SelectList(roles, "RoleId", "RoleName", userViewModel.RoleId);
            
            //else
            //    ViewBag.RoleId = new SelectList(roles, "RoleId", "RoleName");
            return View(userViewModel);
        }

        // GET: /UserManage/Delete/5
        public ActionResult Delete(int? id)
        {
            //return "Not Implemented";
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = unitOfWork.UserRepository.GetUserByID(id.Value);
            
            if(user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: /UserManage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //User user = unitOfWork.UserRepository.GetUserByID(id);
            unitOfWork.UserRepository.DeleteUser(id);
            unitOfWork.Save();
            return RedirectToAction("Index");
        }

       // public JsonResult GetRoles()
       //{
       //     var Testroles = unitOfWork.RoleRepository.GetRoles().Select(x=> new Role{
       //          RoleId = x.RoleId,
       //          RoleName = x.RoleName,
       //          Description = x.Description
       //     });
       //     return Json(Testroles, JsonRequestBehavior.AllowGet);
       // }

        public ActionResult Report(string id)
        {
            List<User> oUsers = new List<User>();
            oUsers = unitOfWork.UserRepository.GetUsers().ToList();

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "rptUsers.rdlc");
            if(System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index",oUsers);
            }

            
            ReportDataSource rd = new ReportDataSource("MyDataSet", oUsers);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtention;

            string deviceInfo =
                    "<DeviceInfo>"+
                    "<OutputFormat>"+ id +"</OutputFormat>"+
                    "<PageWidth>8.5in</PageWidth>" +
                    "<PageHeight>11in</PageHeight>" +
                    "<MarginTop>0.5in</MarginTop>" +
                    "<MarginLeft>1in</MarginLeft>" +
                    "<MarginRight>1in</MarginRight>" +
                    "<MarginBottom>0.5in</MarginBottom>" +  
                    "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderredBytes;

            renderredBytes = lr.Render(reportType,
                                deviceInfo,
                                out mimeType,
                                out encoding,
                                out fileNameExtention,
                                out streams,
                                out warnings);


            return File(renderredBytes, mimeType);
        }

        protected override void Dispose(bool disposing)
        {
            //if (disposing)
            //{
                unitOfWork.Dispose();
            //}
            base.Dispose(disposing);
        }
    }
}
