﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventory.Domain;

namespace Inventory.Controllers
{
    [Authorize]
    public class MenuController : Controller
    {
        private IUnitOfWork unitOfWork;

        public MenuController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public IList<Menu> GetCustomNodeList()
        {
            List<Menu> nodes = new List<Menu>();
            nodes.Add(new Menu() { MenuId = 11, MenuName = "Emp01", URI = "#", ChildMenuItems = null });
            nodes.Add(new Menu() { MenuId = 12, MenuName = "Emp02", URI = "#", ChildMenuItems = null });
            nodes.Add(new Menu() { MenuId = 13, MenuName = "Emp03", URI = "#", ChildMenuItems = null });

            return nodes;
        }

        public Menu GetCustomNode()
        {
            return new Menu() { MenuId = 1, MenuName = "EMPN01", URI = "#", ChildMenuItems = GetCustomNodeList() };
        }

        // GET: /UserManage/
        public ViewResult Index()
        {
            List<Menu> model = new List<Menu>();
            model.Add(GetCustomNode());
            model.Add(GetCustomNode());
            model.Add(GetCustomNode());
            return View(model);
            
        }

        // GET: /UserManage/Details/5
        public string Details(int? id)
        {
            return "Not Implemented";
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //User user = db.Users.Find(id);
            //if (user == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(user);
        }

        // GET: /UserManage/Create
        public string Create()
        {
            return "Not Implemented";
            //return View();
        }

        // POST: /UserManage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="UserId,Username,Email,Password,FirstName,LastName,Comment,IsApproved,PasswordFailuresSinceLastSuccess,LastPasswordFailureDate,LastActivityDate,LastLockoutDate,LastLoginDate,ConfirmationToken,CreateDate,IsLockedOut,LastPasswordChangedDate,PasswordVerificationToken,PasswordVerificationTokenExpirationDate")] User user)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.UserRepository.InsertUser(user);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: /UserManage/Edit/5
        public string Edit(int? id)
        {
            return "Not Implemented";
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //User user = db.Users.Find(id);
            //if (user == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(user);
        }

        // POST: /UserManage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="UserId,Username,Email,Password,FirstName,LastName,Comment,IsApproved,PasswordFailuresSinceLastSuccess,LastPasswordFailureDate,LastActivityDate,LastLockoutDate,LastLoginDate,ConfirmationToken,CreateDate,IsLockedOut,LastPasswordChangedDate,PasswordVerificationToken,PasswordVerificationTokenExpirationDate")] User user)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.UserRepository.UpdateUser(user);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: /UserManage/Delete/5
        public string Delete(int? id)
        {
            return "Not Implemented";
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //User user = db.Users.Find(id);
            //if (user == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(user);
        }

        // POST: /UserManage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //User user = unitOfWork.UserRepository.GetUserByID(id);
            unitOfWork.UserRepository.DeleteUser(id);
            unitOfWork.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            //if (disposing)
            //{
                unitOfWork.Dispose();
            //}
            base.Dispose(disposing);
        }
    }
}
