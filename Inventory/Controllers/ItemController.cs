﻿using Inventory.Domain;
using Inventory.WebUI;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Inventory.Controllers
{
    [Authorize]
    public class ItemController : Controller
    {
        private IUnitOfWork _unitOfWork;
        IEnumerable<SelectListItem>  ItemTypeList;
        IEnumerable<EnumItemType> itemTypes;
        public ItemController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

            itemTypes = Enum.GetValues(typeof(EnumItemType)).Cast<EnumItemType>();

            ItemTypeList = from types in itemTypes
                                select new SelectListItem
                                {
                                    Text = types.ToString(),
                                    Value = ((int)types).ToString()
                                };
        }

        // GET: /Item/
        public ActionResult Index(string searchString)
        {
            var items = _unitOfWork.ItemRepository.GetItems(searchString).Include(i => i.ItemCategory).Include(i => i.LargerUnit).Include(i => i.SmallerUnit).OrderBy(x=>x.Name);
            return View(items.ToList());
        }

        // GET: /Item/Details/5


        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = _unitOfWork.ItemRepository.GetItemByID(id.Value);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // GET: /Item/Create
        public ActionResult Create()
        {
            ViewBag.ItemType = new SelectList(ItemTypeList, "Value", "Text");
            ViewBag.ItemCategoryId = new SelectList(_unitOfWork.ItemCategoryRepository.GetItemCategories(), "Id", "Name");
            ViewBag.LargerUnitId = new SelectList(_unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures(), "Id", "Name");
            ViewBag.SmallerUnitId = new SelectList(_unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures(), "Id", "Name");
            ViewBag.Items = _unitOfWork.ItemRepository.GetItems(string.Empty).Select(x => new ItemComboViewModel() { ItemId = x.Id, ItemName = x.Name });
            return View();
        }

        // POST: /Item/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Item item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Done By Hassan 10 Feb 2015
                    _unitOfWork.BeginTransaction();
                    item = _unitOfWork.ItemRepository.InsertItem(item);
                    _unitOfWork.StockRepository.CreateStock(item);
                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    ViewBag.Message = item.Name+" Saved Successfully. You can add new item";
                }
            }
            catch(Exception ex)
            {
                _unitOfWork.RollBackTransaction();
                ModelState.AddModelError("", ex.Message + " Unable to save data!");
                Exception inex = ex.InnerException;
                while (inex != null)
                {
                    ModelState.AddModelError(string.Empty, inex.Message);
                    inex = inex.InnerException;
                }
            }
            ViewBag.Items = _unitOfWork.ItemRepository.GetItems(string.Empty).Select(x => new ItemComboViewModel() { ItemId = x.Id, ItemName = x.Name });
            
            ViewBag.ItemType = new SelectList(ItemTypeList, "Value", "Text", (int)item.ItemType);
            ViewBag.ItemCategoryId = new SelectList(_unitOfWork.ItemCategoryRepository.GetItemCategories(), "Id", "Name", item.ItemCategoryId);
            ViewBag.LargerUnitId = new SelectList(_unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures(), "Id", "Name", item.LargerUnitId);
            ViewBag.SmallerUnitId = new SelectList(_unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures(), "Id", "Name", item.SmallerUnitId);
            return View(new Item());
        }

        // GET: /Item/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = _unitOfWork.ItemRepository.GetItemByID(id.Value);
            if (item == null)
            {
                return HttpNotFound();
            }
            ViewBag.ItemType = new SelectList(ItemTypeList, "Value", "Text", (int)item.ItemType);
            ViewBag.ItemCategoryId = new SelectList(_unitOfWork.ItemCategoryRepository.GetItemCategories(), "Id", "Name", item.ItemCategoryId);
            ViewBag.LargerUnitId = new SelectList(_unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures(), "Id", "Name", item.LargerUnitId);
            ViewBag.SmallerUnitId = new SelectList(_unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures(), "Id", "Name", item.SmallerUnitId);
            return View(item);
        }

        // POST: /Item/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Item item)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    // Done By Hassan 10 Feb 2015
                    _unitOfWork.BeginTransaction();
                    item = _unitOfWork.ItemRepository.UpdateItem(item);

                    //if (!item.KeepPrevStock)
                    //    _unitOfWork.StockRepository.DeleteStocksByItemID(item.Id);

                    _unitOfWork.StockRepository.CreateStock(item);
                    _unitOfWork.Save();
                    _unitOfWork.CommitTransaction();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                _unitOfWork.RollBackTransaction();
                ModelState.AddModelError("", ex.Message + " Unable to save data!");
                Exception inex = ex.InnerException;
                while (inex != null)
                {
                    ModelState.AddModelError(string.Empty, inex.Message);
                    inex = inex.InnerException;
                }
            }
            ViewBag.ItemType = new SelectList(ItemTypeList, "Value", "Text", (int)item.ItemType);
            ViewBag.ItemCategoryId = new SelectList(_unitOfWork.ItemCategoryRepository.GetItemCategories(), "Id", "Name", item.ItemCategoryId);
            ViewBag.LargerUnitId = new SelectList(_unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures(), "Id", "Name", item.LargerUnitId);
            ViewBag.SmallerUnitId = new SelectList(_unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures(), "Id", "Name", item.SmallerUnitId);
            return View(item);
        }

        // GET: /Item/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = _unitOfWork.ItemRepository.GetItemByID(id.Value);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // POST: /Item/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {

                _unitOfWork.ItemRepository.DeleteItem(id);
                _unitOfWork.Save();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _unitOfWork.RollBackTransaction();
                ModelState.AddModelError("", ex.Message + " Unable to delete data!");
                Exception inex = ex.InnerException;
                while (inex != null)
                {
                    ModelState.AddModelError(string.Empty, inex.Message);
                    inex = inex.InnerException;
                }
            }
            return View();
        }
    }
}
