﻿using Inventory.Domain;
using Kendo.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        
        //[PopulateSiteMap(SiteMapName = "sample", ViewDataKey = "sample")]
        public ActionResult Index()
        {
            //if (!SiteMapManager.SiteMaps.ContainsKey("sample"))
            //{
            //    SiteMapManager.SiteMaps.Register<XmlSiteMap>("sample", sitmap => sitmap.LoadFrom("~/sitemap.config"));
            //}
            //SiteMapBase map = SiteMapManager.SiteMaps["sample"];

            return View();
        }

	}
}