﻿using Inventory.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers
{
    [Authorize]
    public class ItemCategoryController : Controller
    {
        private IUnitOfWork _unitOfWork;

        public ItemCategoryController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        //
        // GET: /ItemCategory/
        public ActionResult Index()
        {
            return View(_unitOfWork.ItemCategoryRepository.GetItemCategories().ToList());
        }

        //
        // GET: /ItemCategory/Details/5
        public ActionResult Details(int id)
        {
            return View(_unitOfWork.ItemCategoryRepository.GetItemCategoryByID(id));
        }

        //
        // GET: /ItemCategory/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ItemCategory/Create
        [HttpPost]
        public ActionResult Create(ItemCategory item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.ItemCategoryRepository.InsertItemCategory(item);
                    _unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message + " Unable to save data!");
                Exception inex = ex.InnerException;
                while (inex != null)
                {
                    ModelState.AddModelError(string.Empty, inex.Message);
                    inex = inex.InnerException;
                }
            }
            return View();
        }

        //
        // GET: /ItemCategory/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ItemCategory oItemCategory = _unitOfWork.ItemCategoryRepository.GetItemCategoryByID(id);

            if (oItemCategory == null)
            {
                return HttpNotFound();
            }
            return View(oItemCategory);
        }

        //
        // POST: /ItemCategory/Edit/5
        [HttpPost]
        public ActionResult Edit(ItemCategory itemCategory)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.ItemCategoryRepository.UpdateItemCategory(itemCategory);
                    _unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message+" Unable to save changes!");
                Exception inex = ex.InnerException;
                while (inex != null)
                {
                    ModelState.AddModelError(string.Empty, inex.Message);
                    inex = inex.InnerException;
                }
            }
            return View(itemCategory);
        }

        //
        // GET: /ItemCategory/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemCategory oItemCategory = _unitOfWork.ItemCategoryRepository.GetItemCategoryByID(id);

            if (oItemCategory == null)
            {
                return HttpNotFound();
            }
            return View(oItemCategory);
        }

        //
        // POST: /ItemCategory/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirm(int id)
        {
            ItemCategory oItemCategory = new ItemCategory();
            try
            {
                oItemCategory = _unitOfWork.ItemCategoryRepository.GetItemCategoryByID(id);
                _unitOfWork.ItemCategoryRepository.DeleteItemCategory(id);
                _unitOfWork.Save();
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message + " Unable to delete!");
                Exception inex = ex.InnerException;
                while(inex!=null)
                {
                    ModelState.AddModelError(string.Empty, inex.Message);
                    inex = inex.InnerException;
                }
            }
            return View(oItemCategory);
        }
    }
}
