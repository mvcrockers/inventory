﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventory.Domain;
using Inventory.WebUI;

namespace Inventory.Controllers
{
    [Authorize]
    public class PurchaseOrderController : Controller
    {
        private IUnitOfWork unitOfWork;

        public PurchaseOrderController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: /PurchaseOrder/
        public ActionResult Index()
        {
            return View(unitOfWork.PurchaseOrderRepository.GetPurchaseOrders().ToList());
        }

        // GET: /PurchaseOrder/Create
        public ActionResult Create()
        {
            PurchaseOrderViewModel VModel = new PurchaseOrderViewModel();

            ViewBag.Suppliers = unitOfWork.SupplierRepository.GetSuppliers().Select(x => new SupplierComboViewModel() { SupplierId = x.Id, SupplierName = x.Name });

            ViewBag.Items = unitOfWork.ItemRepository.GetItems(string.Empty).Select(x => new ItemComboViewModel() { ItemId = x.Id, ItemName = x.Name });

            Session["PurchaseOrderItems"] = new List<PurchaseOrderItemViewModel>();


            return View(VModel);
        }

        // POST: /PurchaseOrder/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PurchaseOrderViewModel oPurchaseOrderVM)
        {
            if (ModelState.IsValid)
            {
                List<PurchaseOrderItemViewModel> VModels = Session["PurchaseOrderItems"] as List<PurchaseOrderItemViewModel>;
                oPurchaseOrderVM.PurchaseOrderItems = VModels;

                if (VModels == null || VModels.Count <= 0)
                    return View(oPurchaseOrderVM);

                oPurchaseOrderVM.ReceivedDate = null;
                oPurchaseOrderVM.IsReceived = false;

                VMModelMapper mapper = new VMModelMapper();
                PurchaseOrder purchaseOrder = mapper.MapPurchaseOrderVM(oPurchaseOrderVM);

                unitOfWork.PurchaseOrderRepository.InsertPurchaseOrder(purchaseOrder);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }

            return View(oPurchaseOrderVM);
        }

        [HttpGet]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder pOrder = unitOfWork.PurchaseOrderRepository.GetPurchaseOrderByID(id.Value);

            if (pOrder == null)
            {
                return HttpNotFound();
            }

            List<Supplier> SupplierList = unitOfWork.SupplierRepository.GetSuppliers().ToList();
            List<Item> ItemList = unitOfWork.ItemRepository.GetItems(string.Empty).ToList();
            List<UnitOfMeasure> UOMList = unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures().ToList();

            VMModelMapper vmMapper = new VMModelMapper();
            List<PurchaseOrderItemViewModel> pItemVM = vmMapper.MapPurchaseOrderItemToVM(pOrder.PurchaseOrderItems);

            if (pItemVM != null && pItemVM.Count > 0)
            {
                int count = 1;
                foreach (var item in pItemVM)
                {
                    item.Index = count++;
                    item.SupplierName = SupplierList.First(x => x.Id == item.SupplierId).Name;
                    item.ItemName = ItemList.First(x => x.Id == item.ItemId).Name;
                    item.UnitName = UOMList.First(x => x.Id == item.UnitOfMeasureId).Name;
                }
            }

            Session["PurchaseOrderItemsVM"] = pItemVM.ToList();

            return View(pOrder);
        }

        [HttpPost]
        public ActionResult Details(PurchaseOrder purchaseOrder)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    purchaseOrder.IsReceived = true;

                    List<PurchaseOrderItemViewModel> pItemVM = Session["PurchaseOrderItemsVM"] as List<PurchaseOrderItemViewModel>;

                    VMModelMapper mapper = new VMModelMapper();
                    List<PurchaseOrderItem> purchaseOrderItems = mapper.MapPurchaseOrderItemFromVM(pItemVM);
                    purchaseOrder.PurchaseOrderItems = purchaseOrderItems;

                    unitOfWork.BeginTransaction();

                    unitOfWork.PurchaseOrderRepository.UpdatePurchaseOrder(purchaseOrder);
                    unitOfWork.Save();

                    ICollection<StockTranItem> stranItems = new List<StockTranItem>();

                    foreach (var item in purchaseOrder.PurchaseOrderItems)
                    {
                        stranItems.Add(new StockTranItem()
                        {
                           // ItemID = item.ItemID,
                            ParentTranID = (int)item.PurchaseOrderID,
                            ParentTranNo = purchaseOrder.OrderCode,
                          //  Quantity = (item.ItemQty * item.ConversionFactor),
                            Trandate = purchaseOrder.ReceivedDate.Value,
                            TranID = (int)item.ID,
                            TransactionType = TransactionTypeEnum.PurchaseViaOrder,
                            TranSide = TranSideEnum.Increase
                        });
                    }

                    unitOfWork.StockRepository.Insert(stranItems);

                    unitOfWork.Save();
                    unitOfWork.CommitTransaction();
                    Session["PurchaseOrderItemsVM"] = null;

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    unitOfWork.RollBackTransaction();
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return View();
        }

        [HttpGet]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PurchaseOrder pOrder = unitOfWork.PurchaseOrderRepository.GetPurchaseOrderByID(id.Value);

            if (pOrder == null)
            {
                return HttpNotFound();
            }

            PurchaseOrderViewModel pOrderVM = new PurchaseOrderViewModel()
            {
                Id = (int)pOrder.ID,
                OrderCode = pOrder.OrderCode,
                OrderDate = pOrder.OrderDate,
                ReceivedDate = pOrder.ReceivedDate,
                IsReceived = pOrder.IsReceived
            };

            List<Supplier> SupplierList = unitOfWork.SupplierRepository.GetSuppliers().ToList();
            List<Item> ItemList = unitOfWork.ItemRepository.GetItems(string.Empty).ToList();
            List<UnitOfMeasure> UOMList = unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures().ToList();

            Session["Suppliers"] = unitOfWork.SupplierRepository.GetSuppliers().Select(x => new SupplierComboViewModel() { SupplierId = x.Id, SupplierName = x.Name });

            Session["Items"] = ItemList.Select(x => new ItemComboViewModel() { ItemId = x.Id, ItemName = x.Name });

            VMModelMapper vmMapper = new VMModelMapper();
            List<PurchaseOrderItemViewModel> pItemVM = vmMapper.MapPurchaseOrderItemToVM(pOrder.PurchaseOrderItems);

            int count = 1;
            foreach (var item in pItemVM)
            {
                item.Index = count++;
                item.SupplierName = SupplierList.First(x => x.Id == item.SupplierId).Name;
                item.ItemName = ItemList.First(x => x.Id == item.ItemId).Name;
                item.UnitName = UOMList.First(x => x.Id == item.UnitOfMeasureId).Name;
            }

            Session["PurchaseOrderItems"] = pItemVM.ToList();

            return View(pOrderVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PurchaseOrderViewModel VModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    VMModelMapper mapper = new VMModelMapper();
                    PurchaseOrder model = mapper.MapPurchaseOrderVM(VModel);

                    List<PurchaseOrderItemViewModel> POIVM = Session["PurchaseOrderItems"] as List<PurchaseOrderItemViewModel>;

                    model.PurchaseOrderItems = mapper.MapPurchaseOrderItemFromVM(POIVM);

                    unitOfWork.BeginTransaction();

                    unitOfWork.PurchaseOrderRepository.UpdatePurchaseOrder(model);
                    unitOfWork.Save();

                    if (VModel.IsReceived)
                    {
                        ICollection<StockTranItem> stranItems = new List<StockTranItem>();

                        foreach (var item in model.PurchaseOrderItems)
                        {
                            stranItems.Add(new StockTranItem()
                            {
                               // ItemID = item.ItemID,
                                ParentTranID = (int)item.PurchaseOrderID,
                                ParentTranNo = VModel.OrderCode,
                              //  Quantity = (item.ItemQty * item.ConversionFactor),
                                Trandate = VModel.ReceivedDate.Value,
                                TranID = (int)item.ID,
                                TransactionType = TransactionTypeEnum.PurchaseViaOrder,
                                TranSide = TranSideEnum.Increase
                            });
                        }

                        unitOfWork.StockRepository.Update(stranItems);
                        unitOfWork.Save();
                    }

                    unitOfWork.CommitTransaction();
                    Session["PurchaseOrderItems"] = null;
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    unitOfWork.RollBackTransaction();
                    ModelState.AddModelError("", ex.Message);
                }

            }

            return View(VModel);
        }

        [HttpGet]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder pOrder = unitOfWork.PurchaseOrderRepository.GetPurchaseOrderByID(id.Value);

            if (pOrder == null)
            {
                return HttpNotFound();
            }

            List<Supplier> SupplierList = unitOfWork.SupplierRepository.GetSuppliers().ToList();
            List<Item> ItemList = unitOfWork.ItemRepository.GetItems(string.Empty).ToList();
            List<UnitOfMeasure> UOMList = unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures().ToList();

            VMModelMapper vmMapper = new VMModelMapper();
            List<PurchaseOrderItemViewModel> pItemVM = vmMapper.MapPurchaseOrderItemToVM(pOrder.PurchaseOrderItems);

            int count = 1;
            foreach (var item in pItemVM)
            {
                item.Index = count++;
                item.SupplierName = SupplierList.First(x => x.Id == item.SupplierId).Name;
                item.ItemName = ItemList.First(x => x.Id == item.ItemId).Name;
                item.UnitName = UOMList.First(x => x.Id == item.UnitOfMeasureId).Name;
            }

            Session["PurchaseOrderItemsVM"] = pItemVM.ToList();

            return View(pOrder);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                PurchaseOrder pOrder = unitOfWork.PurchaseOrderRepository.GetPurchaseOrderByID(id);
                unitOfWork.BeginTransaction();
                unitOfWork.PurchaseOrderRepository.DeletePurchaseOrder(id);
                unitOfWork.Save();
                unitOfWork.StockRepository.Delete(TransactionTypeEnum.PurchaseViaOrder, (int)id);
                unitOfWork.Save();
                unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                unitOfWork.RollBackTransaction();
            }

            return RedirectToAction("Index");
        }

        public JsonResult AddPurchaseOrderItem(string supplierId, string ItemId, string uomId, string qty)
        {
            PurchaseOrderItemViewModel VModel = new PurchaseOrderItemViewModel();
            List<PurchaseOrderItemViewModel> VModels = Session["PurchaseOrderItems"] as List<PurchaseOrderItemViewModel>;
            VModel.Index = VModels.Count + 1;
            VModel.SupplierId = Convert.ToInt32(supplierId);
            VModel.ItemId = Convert.ToInt32(ItemId);
            VModel.ItemQty = Convert.ToInt64(qty);
            VModel.UnitOfMeasureId = Convert.ToInt32(uomId);

            Supplier oSupplier = unitOfWork.SupplierRepository.GetSupplierByID(VModel.SupplierId);

            Item oItem = unitOfWork.ItemRepository.GetItemByID(VModel.ItemId);
            UnitOfMeasure oUOM = unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasureByID(VModel.UnitOfMeasureId);
            if (oItem.SmallerUnitId == VModel.UnitOfMeasureId)
            {
                VModel.ConversionFactor = 1;
            }
            else
            {
                VModel.ConversionFactor = oItem.ConversionFactor.Value;
            }
            VModel.SupplierName = oSupplier.Name;
            VModel.ItemName = oItem.Name;
            VModel.UnitName = oUOM.Name;

            VModels.Add(VModel);

            Session["PurchaseOrderItems"] = VModels;

            return Json(VModel);
        }

        public JsonResult DeletePurchaseOrderItem(string Index)
        {
            List<PurchaseOrderItemViewModel> VModels = Session["PurchaseOrderItems"] as List<PurchaseOrderItemViewModel>;
            var deletedVModel = VModels.SingleOrDefault(x => x.Index == Convert.ToInt32(Index));
            if (deletedVModel != null)
                VModels.Remove(deletedVModel);

            int count = 1;
            VModels.ForEach(x => x.Index = (count++));

            Session["PurchaseOrderItems"] = VModels;

            return Json(VModels);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
