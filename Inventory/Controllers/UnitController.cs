﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventory.Domain;

namespace Inventory.Controllers
{
    [Authorize]
    public class UnitController : Controller
    {
        private IUnitOfWork unitOfWork;

        public UnitController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: /Unit/
        public ActionResult Index()
        {
            return View(unitOfWork.UnitRepository.GetUnits().ToList());
        }

        // GET: /Unit/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Unit unit = unitOfWork.UnitRepository.GetUnitByID(id.Value);
            if (unit == null)
            {
                return HttpNotFound();
            }
            return View(unit);
        }

        // GET: /Unit/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Unit/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Code,Name,Address")] Unit unit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.UnitRepository.InsertUnit(unit);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message + " Unable to save data!");
                Exception inex = ex.InnerException;
                while (inex != null)
                {
                    ModelState.AddModelError(string.Empty, inex.Message);
                    inex = inex.InnerException;
                }
            }
            
            return View(unit);
        }

        // GET: /Unit/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Unit unit = unitOfWork.UnitRepository.GetUnitByID(id.Value);
            if (unit == null)
            {
                return HttpNotFound();
            }
            return View(unit);
        }

        // POST: /Unit/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Code,Name,Address")] Unit unit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.UnitRepository.UpdateUnit(unit);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message + " Unable to save data!");
                Exception inex = ex.InnerException;
                while (inex != null)
                {
                    ModelState.AddModelError(string.Empty, inex.Message);
                    inex = inex.InnerException;
                }
            }
            return View(unit);
        }

        // GET: /Unit/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Unit unit = unitOfWork.UnitRepository.GetUnitByID(id.Value);
            if (unit == null)
            {
                return HttpNotFound();
            }
            return View(unit);
        }

        // POST: /Unit/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                unitOfWork.UnitRepository.DeleteUnit(id);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message + " Unable to delete data!");
                Exception inex = ex.InnerException;
                while (inex != null)
                {
                    ModelState.AddModelError(string.Empty, inex.Message);
                    inex = inex.InnerException;
                }
            }
            Unit model = unitOfWork.UnitRepository.GetUnitByID(id);
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
