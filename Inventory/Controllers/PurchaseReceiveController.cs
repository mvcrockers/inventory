﻿using System.IO;
using Inventory.Domain;
using Inventory.WebUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;

namespace Inventory.Controllers
{
    [Authorize]
    public class PurchaseReceiveController : Controller
    {
        private IUnitOfWork unitOfWork;


        public PurchaseReceiveController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: /PurchaseReceive/
        public ActionResult DirectPurchase()
        {
            List<PurchaseReceive> rcvList = new List<PurchaseReceive>();
            rcvList = unitOfWork
                        .PurchaseReceiveRepository
                        .GetPurchaseReceives()
                        .Where(x => x.PurchaseOrderID == null && x.ReceiveDate == DateTime.Today).ToList();



            rcvList.ForEach(x => x.TotalAmount = x.PurchaseReceiveItems.Sum(item => item.Price));

            return View(rcvList);
        }

        public ActionResult FilterDirectPurchases(string receivedStockType, DateTime? fromDate, DateTime? toDate)
        {
            ViewBag.FromDate = fromDate;
            ViewBag.ToDate = toDate;

            List<PurchaseReceive> rcvList = new List<PurchaseReceive>();
            if (!string.IsNullOrEmpty(receivedStockType) && fromDate.HasValue && toDate.HasValue)
            {
                ReceivedStockTypeEnum rsType = (ReceivedStockTypeEnum)Convert.ToInt32(receivedStockType);
                rcvList = unitOfWork
                .PurchaseReceiveRepository
                .GetPurchaseReceives()
                .Where(x => x.PurchaseOrderID == null && x.ReceivedStockType == rsType && (x.ReceiveDate >= fromDate.Value && x.ReceiveDate <= toDate.Value)).ToList();
            }
            else if (string.IsNullOrEmpty(receivedStockType) && fromDate.HasValue && toDate.HasValue)
            {
                //ReceivedStockTypeEnum rsType = (ReceivedStockTypeEnum)Convert.ToInt32(receivedStockType);
                rcvList = unitOfWork
                .PurchaseReceiveRepository
                .GetPurchaseReceives()
                .Where(x => x.PurchaseOrderID == null && (x.ReceiveDate >= fromDate.Value && x.ReceiveDate <= toDate.Value)).ToList();
            }
            else
            {
                rcvList = unitOfWork
                    .PurchaseReceiveRepository
                    .GetPurchaseReceives()
                    .Where(x => x.PurchaseOrderID == null).ToList();
            }

            rcvList.ForEach(x => x.TotalAmount = x.PurchaseReceiveItems.Sum(item => item.Price));

            ViewBag.ReceivedStocks = rcvList;
            //return PartialView("EditorTemplates/PurchaseReceive", ViewBag.ReceivedStocks);
            return PartialView("PurchaseReceiveGridPartial", ViewBag.ReceivedStocks);
        }

        [HttpPost]
        public ActionResult Report(IList<PurchaseReceive> purchaseReceives)
        {
            string ids = string.Empty;
            foreach (var purchaseReceive in purchaseReceives.Where(x => x.IsSelected))
            {
                ids += purchaseReceive.Id.ToString() + ',';
            }
            ids = ids.Trim(',');

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "PurchaseReceiveInvoiceReport.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return PartialView("PurchaseReceiveGridPartial");
            }

            InventoryDataSet InDataSet = new InventoryDataSet();

            InventoryDataSetTableAdapters.PurchaseReceiveInvoiceReportTableAdapter InTableAdapter =
                new InventoryDataSetTableAdapters.PurchaseReceiveInvoiceReportTableAdapter();

            InTableAdapter.Fill(InDataSet.PurchaseReceiveInvoiceReport, ids);

            ReportDataSource rd = new ReportDataSource("PurchaseReceiveInvoiceReport", InDataSet.Tables["PurchaseReceiveInvoiceReport"]);
            lr.DataSources.Add(rd);
            const string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtention;

            const string deviceInfo =
                    "<DeviceInfo>" +
                    "<OutputFormat>" + reportType + "</OutputFormat>" +
                    "<PageWidth>8.5in</PageWidth>" +
                    "<PageHeight>11in</PageHeight>" +
                    "<MarginTop>0.5in</MarginTop>" +
                    "<MarginLeft>1in</MarginLeft>" +
                    "<MarginRight>1in</MarginRight>" +
                    "<MarginBottom>0.5in</MarginBottom>" +
                    "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;

            byte[] renderredBytes = lr.Render(reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtention,
                out streams,
                out warnings);


            return File(renderredBytes, mimeType);
        }

        public ActionResult Report(string id)
        {
            string ids = string.IsNullOrEmpty(id) ? string.Empty : id.Trim(',');
            

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "PurchaseReceiveInvoiceReport.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return PartialView("PurchaseReceiveGridPartial");
            }

            InventoryDataSet InDataSet = new InventoryDataSet();

            InventoryDataSetTableAdapters.PurchaseReceiveInvoiceReportTableAdapter InTableAdapter =
                new InventoryDataSetTableAdapters.PurchaseReceiveInvoiceReportTableAdapter();

            if (ids != string.Empty)
                InTableAdapter.Fill(InDataSet.PurchaseReceiveInvoiceReport, ids);

            ReportDataSource rd = new ReportDataSource("PurchaseReceiveInvoiceReport", InDataSet.Tables["PurchaseReceiveInvoiceReport"]);
            lr.DataSources.Add(rd);
            const string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtention;

            const string deviceInfo =
                    "<DeviceInfo>" +
                    "<OutputFormat>" + reportType + "</OutputFormat>" +
                    "<PageWidth>8.5in</PageWidth>" +
                    "<PageHeight>11in</PageHeight>" +
                    "<MarginTop>0.5in</MarginTop>" +
                    "<MarginLeft>1in</MarginLeft>" +
                    "<MarginRight>1in</MarginRight>" +
                    "<MarginBottom>0.5in</MarginBottom>" +
                    "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;

            byte[] renderredBytes = lr.Render(reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtention,
                out streams,
                out warnings);


            return File(renderredBytes, mimeType);
        }

        public ActionResult DetailsDirectPurchase(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseReceive pReceive = unitOfWork.PurchaseReceiveRepository.GetPurchaseByID(id.Value);

            if (pReceive == null)
            {
                return HttpNotFound();
            }

            List<Item> ItemList = unitOfWork.ItemRepository.GetItems(string.Empty).ToList();
            List<UnitOfMeasure> UOMList = unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures().ToList();

            int count = 1;
            foreach (var item in pReceive.PurchaseReceiveItems)
            {
                item.index = count++;
                item.ItemName = ItemList.First(x => x.Id == item.Stock.ItemId).Name;
                item.UnitName = UOMList.First(x => x.Id == item.UnitOfMeasureId).Name;
            }

            ViewBag.PurchaseReceiveItems = pReceive.PurchaseReceiveItems.ToList();

            return View(pReceive);
        }
        public ActionResult CreateDirectPurchase()
        {
            DirectPurchaseReceiveViewModel VModel = new DirectPurchaseReceiveViewModel();

            ViewBag.Suppliers = new SelectList(unitOfWork.SupplierRepository.GetSuppliers().ToList(), "Id", "Name", VModel.SupplierId);

            ViewBag.Items = unitOfWork.ItemRepository.GetItems(string.Empty).Select(x => new ItemComboViewModel() { ItemId = x.Id, ItemName = x.Name });

            Session["PurchaseReceiveItems"] = ViewBag.PurchaseReceiveItems = new List<PurchaseReceiveItem>();

            // = new List<PurchaseReceiveItem>();

            return View(VModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDirectPurchase(DirectPurchaseReceiveViewModel VModel)
        {
            if (ModelState.IsValid)
            {
                //DirectPurchaseReceiveViewModel VModel = new DirectPurchaseReceiveViewModel();
                try
                {
                    PurchaseReceive PurchaseReceive = new PurchaseReceive()
                    {
                        PurchaseNo = VModel.PurchaseNo,
                        SupplierId = VModel.SupplierId,
                        ReceiveDate = VModel.ReceiveDate,
                        ReceivedStockType = VModel.ReceivedStockType,
                        Remarks = VModel.Remarks
                    };
                    PurchaseReceive.PurchaseReceiveItems = Session["PurchaseReceiveItems"] as List<PurchaseReceiveItem>;
                    foreach (var item in PurchaseReceive.PurchaseReceiveItems)
                    {
                        item.SupplierId = PurchaseReceive.SupplierId;
                    }

                    unitOfWork.BeginTransaction();

                    unitOfWork.PurchaseReceiveRepository.InsertPurchaseReceive(PurchaseReceive);

                    ICollection<StockTranItem> stranItems = new List<StockTranItem>();

                    foreach (var item in PurchaseReceive.PurchaseReceiveItems)
                    {
                        stranItems.Add(new StockTranItem()
                        {
                            ParentTranID = (Int32)PurchaseReceive.Id,
                            ParentTranNo = PurchaseReceive.PurchaseNo,
                            QuantityInSmallerUnit = item.ItemQty * item.ConversionFactor,
                            StockId = item.StockID,
                            Trandate = PurchaseReceive.ReceiveDate,
                            TranID = (int)item.Id,
                            TransactionType = TransactionTypeEnum.DirectPurchase,
                            TranSide = TranSideEnum.Increase
                        });
                    }

                    unitOfWork.StockRepository.Insert(stranItems);
                    unitOfWork.Save();
                    unitOfWork.CommitTransaction();
                    Session["PurchaseReceiveItems"] = null;
                    return RedirectToAction("DirectPurchase");
                }
                catch (Exception ex)
                {
                    unitOfWork.RollBackTransaction();
                    ModelState.AddModelError("", ex.Message);
                }

            }

            ViewBag.Suppliers = new SelectList(unitOfWork.SupplierRepository.GetSuppliers().ToList(), "Id", "Name", VModel.SupplierId);

            ViewBag.Items = unitOfWork.ItemRepository.GetItems(string.Empty).Select(x => new ItemComboViewModel() { ItemId = x.Id, ItemName = x.Name });

            ViewBag.PurchaseReceiveItems = Session["PurchaseReceiveItems"] as List<PurchaseReceiveItem>;
            return View(VModel);
        }


        public ActionResult UpdateDirectPurchase(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseReceive pReceive = unitOfWork.PurchaseReceiveRepository.GetPurchaseByID(id.Value);

            if (pReceive == null)
            {
                return HttpNotFound();
            }

            DirectPurchaseReceiveViewModel dPRVModel = new DirectPurchaseReceiveViewModel()
            {
                Id = (int)pReceive.Id,
                PurchaseNo = pReceive.PurchaseNo,
                ReceiveDate = pReceive.ReceiveDate,
                SupplierId = pReceive.SupplierId,
                ReceivedStockType = pReceive.ReceivedStockType,
                Remarks = pReceive.Remarks
            };
            List<Item> ItemList = unitOfWork.ItemRepository.GetItems(string.Empty).ToList();
            List<UnitOfMeasure> UOMList = unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures().ToList();

            ViewBag.Suppliers = new SelectList(unitOfWork.SupplierRepository.GetSuppliers().ToList(), "Id", "Name", dPRVModel.SupplierId);

            ViewBag.Items = ItemList.Select(x => new ItemComboViewModel() { ItemId = x.Id, ItemName = x.Name });

            int count = 1;

            foreach (var item in pReceive.PurchaseReceiveItems)
            {
                item.index = count++;
                item.ItemName = item.Stock.Item.Name;
            }



            Session["PurchaseReceiveItems"] = ViewBag.PurchaseReceiveItems = pReceive.PurchaseReceiveItems.ToList();
            return View(dPRVModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateDirectPurchase(DirectPurchaseReceiveViewModel VModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    PurchaseReceive model = new PurchaseReceive()
                    {
                        Id = VModel.Id,
                        PurchaseNo = VModel.PurchaseNo,
                        SupplierId = VModel.SupplierId,
                        ReceiveDate = VModel.ReceiveDate,
                        ReceivedStockType = VModel.ReceivedStockType,
                        Remarks = VModel.Remarks
                    };
                    model.PurchaseReceiveItems = Session["PurchaseReceiveItems"] as List<PurchaseReceiveItem>;
                    foreach (var item in model.PurchaseReceiveItems)
                    {
                        item.SupplierId = model.SupplierId;
                        item.PurchaseReceiveId = model.Id;
                    }
                    ICollection<StockTranItem> stranItems = new List<StockTranItem>();

                    unitOfWork.BeginTransaction();

                    unitOfWork.PurchaseReceiveRepository.UpdatePurchaseReceive(model);


                    foreach (var item in model.PurchaseReceiveItems)
                    {
                        stranItems.Add(new StockTranItem()
                        {
                            ParentTranID = (Int32)model.Id,
                            ParentTranNo = model.PurchaseNo,
                            QuantityInSmallerUnit = item.ItemQty * item.ConversionFactor,
                            StockId = item.StockID,
                            Trandate = model.ReceiveDate,
                            TranID = (int)item.Id,
                            TransactionType = TransactionTypeEnum.DirectPurchase,
                            TranSide = TranSideEnum.Increase
                        });
                    }

                    unitOfWork.StockRepository.Update(stranItems);
                    unitOfWork.Save();
                    unitOfWork.CommitTransaction();
                    Session["PurchaseReceiveItems"] = null;
                    return RedirectToAction("DirectPurchase");
                }
                catch (Exception ex)
                {
                    unitOfWork.RollBackTransaction();
                    ModelState.AddModelError("", ex.Message);
                }

            }

            ViewBag.Suppliers = new SelectList(unitOfWork.SupplierRepository.GetSuppliers().ToList(), "Id", "Name", VModel.SupplierId);

            ViewBag.Items = unitOfWork.ItemRepository.GetItems(string.Empty).Select(x => new ItemComboViewModel() { ItemId = x.Id, ItemName = x.Name });

            ViewBag.PurchaseReceiveItems = Session["PurchaseReceiveItems"] as List<PurchaseReceiveItem>;
            return View(VModel);
        }

        public ActionResult DeleteDirectPurchase(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseReceive pReceive = unitOfWork.PurchaseReceiveRepository.GetPurchaseByID(id.Value);

            if (pReceive == null)
            {
                return HttpNotFound();
            }


            List<Item> ItemList = unitOfWork.ItemRepository.GetItems(string.Empty).ToList();
            List<UnitOfMeasure> UOMList = unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasures().ToList();

            int count = 1;
            foreach (var item in pReceive.PurchaseReceiveItems)
            {
                item.index = count++;
                item.ItemName = ItemList.First(x => x.Id == item.Stock.ItemId).Name;
                item.UnitName = UOMList.First(x => x.Id == item.UnitOfMeasureId).Name;
            }

            ViewBag.PurchaseReceiveItems = pReceive.PurchaseReceiveItems.ToList();

            return View(pReceive);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteDirectPurchase(long id)
        {
            try
            {
                PurchaseReceive rcv = unitOfWork.PurchaseReceiveRepository.GetPurchaseByID(id);
                unitOfWork.BeginTransaction();
                unitOfWork.PurchaseReceiveRepository.DeletePurchaseReceive(id);
                unitOfWork.Save();
                unitOfWork.StockRepository.Delete(rcv.ReceivedStockType == ReceivedStockTypeEnum.Direct_Purchase ?
                                                        TransactionTypeEnum.DirectPurchase :
                                                        TransactionTypeEnum.ReceiveWithoutPurchase, (int)id);
                unitOfWork.Save();
                unitOfWork.CommitTransaction();

            }
            catch (Exception ex)
            {
                unitOfWork.RollBackTransaction();
            }

            return RedirectToAction("DirectPurchase");
        }


        public JsonResult AddPurchaseReceiveItem(string itemId, string StockId, string uomId, string qty, string supId)
        {
            PurchaseReceiveItem ItemModel = new PurchaseReceiveItem();
            List<PurchaseReceiveItem> ItemModels = Session["PurchaseReceiveItems"] as List<PurchaseReceiveItem>;
            ItemModel.index = ItemModels.Count + 1;
            ItemModel.StockID = Convert.ToInt32(StockId);
            ItemModel.ItemQty = Convert.ToInt64(qty);
            ItemModel.UnitOfMeasureId = Convert.ToInt32(uomId);
            ItemModel.SupplierId = Convert.ToInt32(supId);

            Item oItem = unitOfWork.ItemRepository.GetItemByID(Convert.ToInt32(itemId));
            UnitOfMeasure oUOM = unitOfWork.UnitOfMeasureRepository.GetUnitOfMeasureByID(ItemModel.UnitOfMeasureId);

            ItemModel.ItemName = oItem.Name;
            ItemModel.UnitName = oUOM.Name;

            Stock Stock = unitOfWork.StockRepository.GetStock(ItemModel.StockID);
            ItemModel.ConversionFactor = (Stock.LargerUnitId.HasValue && ItemModel.UnitOfMeasureId == Stock.LargerUnitId.Value) ? Stock.ConversionFactor.Value : 1;
            ItemModel.UnitPrice = Helper.CalculateUnitPrice(ItemModel.UnitOfMeasureId, Stock);

            ItemModel.Price = ItemModel.UnitPrice * ItemModel.ItemQty ;
            ItemModels.Add(ItemModel);

            Session["PurchaseReceiveItems"] = ItemModels;

            PurchaseReceiveItem viewModel = new PurchaseReceiveItem()
            {
                index = ItemModel.index,
                ItemName = ItemModel.ItemName,
                ItemQty = ItemModel.ItemQty,
                Price = ItemModel.Price
            };

            return Json(viewModel);
        }

        public JsonResult DeletePurchaseReceiveItem(string Index)
        {
            List<PurchaseReceiveItem> ItemModels = (Session["PurchaseReceiveItems"] as ICollection<PurchaseReceiveItem>).ToList();
            var deletedVModel = ItemModels.SingleOrDefault(x => x.index == Convert.ToInt32(Index));
            if (deletedVModel != null)
                ItemModels.Remove(deletedVModel);

            int count = 1;
            ItemModels.ForEach(x => x.index = (count++));

            Session["PurchaseReceiveItems"] = ItemModels;

            List<PurchaseReceiveItem> viewModels = new List<PurchaseReceiveItem>();

            ItemModels.ForEach(item =>
                                viewModels.Add(new PurchaseReceiveItem()
                                {
                                    index = item.index,
                                    ItemName = item.ItemName,
                                    ItemQty = item.ItemQty,
                                    Price = item.Price
                                })
                                );

            return Json(viewModels);
        }
    }
}