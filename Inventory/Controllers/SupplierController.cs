﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventory.Domain;

namespace Inventory.Controllers
{
    [Authorize]
    public class SupplierController : Controller
    {
        private IUnitOfWork unitOfWork;

        public SupplierController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            
        }

        // GET: /Supplier/
        public ActionResult Index()
        {
            List<Supplier> oSuppliers = new List<Supplier>();
            try
            {
                oSuppliers = unitOfWork.SupplierRepository.GetSuppliers().ToList();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Refresh the Page and if the problem still exists, Contact your system administrator");
            }
            return View(oSuppliers);
        }

        // GET: /Supplier/Details/5
        public ActionResult Details(int? id)
        {
            Supplier supplier = null;
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                supplier = unitOfWork.SupplierRepository.GetSupplierByID(id.Value);
                if (supplier == null)
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Refresh the Page and if the problem still exists, Contact your system administrator");
            }

            return View(supplier);
        }

        // GET: /Supplier/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Supplier/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,Address,MobileNo")] Supplier supplier)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.SupplierRepository.InsertSupplier(supplier);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Submit the Page again and if the problem still exists, Contact your system administrator");
            }

            return View(supplier);
        }

        // GET: /Supplier/Edit/5
        public ActionResult Edit(int? id)
        {
            Supplier supplier = null;
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                supplier = unitOfWork.SupplierRepository.GetSupplierByID(id.Value);
                if (supplier == null)
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Refresh the Page and if the problem still exists, Contact your system administrator");
            }

            return View(supplier);
        }

        // POST: /Supplier/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,Address,MobileNo")] Supplier supplier)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.SupplierRepository.UpdateSupplier(supplier);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Submit the Page again and if the problem still exists, Contact your system administrator");
            }
            return View(supplier);
        }

        // GET: /Supplier/Delete/5
        public ActionResult Delete(int? id)
        {
            Supplier supplier = null;
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                supplier = unitOfWork.SupplierRepository.GetSupplierByID(id.Value);
                if (supplier == null)
                {
                    return HttpNotFound();
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Refresh the Page and if the problem still exists, Contact your system administrator");
            }

            return View(supplier);
        }

        // POST: /Supplier/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Department department = new Department();
            try
            {
                unitOfWork.SupplierRepository.DeleteSupplier(id);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Delete again and if the problem still exists, Contact your system administrator");
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
