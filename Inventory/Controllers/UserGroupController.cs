﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inventory.Domain;
//using Telerik.Web.Mvc;
using Kendo.Mvc;
using System.Collections;
using System.Data;
namespace Inventory.Controllers
{
    [Authorize]
    public class UserGroupController : Controller
    {
        public IUserTypeRepository UserTypeRepository;
        public IUserGroupRepository UserGroupRepository;
        public IPermissionRepository PermissionRepository;
        public UserGroupController(IUserTypeRepository userTypeRepo, IUserGroupRepository groupRepo, IPermissionRepository permissionRepo)
        {
            this.UserTypeRepository = userTypeRepo;
            this.UserGroupRepository = groupRepo;
            this.PermissionRepository = permissionRepo;
        }
        //
        // GET: /UserGroup/
        public ActionResult Index()
        {
            IEnumerable<Group> groups = UserGroupRepository.GetUserGroup().ToList();
            return View(groups);
        }

        //
        // GET: /UserGroup/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /UserGroup/Create
        [PopulateSiteMap(SiteMapName = "sample", ViewDataKey = "sample")]
        public ActionResult Create()
        {

            PopulateUserTypeDownList(1);
            if (!SiteMapManager.SiteMaps.ContainsKey("sample"))
            {
                SiteMapManager.SiteMaps.Register<XmlSiteMap>("sample", sitmap => sitmap.LoadFrom("~/sitemap.config"));            
            }
            SiteMapBase map = SiteMapManager.SiteMaps["sample"];
            object value;
            map.RootNode.Attributes.TryGetValue("id",out value);
            return View("UserGroupCreate", new Group());
        }

        private void PopulateUserTypeDownList(object selectedUserType = null)
        {
            var UserTypes = UserTypeRepository.UserTypes;
            ViewBag.UserTypeID = new SelectList(UserTypes, "UserTypeID", "Name", selectedUserType);
        }
        //
        // Post: /UserGroup/Create
        [HttpPost]
        public ActionResult Create(string[] checkedFiles, Group UserGruop)
        { 
            foreach (string item in checkedFiles)
            {
                Permission perm = new Permission()
                                             {
                                                 GroupID = UserGruop.GroupID,
                                                 PermissionKey = item
                                             };
                UserGruop.Permissions.Add(perm);
            }

            UserGruop.CreateDate = DateTime.Today;
            UserGruop.CreatedBy = 1;
            UserGruop.InitTransferLogId = 1;
            UserGruop.IsActive = 1;
            UserGruop.TransferLogId = 1;

            try
            {
                if (ModelState.IsValid)
                {
                    UserGroupRepository.InsertUserGroup(UserGruop);
                    UserGroupRepository.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                ModelState.AddModelError("", "Unable to save changes!");
            }
            return View("UserGroupCreate");
        }

        //
        // GET: /UserGroup/Edit/5
        [PopulateSiteMap(SiteMapName = "sample", ViewDataKey = "sample")]
        public ActionResult Edit(int id)
        {
            if (!SiteMapManager.SiteMaps.ContainsKey("sample"))
            {
                SiteMapManager.SiteMaps.Register<XmlSiteMap>("sample", sitmap => sitmap.LoadFrom("~/sitemap.config"));
            }
            SiteMapBase map = SiteMapManager.SiteMaps["sample"];

            Group UserGroup = UserGroupRepository.GetUserGroupByID(id);
            PopulateUserTypeDownList(UserGroup.UserTypeID);
            return View(UserGroup);
        }

        //
        // POST: /UserGroup/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, string[] checkedFiles)
        {
            Group GroupToUpdate = UserGroupRepository.GetUserGroupByID(id);

            if (TryUpdateModel(GroupToUpdate, "", new string[] { "GroupName", "UserTypeID" }))
            {
                try
                {
                    UpdateGroupPermissions(checkedFiles, GroupToUpdate);

                    UserGroupRepository.UpdateUserGroup(GroupToUpdate);
                    UserGroupRepository.Save();

                    return RedirectToAction("Index");
                }
                catch (DataException /* dex */)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }
            PopulateUserTypeDownList(GroupToUpdate.GroupID);
            return View(GroupToUpdate);
        }

        private void UpdateGroupPermissions(string[] permissionKeys, Group groupToUpdate)
        {
           groupToUpdate.Permissions.Clear();

           foreach (string item in permissionKeys)
           {
               Permission perm = new Permission()
               {
                   GroupID = groupToUpdate.GroupID,
                   PermissionKey = item
               };
               groupToUpdate.Permissions.Add(perm);
           }
        }

        //
        // GET: /UserGroup/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /UserGroup/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
