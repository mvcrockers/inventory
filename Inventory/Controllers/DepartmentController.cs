﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventory.Domain;
using Inventory.ViewModels;

namespace Inventory.Controllers
{
    [Authorize]
    public class DepartmentController : Controller
    {
        private IUnitOfWork unitOfWork;
        private List<Unit> units;

        public DepartmentController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            
        }

        // GET: /Department/
        public ActionResult Index()
        {
            List<Department> oDepartments = new List<Department>();
            try
            {
              oDepartments =  unitOfWork.DepartmentRepository.GetDepartments().ToList();
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("","An Error Occurred:'"+ ex.Message+".'");
                ModelState.AddModelError("", "Please Refresh the Page and if the problem still exists, Contact your system administrator");                
            }
            return View(oDepartments);
        }

        // GET: /Department/Details/5
        public ActionResult Details(int? id)
        {
            Department department = null;
            try
            {
                if(id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                department = unitOfWork.DepartmentRepository.GetDepartmentByID(id.Value);
                if(department == null)
                {
                    return HttpNotFound();
                }
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Refresh the Page and if the problem still exists, Contact your system administrator");                
            }
            
            return View(department);
        }

        // GET: /Department/Create
        public ActionResult Create()
        {
            try
            {
                //units = this.unitOfWork.UnitRepository.GetUnits().ToList();
                //ViewBag.UnitId = new SelectList(units, "Id", "Name");
                var department = new Department
                {
                    Units = new Collection<Unit>()
                };
                PopulateAssignedUnits(department);
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Refresh the Page and if the problem still exists, Contact your system administrator");                
            }

            return View();
            
        }

        // POST: /Department/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Code,Name")] Department department, string[] selectedUnits)
        {
            try
            {
                if (selectedUnits != null)
                {
                    department.Units = new Collection<Unit>();
                    foreach (var unit in selectedUnits)
                    {
                        var unitToAdd = unitOfWork.UnitRepository.GetUnitByID(int.Parse(unit));
                        department.Units.Add(unitToAdd);
                    }
                }
                //units = this.unitOfWork.UnitRepository.GetUnits().ToList();
                //ViewBag.UnitId = new SelectList(units, "Id", "Name", department.UnitId);

                if(ModelState.IsValid)
                {
                    unitOfWork.DepartmentRepository.InsertDepartment(department);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Submit the Page again and if the problem still exists, Contact your system administrator");                
            }

            PopulateAssignedUnits(department);
            return View(department);
        }

        // GET: /Department/Edit/5
        public ActionResult Edit(int? id)
        {
            Department department = null;
            try
            {
                if(id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                //units = this.unitOfWork.UnitRepository.GetUnits().ToList();
                //ViewBag.UnitId = new SelectList(units, "Id", "Name");
                
                department = unitOfWork.DepartmentRepository.GetDepartmentByID(id.Value);
                PopulateAssignedUnits(department);
                
                if(department == null)
                {
                    return HttpNotFound();
                }
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Refresh the Page and if the problem still exists, Contact your system administrator");                
            }

            return View(department);
            
        }

        private void PopulateAssignedUnits(Department department)
        {
            var allUnits = unitOfWork.UnitRepository.GetUnits();
            var departmentUnits = new HashSet<int>();
            if (department.Units != null)
            {
                departmentUnits = new HashSet<int>(department.Units.Select(c => c.Id));
            }
            else
            {
                department.Units = new Collection<Unit>();
            }
            var viewModel = new List<AssignedUnitViewModel>();
            foreach (var unit in allUnits)
            {
                viewModel.Add(new AssignedUnitViewModel
                {
                    UnitID = unit.Id,
                    Name = unit.Name,
                    Assigned = departmentUnits.Contains(unit.Id)
                });
            }
            ViewBag.Units = viewModel;
        }

        // POST: /Department/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, string[] selectedUnits)
        {
            var departmentToUpdate = unitOfWork.DepartmentRepository.GetDepartments()
                .Include(i => i.Units).Single(i => i.Id == id);
                //units = this.unitOfWork.UnitRepository.GetUnits().ToList();
                //ViewBag.UnitId = new SelectList(units, "Id", "Name", department.UnitId);
                if (TryUpdateModel(departmentToUpdate, "",
                new string[] { "Code", "Name"}))
                {
                    try
                    {
                        UpdateDepartmentUnits(selectedUnits, departmentToUpdate);

                        unitOfWork.DepartmentRepository.UpdateDepartment(departmentToUpdate);
                        unitOfWork.Save();

                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                        ModelState.AddModelError("", "Please Submit the Page again and if the problem still exists, Contact your system administrator");
                    }
                }
                PopulateAssignedUnits(departmentToUpdate);
                return View(departmentToUpdate);
        }

        private void UpdateDepartmentUnits(string[] selectedUnits, Department department)
        {
            if (selectedUnits == null)
            {
                department.Units = new List<Unit>();
                return;
            }

            var selectedUnitsHS = new HashSet<string>(selectedUnits);
            var departmentUnits = new HashSet<int>();
            if (department.Units != null)
            {
                departmentUnits = new HashSet<int>(department.Units.Select(c => c.Id));
            }
            else
            {
                department.Units = new Collection<Unit>();
            }
            foreach (var unit in unitOfWork.UnitRepository.GetUnits())
            {
                if (selectedUnitsHS.Contains(unit.Id.ToString()))
                {
                    if (!departmentUnits.Contains(unit.Id))
                    {
                        department.Units.Add(unit);
                    }
                }
                else
                {
                    if (departmentUnits.Contains(unit.Id))
                    {
                        department.Units.Remove(unit);
                    }
                }
            }
        }

        // GET: /Department/Delete/5
        public ActionResult Delete(int? id)
        {
            Department department = new Department();
            try
            {

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                department = unitOfWork.DepartmentRepository.GetDepartmentByID(id.Value);
                if (department == null)
                {
                    return HttpNotFound();
                }

            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Refresh the Page and if the problem still exists, Contact your system administrator");                
            }
            return View(department);
        }

        // POST: /Department/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                unitOfWork.DepartmentRepository.DeleteDepartment(id);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", "An Error Occurred:'" + ex.Message + ".'");
                ModelState.AddModelError("", "Please Delete again and if the problem still exists, Contact your system administrator");
            }

            return RedirectToAction("Delete",id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
