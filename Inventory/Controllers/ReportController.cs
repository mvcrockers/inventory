﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventory.Domain;
using Inventory.WebUI;
using Microsoft.Reporting.WebForms;
using System.IO;

namespace Inventory.Controllers
{
    public class ReportController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private List<Unit> _units;
        private List<Department> _departments;
        private List<ItemCategory> _itemCategories;
        private List<Item> _items;
        public ReportController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public ActionResult MonthlyStationary()
        {
            _units = this._unitOfWork.UnitRepository.GetUnits().ToList();
            _itemCategories = this._unitOfWork.ItemCategoryRepository.GetItemCategories().ToList();
            _items = this._unitOfWork.ItemRepository.GetItems(null).ToList();

            _departments = this._unitOfWork.DepartmentRepository.GetDepartments().ToList();

            ViewBag.UnitId = new SelectList(_units, "Id", "Name");
            ViewBag.ItemCategoryId = new SelectList(_itemCategories, "Id", "Name");
            ViewBag.DepartmentId = new SelectList(_departments, "Id", "Name");
            ViewBag.ItemId = new SelectList(_items, "Id", "Name");

            return View("MonthlyStationaryReport");

            //return View("ReportViewer");
        }

        public JsonResult GetDeptByUnit(int unitId)
        {
            var departments = this._unitOfWork.UnitRepository.GetUnits().FirstOrDefault(u => u.Id == unitId).Departments.ToList();
            var obj = Json(new SelectList(departments, "Id", "Name"), JsonRequestBehavior.AllowGet);
            return obj;
        }


        [HttpPost]
        public ActionResult ViewReport(int? unitId, int? departmentId, int? itemCategoryId, int? ItemId, DateTime? fromDate, DateTime? toDate, RptRenderType rdoRptRenderType, RptType rdoRptType)
        {
            string path=string.Empty;
            LocalReport lr = new LocalReport();
            
            if(rdoRptType==RptType.UnitWise)
                path = Path.Combine(Server.MapPath("~/Report"), "Distribution.rdlc");
            else if (rdoRptType == RptType.ItemWise)
                path = Path.Combine(Server.MapPath("~/Report"), "rptMonthlyStationary.rdlc");

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("MonthlyStationaryReport");
            }

            InventoryDataSet InDataSet = new InventoryDataSet();

            InventoryDataSetTableAdapters.spDistributionTableAdapter InTableAdapter =
                new InventoryDataSetTableAdapters.spDistributionTableAdapter();


            InTableAdapter.Fill(InDataSet.spDistribution, unitId, departmentId, itemCategoryId, ItemId, fromDate, toDate);

            ReportDataSource rd = new ReportDataSource("Distribution", InDataSet.Tables["spDistribution"]);
            lr.DataSources.Add(rd);
            string mimeType;
            string encoding;
            string fileNameExtention;

            string deviceInfo =
                    "<DeviceInfo>" +
                    "<OutputFormat>" + rdoRptRenderType.ToString() + "</OutputFormat>" +
                    "<PageWidth>8.5in</PageWidth>" +
                    "<PageHeight>11in</PageHeight>" +
                    "<MarginTop>0.5in</MarginTop>" +
                    "<MarginLeft>1in</MarginLeft>" +
                    "<MarginRight>1in</MarginRight>" +
                    "<MarginBottom>0.5in</MarginBottom>" +
                    "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderredBytes;

            renderredBytes = lr.Render(rdoRptRenderType.ToString(),
                                deviceInfo,
                                out mimeType,
                                out encoding,
                                out fileNameExtention,
                                out streams,
                                out warnings);


            return File(renderredBytes, mimeType);
        }

        public ActionResult StockReport(string id)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "rptStockPosition.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("MonthlyStationaryReport");
            }

            InventoryDataSet InDataSet = new InventoryDataSet();

            InventoryDataSetTableAdapters.StockPositionReportTableAdapter InTableAdapter =
                new InventoryDataSetTableAdapters.StockPositionReportTableAdapter();

            InTableAdapter.Fill(InDataSet.StockPositionReport);

            ReportDataSource rd = new ReportDataSource("StockPositionReport", InDataSet.Tables["StockPositionReport"]);
            lr.DataSources.Add(rd);
            

            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtention;

            string deviceInfo =
                    "<DeviceInfo>" +
                    "<OutputFormat>" + id + "</OutputFormat>" +
                    "<PageWidth>8.5in</PageWidth>" +
                    "<PageHeight>11in</PageHeight>" +
                    "<MarginTop>0.5in</MarginTop>" +
                    "<MarginLeft>1in</MarginLeft>" +
                    "<MarginRight>1in</MarginRight>" +
                    "<MarginBottom>0.5in</MarginBottom>" +
                    "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderredBytes;

            renderredBytes = lr.Render(reportType,
                                deviceInfo,
                                out mimeType,
                                out encoding,
                                out fileNameExtention,
                                out streams,
                                out warnings);


            return File(renderredBytes, mimeType);
        }
    }
}