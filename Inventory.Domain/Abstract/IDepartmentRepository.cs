﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IDepartmentRepository
    {
        IQueryable<Department> GetDepartments();

        Department GetDepartmentByID(int deptId);

        void InsertDepartment(Department oDepartment);

        void DeleteDepartment(int deptId);

        void UpdateDepartment(Department oDepartment);
    }
}
