﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IPurchaseOrderRepository
    {
        IQueryable<PurchaseOrder> GetPurchaseOrders();

        PurchaseOrder GetPurchaseOrderByID(long purchaseOrderID);

        void InsertPurchaseOrder(PurchaseOrder oPurchaseOrder);

        void DeletePurchaseOrder(long purchaseOrderID);

        void UpdatePurchaseOrder(PurchaseOrder oPurchaseOrder);

        IQueryable<PurchaseOrderItem> GetPurchaseOrderItems(long purchaseOrderID);

        PurchaseOrderItem GetPurchaseOrderItemByID(long purchaseOrderItemID);

        void InsertPurchaseOrderItem(PurchaseOrderItem oPurchaseOrderItem);

        void DeletePurchaseOrderItem(long purchaseOrderItemID);

        void UpdatePurchaseOrderItem(PurchaseOrderItem oPurchaseOrderItem);
    }
}
