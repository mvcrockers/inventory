﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IRoleRepository
    {
        IQueryable<Role> GetRoles();
        Role GetRoleByID(int RoleId);
        void InsertRole(Role oRole);
        void DeleteRole(int RoleId);
        void UpdateRole(Role oRole);
    }
}
