﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IUnitOfMeasureRepository
    {
        IQueryable<UnitOfMeasure> GetUnitOfMeasures();

        UnitOfMeasure GetUnitOfMeasureByID(int UnitOfMeasureId);

        void InsertUnitOfMeasure(UnitOfMeasure oUnitOfMeasure);

        void DeleteUnitOfMeasure(int UnitOfMeasureId);

        void UpdateUnitOfMeasure(UnitOfMeasure oUnitOfMeasure);
    }
}
