﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IStockTranItemRepository
    {
        List<StockTranItem> GetStockTranItems(TransactionTypeEnum tranType, int ParentTranId);
    }
}
