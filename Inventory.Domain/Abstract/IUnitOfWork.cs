﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IUnitOfWork
    {
        IUserRepository UserRepository { get; }

        IRoleRepository RoleRepository { get; }

        IUnitRepository UnitRepository { get; }

        IDepartmentRepository DepartmentRepository { get; }

        IItemCategoryRepository ItemCategoryRepository { get; }

        IUnitOfMeasureRepository UnitOfMeasureRepository { get; }

        IItemRepository ItemRepository { get; }

        ISupplierRepository SupplierRepository { get; }

        IPurchaseOrderRepository PurchaseOrderRepository { get; }
        
        IPurchaseReceiveRepository PurchaseReceiveRepository { get; }

        IStockRepository StockRepository { get; }

        IProductDistributionRepository ProductDistributionRepository { get; }
        void BeginTransaction();

        void CommitTransaction();

        void RollBackTransaction();
        void Save();
        void Dispose();
    }
}
