﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IPurchaseReceiveRepository
    {
        IQueryable<PurchaseReceive> GetPurchaseReceives();

        PurchaseReceive GetPurchaseByID(long purchaseReceiveID);

        PurchaseReceive InsertPurchaseReceive(PurchaseReceive oPurchaseReceive);

        PurchaseReceive UpdatePurchaseReceive(PurchaseReceive oPurchaseReceive);

        void DeletePurchaseReceive(long purchaseReceiveID);

        //void UpdatePurchaseReceive(PurchaseReceive oPurchaseReceive);

        //IQueryable<PurchaseReceiveItem> GetPurchaseReceiveItems(long purchaseReceiveID);

        //PurchaseReceiveItem GetPurchaseReceiveItemByID(long purchaseReceiveItemID);

        //void InsertPurchaseReceiveItem(PurchaseReceiveItem oPurchaseReceiveItem);

        //void DeletePurchaseReceiveItem(long purchaseReceiveItemID);

        //void UpdatePurchaseReceiveItem(PurchaseReceiveItem oPurchaseReceiveItem);
    }
}
