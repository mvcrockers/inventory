﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IStockRepository
    {
        Stock GetStock(int id);
        Stock GetStock(int ItemId, int? smallerUnitId, int? largerUnitId, decimal unitPrice, bool IsPriceInLargerUnit, decimal? ConversionFactor);
        IEnumerable<Stock> GetStocks(int itemId);
        void CreateStock(Item oItem);
        void Insert(ICollection<StockTranItem> Trans);
        void Update(ICollection<StockTranItem> Trans);
        void Delete(TransactionTypeEnum tranType, int ParentTranId);
        void DeleteStocksByItemID(int itemID);
    }
}
