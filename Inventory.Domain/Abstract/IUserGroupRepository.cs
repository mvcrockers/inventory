﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IUserGroupRepository
    {
        IQueryable<Group> GetUserGroup();
        Group GetUserGroupByID(int UserGroupId);
        void InsertUserGroup(Group UserGroup);
        void DeleteUserGroup(int UserGroupID);
        void UpdateUserGroup(Group UserGroup);
        void Save();
    }
}
