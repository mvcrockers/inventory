﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IProductDistributionRepository
    {
        IQueryable<ProductDistribution> GetProductDistributions();

        ProductDistribution GetProductDistributionByID(int ProductDistributionID);

        ProductDistribution InsertProductDistribution(ProductDistribution oProductDistribution);

        void DeleteProductDistribution(int ProductDistributionID);

        ProductDistribution UpdateProductDistribution(ProductDistribution oProductDistribution);
    }
}
