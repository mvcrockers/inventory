﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface ISupplierRepository
    {
        IQueryable<Supplier> GetSuppliers();

        Supplier GetSupplierByID(int supplierId);

        void InsertSupplier(Supplier oSupplier);

        void DeleteSupplier(int supplierId);

        void UpdateSupplier(Supplier oSupplier);
    }
}
