﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IUnitRepository
    {
        IQueryable<Unit> GetUnits();

        Unit GetUnitByID(int unitId);

        void InsertUnit(Unit oUnit);

        void DeleteUnit(int unitId);

        void UpdateUnit(Unit oUnit);
    }
}
