﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IItemCategoryRepository
    {
        IQueryable<ItemCategory> GetItemCategories();

        ItemCategory GetItemCategoryByID(int itemCategoryId);

        void InsertItemCategory(ItemCategory oItemCategory);

        void DeleteItemCategory(int itemCategoryId);

        void UpdateItemCategory(ItemCategory oItemCategory);
    }
}
