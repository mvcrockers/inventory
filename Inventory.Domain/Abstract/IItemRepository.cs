﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public interface IItemRepository
    {
        IQueryable<Item> GetItems(string searchString);

        Item GetItemByID(int ItemId);

        Item InsertItem(Item oItem);

        void DeleteItem(int ItemId);

        Item UpdateItem(Item oItem);


    }
}
