﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;


namespace Inventory.Domain
{
    [ServiceContract]
    public interface IUserTypeRepository
    {
        IQueryable<UserType> GetUserType();
        IQueryable<UserType> UserTypes { get; }
        UserType GetUserTypeByID(int UserTypeId);
        void InsertUserType(UserType UserType);
        void DeleteUserType(int UserTypeID);
        void UpdateUserType(UserType UserType);
        void Save();
    }
}
