namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeObjectOfStockReceive : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StockReceiveWithoutPurchases", "ItemId", "dbo.Items");
            DropForeignKey("dbo.StockReceiveWithoutPurchases", "UnitOfMeasureId", "dbo.UnitOfMeasures");
            DropIndex("dbo.StockReceiveWithoutPurchases", new[] { "ItemId" });
            DropIndex("dbo.StockReceiveWithoutPurchases", new[] { "UnitOfMeasureId" });
            CreateTable(
                "dbo.ReceivedStocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VoucherNo = c.String(nullable: false),
                        ReceiptDate = c.DateTime(nullable: false),
                        VoucherCode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReceivedStockItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitOfMeasureId = c.Int(nullable: false),
                        ReceivedStockId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: false)
                .ForeignKey("dbo.ReceivedStocks", t => t.ReceivedStockId, cascadeDelete: true)
                .ForeignKey("dbo.UnitOfMeasures", t => t.UnitOfMeasureId, cascadeDelete: false)
                .Index(t => t.ItemId)
                .Index(t => t.ReceivedStockId)
                .Index(t => t.UnitOfMeasureId);
            
            DropTable("dbo.StockReceiveWithoutPurchases");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.StockReceiveWithoutPurchases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitOfMeasureId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.ReceivedStockItems", "UnitOfMeasureId", "dbo.UnitOfMeasures");
            DropForeignKey("dbo.ReceivedStockItems", "ReceivedStockId", "dbo.ReceivedStocks");
            DropForeignKey("dbo.ReceivedStockItems", "ItemId", "dbo.Items");
            DropIndex("dbo.ReceivedStockItems", new[] { "UnitOfMeasureId" });
            DropIndex("dbo.ReceivedStockItems", new[] { "ReceivedStockId" });
            DropIndex("dbo.ReceivedStockItems", new[] { "ItemId" });
            DropTable("dbo.ReceivedStockItems");
            DropTable("dbo.ReceivedStocks");
            CreateIndex("dbo.StockReceiveWithoutPurchases", "UnitOfMeasureId");
            CreateIndex("dbo.StockReceiveWithoutPurchases", "ItemId");
            AddForeignKey("dbo.StockReceiveWithoutPurchases", "UnitOfMeasureId", "dbo.UnitOfMeasures", "Id", cascadeDelete: false);
            AddForeignKey("dbo.StockReceiveWithoutPurchases", "ItemId", "dbo.Items", "Id", cascadeDelete: false);
        }
    }
}
