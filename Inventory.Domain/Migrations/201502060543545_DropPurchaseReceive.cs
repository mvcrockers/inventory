namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropPurchaseReceive : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PurchaseReceiveItems", "Item_Id", "dbo.Items");
            DropForeignKey("dbo.PurchaseReceives", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseReceiveItems", "PurchaseReceiveId", "dbo.PurchaseReceives");
            DropForeignKey("dbo.PurchaseReceives", "Supplier_Id", "dbo.Suppliers");
            DropForeignKey("dbo.PurchaseReceiveItems", "Supplier_Id", "dbo.Suppliers");
            DropForeignKey("dbo.PurchaseReceiveItems", "UnitOfMeasure_Id", "dbo.UnitOfMeasures");
            DropIndex("dbo.PurchaseReceiveItems", new[] { "Item_Id" });
            DropIndex("dbo.PurchaseReceives", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseReceiveItems", new[] { "PurchaseReceiveId" });
            DropIndex("dbo.PurchaseReceives", new[] { "Supplier_Id" });
            DropIndex("dbo.PurchaseReceiveItems", new[] { "Supplier_Id" });
            DropIndex("dbo.PurchaseReceiveItems", new[] { "UnitOfMeasure_Id" });
            DropTable("dbo.PurchaseReceiveItems");
            DropTable("dbo.PurchaseReceives");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PurchaseReceives",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PurchaseNo = c.String(),
                        SupplierId = c.Long(nullable: false),
                        PurchaseOrderID = c.Long(),
                        PurchaseOrderNo = c.String(),
                        ReceiveDate = c.DateTime(nullable: false),
                        Supplier_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PurchaseReceiveItems",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PurchaseReceiveId = c.Long(nullable: false),
                        SupplierId = c.Long(nullable: false),
                        ItemId = c.Long(nullable: false),
                        UnitOfMeasureId = c.Long(nullable: false),
                        ItemQty = c.Long(nullable: false),
                        ConversionFactor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Item_Id = c.Int(),
                        Supplier_Id = c.Int(),
                        UnitOfMeasure_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.PurchaseReceiveItems", "UnitOfMeasure_Id");
            CreateIndex("dbo.PurchaseReceiveItems", "Supplier_Id");
            CreateIndex("dbo.PurchaseReceives", "Supplier_Id");
            CreateIndex("dbo.PurchaseReceiveItems", "PurchaseReceiveId");
            CreateIndex("dbo.PurchaseReceives", "PurchaseOrderID");
            CreateIndex("dbo.PurchaseReceiveItems", "Item_Id");
            AddForeignKey("dbo.PurchaseReceiveItems", "UnitOfMeasure_Id", "dbo.UnitOfMeasures", "Id");
            AddForeignKey("dbo.PurchaseReceiveItems", "Supplier_Id", "dbo.Suppliers", "Id");
            AddForeignKey("dbo.PurchaseReceives", "Supplier_Id", "dbo.Suppliers", "Id");
            AddForeignKey("dbo.PurchaseReceiveItems", "PurchaseReceiveId", "dbo.PurchaseReceives", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PurchaseReceives", "PurchaseOrderID", "dbo.PurchaseOrders", "ID");
            AddForeignKey("dbo.PurchaseReceiveItems", "Item_Id", "dbo.Items", "Id");
        }
    }
}
