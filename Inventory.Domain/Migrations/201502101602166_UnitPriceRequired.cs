namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UnitPriceRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Items", "UnitPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Items", "UnitPrice", c => c.Decimal(precision: 18, scale: 2));
        }
    }
}
