namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BugFixing : DbMigration
    {
        public override void Up()
        {
            Sql("IF OBJECT_ID('ProductDistributionItems') IS NOT NULL"
                + "  DROP TABLE ProductDistributionItems;");

            CreateTable(
                "dbo.ProductDistributionItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UnitID = c.Int(nullable: false),
                        DepartmentID = c.Int(nullable: false),
                        ItemId = c.Int(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitOfMeasureId = c.Int(nullable: false),
                        ProductDistributionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("dbo.ProductDistributions", t => t.ProductDistributionId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.ProductDistributionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductDistributionItems", "ProductDistributionId", "dbo.ProductDistributions");
            DropForeignKey("dbo.ProductDistributionItems", "ItemId", "dbo.Items");
            DropIndex("dbo.ProductDistributionItems", new[] { "ProductDistributionId" });
            DropIndex("dbo.ProductDistributionItems", new[] { "ItemId" });
            DropTable("dbo.ProductDistributionItems");
        }
    }
}
