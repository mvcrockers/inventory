namespace Inventory.Domain.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<Inventory.Domain.TMContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Inventory.Domain.TMContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //context.UserTypes.AddOrUpdate(
            //    p => p.UserTypeID,
            //    new UserType()
            //    {
            //        UserTypeID = 1,
            //        Code="001",
            //        Name = "SuperUser"
                    
            //    });

            var User1 = new User
            {
                Username = "SuperUser1",
                //Password = Crypto.HashPassword("Orange1@2#3$4"),
                Password = Crypto.HashPassword("x"),
                Email = "hassan@celimited.com",
                IsApproved = true,
                IsLockedOut = false,
                PasswordFailuresSinceLastSuccess = 0,
                Roles = new List<Role>()
            };
            var User2 = new User
            {
                Username = "Admin1",
                Password = Crypto.HashPassword("123456"),
                Email = "dibosh@celimited.com",
                IsApproved = true,
                IsLockedOut = false,
                PasswordFailuresSinceLastSuccess = 0,
                Roles = new List<Role>()
            };
            var User3 = new User
            {
                Username = "General1",
                Password = Crypto.HashPassword("123456"),
                Email = "chapal@celimited.com",
                IsApproved = true,
                IsLockedOut = false,
                PasswordFailuresSinceLastSuccess = 0,
                Roles = new List<Role>()
            };
            var Role1 = new Role
            {
                RoleName = "SuperUser",
                Description = "Owner"
            };
            var Role2 = new Role
            {
                RoleName = "Admin",
                Description = "Administrator"
            };
            var Role3 = new Role
            {
                RoleName = "General",
                Description = "Normal User"
            };

            User1.Roles.Add(Role1);
            User2.Roles.Add(Role2);
            User3.Roles.Add(Role3);

            context.Roles.AddOrUpdate(
                p => p.RoleName,
                Role1, Role2, Role3);

            context.Users.AddOrUpdate(
                p => p.Username,
                User1, User2, User3);
        }
    }
}
