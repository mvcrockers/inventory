namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomeBasicModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 100),
                        UnitId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: true)
                .Index(t => t.UnitId);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 100),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ItemCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 100),
                        ItemType = c.Int(nullable: false),
                        UnitPrice = c.Decimal(precision: 18, scale: 2),
                        PriceChangeDate = c.DateTime(),
                        ConversionFactor = c.Decimal(precision: 18, scale: 2),
                        LargerUnitId = c.Int(),
                        SmallerUnitId = c.Int(),
                        ItemCategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ItemCategories", t => t.ItemCategoryId)
                .ForeignKey("dbo.UnitOfMessures", t => t.LargerUnitId)
                .ForeignKey("dbo.UnitOfMessures", t => t.SmallerUnitId)
                .Index(t => t.ItemCategoryId)
                .Index(t => t.LargerUnitId)
                .Index(t => t.SmallerUnitId);
            
            CreateTable(
                "dbo.UnitOfMessures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Items", "SmallerUnitId", "dbo.UnitOfMessures");
            DropForeignKey("dbo.Items", "LargerUnitId", "dbo.UnitOfMessures");
            DropForeignKey("dbo.Items", "ItemCategoryId", "dbo.ItemCategories");
            DropForeignKey("dbo.Departments", "UnitId", "dbo.Units");
            DropIndex("dbo.Items", new[] { "SmallerUnitId" });
            DropIndex("dbo.Items", new[] { "LargerUnitId" });
            DropIndex("dbo.Items", new[] { "ItemCategoryId" });
            DropIndex("dbo.Departments", new[] { "UnitId" });
            DropTable("dbo.UnitOfMessures");
            DropTable("dbo.Items");
            DropTable("dbo.ItemCategories");
            DropTable("dbo.Units");
            DropTable("dbo.Departments");
        }
    }
}
