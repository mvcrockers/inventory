namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropStock : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Stocks", "ItemID", "dbo.Items");
            DropIndex("dbo.Stocks", new[] { "ItemID" });
            DropTable("dbo.Stocks");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Stocks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ItemID = c.Int(nullable: false),
                        StockQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateIndex("dbo.Stocks", "ItemID");
            AddForeignKey("dbo.Stocks", "ItemID", "dbo.Items", "Id", cascadeDelete: true);
        }
    }
}
