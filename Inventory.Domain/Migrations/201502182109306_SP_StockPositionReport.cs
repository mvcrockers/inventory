namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SP_StockPositionReport : DbMigration
    {
        public override void Up()
        {
            Sql(@"CREATE PROCEDURE [dbo].[spStockPositionReport]
                AS
	                SET NOCOUNT ON;
                SELECT items.Name AS ItemName, stk.UnitPrice, (CAST(CAST((stk.StockQty / stk.ConversionFactor) AS INT) AS VARCHAR(100)) + ' ' + luom.Name + ' ' + CAST(CAST((stk.StockQty % stk.ConversionFactor) AS INT) AS VARCHAR(100)) + ' ' + suom.Name) AS QtyWithUnit
                FROM Items items
	                INNER JOIN Stocks stk ON stk.ItemId = items.Id
	                INNER JOIN UnitOfMeasures suom ON suom.Id = stk.SmallerUnitId
	                INNER JOIN UnitOfMeasures luom ON luom.Id = stk.LargerUnitId");
        }
        
        public override void Down()
        {
            Sql(@"DROP PROCEDURE [dbo].[spStockPositionReport]");
        }
    }
}
