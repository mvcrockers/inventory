namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterStock : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "StockQty", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Items", "StockQty");
        }
    }
}
