namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DistributionChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductDistributionItems", "StockID", c => c.Int(nullable: false));
            AddColumn("dbo.ProductDistributionItems", "UnitPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.ProductDistributionItems", "TotalPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductDistributionItems", "TotalPrice");
            DropColumn("dbo.ProductDistributionItems", "UnitPrice");
            DropColumn("dbo.ProductDistributionItems", "StockID");
        }
    }
}
