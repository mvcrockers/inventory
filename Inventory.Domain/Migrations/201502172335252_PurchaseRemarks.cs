namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PurchaseRemarks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseReceives", "Remarks", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseReceives", "Remarks");
        }
    }
}
