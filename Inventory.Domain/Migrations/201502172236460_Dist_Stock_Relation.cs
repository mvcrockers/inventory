namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dist_Stock_Relation : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.ProductDistributionItems", "StockID");
            AddForeignKey("dbo.ProductDistributionItems", "StockID", "dbo.Stocks", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductDistributionItems", "StockID", "dbo.Stocks");
            DropIndex("dbo.ProductDistributionItems", new[] { "StockID" });
        }
    }
}
