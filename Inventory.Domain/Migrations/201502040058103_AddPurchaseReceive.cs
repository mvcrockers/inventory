namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPurchaseReceive : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PurchaseReceiveItems",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PurchaseReceiveId = c.Long(nullable: false),
                        SupplierId = c.Long(nullable: false),
                        ItemId = c.Long(nullable: false),
                        UnitOfMeasureId = c.Long(nullable: false),
                        ItemQty = c.Long(nullable: false),
                        ConversionFactor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Item_Id = c.Int(),
                        Supplier_Id = c.Int(),
                        UnitOfMeasure_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.Item_Id)
                .ForeignKey("dbo.PurchaseReceives", t => t.PurchaseReceiveId, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.Supplier_Id)
                .ForeignKey("dbo.UnitOfMeasures", t => t.UnitOfMeasure_Id)
                .Index(t => t.Item_Id)
                .Index(t => t.PurchaseReceiveId)
                .Index(t => t.Supplier_Id)
                .Index(t => t.UnitOfMeasure_Id);
            
            CreateTable(
                "dbo.PurchaseReceives",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PurchaseNo = c.String(),
                        SupplierId = c.Long(nullable: false),
                        PurchaseOrderID = c.Long(),
                        PurchaseOrderNo = c.String(),
                        ReceiveDate = c.DateTime(nullable: false),
                        Supplier_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderID)
                .ForeignKey("dbo.Suppliers", t => t.Supplier_Id)
                .Index(t => t.PurchaseOrderID)
                .Index(t => t.Supplier_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurchaseReceiveItems", "UnitOfMeasure_Id", "dbo.UnitOfMeasures");
            DropForeignKey("dbo.PurchaseReceiveItems", "Supplier_Id", "dbo.Suppliers");
            DropForeignKey("dbo.PurchaseReceives", "Supplier_Id", "dbo.Suppliers");
            DropForeignKey("dbo.PurchaseReceiveItems", "PurchaseReceiveId", "dbo.PurchaseReceives");
            DropForeignKey("dbo.PurchaseReceives", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseReceiveItems", "Item_Id", "dbo.Items");
            DropIndex("dbo.PurchaseReceiveItems", new[] { "UnitOfMeasure_Id" });
            DropIndex("dbo.PurchaseReceiveItems", new[] { "Supplier_Id" });
            DropIndex("dbo.PurchaseReceives", new[] { "Supplier_Id" });
            DropIndex("dbo.PurchaseReceiveItems", new[] { "PurchaseReceiveId" });
            DropIndex("dbo.PurchaseReceives", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseReceiveItems", new[] { "Item_Id" });
            DropTable("dbo.PurchaseReceives");
            DropTable("dbo.PurchaseReceiveItems");
        }
    }
}
