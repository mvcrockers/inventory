namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Purchase : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PurchaseReceiveItems", "ItemId", "dbo.Items");
            DropForeignKey("dbo.StockTranItems", "ItemID", "dbo.Items");
            DropForeignKey("dbo.StockTranItems", "LargerUnitId", "dbo.UnitOfMeasures");
            DropForeignKey("dbo.StockTranItems", "SmallerUnitId", "dbo.UnitOfMeasures");
            DropForeignKey("dbo.StockTranItems", "StockId", "dbo.Stocks");
            DropIndex("dbo.PurchaseReceiveItems", new[] { "ItemId" });
            DropIndex("dbo.StockTranItems", new[] { "ItemID" });
            DropIndex("dbo.StockTranItems", new[] { "LargerUnitId" });
            DropIndex("dbo.StockTranItems", new[] { "SmallerUnitId" });
            DropIndex("dbo.StockTranItems", new[] { "StockId" });
            AddColumn("dbo.PurchaseReceiveItems", "StockID", c => c.Int(nullable: false));
            AddColumn("dbo.StockTranItems", "QuantityInSmallerUnit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.PurchaseReceiveItems", "ConversionFactor", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.StockTranItems", "StockId", c => c.Int(nullable: false));
            CreateIndex("dbo.PurchaseReceiveItems", "StockID");
            CreateIndex("dbo.StockTranItems", "StockId");
            AddForeignKey("dbo.PurchaseReceiveItems", "StockID", "dbo.Stocks", "Id", cascadeDelete: true);
            AddForeignKey("dbo.StockTranItems", "StockId", "dbo.Stocks", "Id", cascadeDelete: true);
            DropColumn("dbo.PurchaseReceiveItems", "ItemId");
            DropColumn("dbo.PurchaseReceiveItems", "IsUnitLarger");
            DropColumn("dbo.PurchaseReceiveItems", "IsPriceInLargerUnit");
            DropColumn("dbo.StockTranItems", "ItemID");
            DropColumn("dbo.StockTranItems", "SmallerUnitId");
            DropColumn("dbo.StockTranItems", "LargerUnitId");
            DropColumn("dbo.StockTranItems", "ConversionFactor");
            DropColumn("dbo.StockTranItems", "IsPriceInLargerUnit");
            DropColumn("dbo.StockTranItems", "UnitPrice");
            DropColumn("dbo.StockTranItems", "Quantity");
        }
        
        public override void Down()
        {
            AddColumn("dbo.StockTranItems", "Quantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.StockTranItems", "UnitPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.StockTranItems", "IsPriceInLargerUnit", c => c.Boolean(nullable: false));
            AddColumn("dbo.StockTranItems", "ConversionFactor", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.StockTranItems", "LargerUnitId", c => c.Int());
            AddColumn("dbo.StockTranItems", "SmallerUnitId", c => c.Int());
            AddColumn("dbo.StockTranItems", "ItemID", c => c.Int(nullable: false));
            AddColumn("dbo.PurchaseReceiveItems", "IsPriceInLargerUnit", c => c.Boolean(nullable: false));
            AddColumn("dbo.PurchaseReceiveItems", "IsUnitLarger", c => c.Boolean(nullable: false));
            AddColumn("dbo.PurchaseReceiveItems", "ItemId", c => c.Int(nullable: false));
            DropForeignKey("dbo.StockTranItems", "StockId", "dbo.Stocks");
            DropForeignKey("dbo.PurchaseReceiveItems", "StockID", "dbo.Stocks");
            DropIndex("dbo.StockTranItems", new[] { "StockId" });
            DropIndex("dbo.PurchaseReceiveItems", new[] { "StockID" });
            AlterColumn("dbo.StockTranItems", "StockId", c => c.Int());
            AlterColumn("dbo.PurchaseReceiveItems", "ConversionFactor", c => c.Decimal(precision: 18, scale: 2));
            DropColumn("dbo.StockTranItems", "QuantityInSmallerUnit");
            DropColumn("dbo.PurchaseReceiveItems", "StockID");
            CreateIndex("dbo.StockTranItems", "StockId");
            CreateIndex("dbo.StockTranItems", "SmallerUnitId");
            CreateIndex("dbo.StockTranItems", "LargerUnitId");
            CreateIndex("dbo.StockTranItems", "ItemID");
            CreateIndex("dbo.PurchaseReceiveItems", "ItemId");
            AddForeignKey("dbo.StockTranItems", "StockId", "dbo.Stocks", "Id");
            AddForeignKey("dbo.StockTranItems", "SmallerUnitId", "dbo.UnitOfMeasures", "Id");
            AddForeignKey("dbo.StockTranItems", "LargerUnitId", "dbo.UnitOfMeasures", "Id");
            AddForeignKey("dbo.StockTranItems", "ItemID", "dbo.Items", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PurchaseReceiveItems", "ItemId", "dbo.Items", "Id", cascadeDelete: true);
        }
    }
}
