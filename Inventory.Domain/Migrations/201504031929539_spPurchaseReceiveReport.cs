namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class spPurchaseReceiveReport : DbMigration
    {
        public override void Up()
        {
            Sql(@"CREATE FUNCTION [dbo].[STRING_TO_INT] (@LIST_STRING VARCHAR(1000))
                    RETURNS @VALUE_TABLE TABLE (NUMBER VARCHAR(10) ) AS
                    BEGIN
                    DECLARE @POS      INT,
                            @NEXTPOS  INT,
                            @VALUELEN INT;
 
 
 
                    IF @LIST_STRING IS NOT NULL AND @LIST_STRING <>''
 	                BEGIN 
 	                SELECT @POS = 0, @NEXTPOS = 1;	
                    WHILE @NEXTPOS > 0
                        BEGIN
                            SELECT @NEXTPOS = CHARINDEX(',', @LIST_STRING, @POS + 1);

                            SELECT @VALUELEN = CASE
                                   	                WHEN @NEXTPOS > 0 THEN @NEXTPOS
                                   	                ELSE LEN(@LIST_STRING) + 1
                               	                END - @POS - 1;
       	                INSERT
         	                INTO @VALUE_TABLE
                                (NUMBER)
       	                VALUES (CONVERT(INT, SUBSTRING(@LIST_STRING, @POS + 1, @VALUELEN)));
     
       	                SELECT @POS = @NEXTPOS;
                    END
 
   
                    END
                    ELSE IF @LIST_STRING IS NULL OR @LIST_STRING =''
                BEGIN
                INSERT INTO @VALUE_TABLE
                                (NUMBER)
       	                VALUES (NULL);
                END 
                RETURN;
                END
                ");

            Sql(@"CREATE PROCEDURE [dbo].[spPurchaseReceiveInvoiceReport] 
                    -- Add the parameters for the stored procedure here
                    @purchaseReceiveIDs varchar(500)
	
                    AS
                    BEGIN
                    -- SET NOCOUNT ON added to prevent extra result sets from
                    -- interfering with SELECT statements.
                    SET NOCOUNT ON;

                    SELECT 
                    pr.PurchaseNo, 
                    pr.ReceiveDate,
                    i.Name AS [Description],
                    pri.ItemQty,
                    pri.Price
                    FROM PurchaseReceives AS pr
                    INNER JOIN PurchaseReceiveItems AS pri ON pri.PurchaseReceiveId = pr.Id
                    INNER JOIN Stocks AS s ON s.Id = pri.StockID
                    INNER JOIN Items AS i ON i.Id = s.ItemId
                    WHERE pr.Id IN (SELECT * from dbo.[STRING_TO_INT](@purchaseReceiveIDs))
                    END
                ");
        }
        
        public override void Down()
        {
            Sql(@"IF object_id(N'[dbo].[STRING_TO_INT]', N'FN') IS NOT NULL
                        DROP FUNCTION [dbo].[STRING_TO_INT]
                    GO");
            Sql(@"DROP PROCEDURE [dbo].[spPurchaseReceiveInvoiceReport]");
        }
    }
}
