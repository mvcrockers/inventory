namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class purchaseReceiveChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseReceives", "ReceivedStockType", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseReceives", "ReceivedStockType");
        }
    }
}
