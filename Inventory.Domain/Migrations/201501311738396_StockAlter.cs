namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StockAlter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StockTranItems", "TranID", c => c.Int(nullable: false));
            AddColumn("dbo.StockTranItems", "ParentTranNo", c => c.String());
            DropColumn("dbo.StockTranItems", "TranNo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.StockTranItems", "TranNo", c => c.String());
            DropColumn("dbo.StockTranItems", "ParentTranNo");
            DropColumn("dbo.StockTranItems", "TranID");
        }
    }
}
