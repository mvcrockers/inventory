namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SPDistribution : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                Create PROCEDURE spDistribution 
	                -- Add the parameters for the stored procedure here
	                @UnitID varchar(10),
	                @DepartmentID varchar(10),
	                @ItemID varchar(10)
	
                AS
                BEGIN
	                -- SET NOCOUNT ON added to prevent extra result sets from
	                -- interfering with SELECT statements.
	                SET NOCOUNT ON;

                    -- Insert statements for procedure here
                declare  @sql varchar (MAX)
                declare @where varchar (100)

                set @where =''


                if(@UnitID is Not Null)
                begin
                   set @where = @where +' where u.Id='+@UnitID 
                end

                if(@DepartmentID is Not Null)
                begin
                   if(@where != '')
                   begin 
		                set @where = @where +' and d.Id='+@DepartmentID 
                   end
                   else
                   begin
                     set @where = @where +' where d.Id='+@DepartmentID 
                   end
   
                end


                if(@ItemID is Not Null)
                begin
                   if(@where != '')
                   begin 
		                set @where = @where +' and i.Id='+@ItemID 
                   end
                   else
                   begin
                     set @where = @where +' where d.Id='+@ItemID 
                   end
   
                end


                set @sql ='SELECT u.Name AS Unit, d.Name AS Department, ic.Name AS Category, 
                       i.Name AS Item, pdi.TotalPrice AS Amount
                FROM   ProductDistributionItems AS pdi INNER JOIN
                Units AS u ON u.Id = pdi.UnitID INNER JOIN
                Departments AS d ON d.Id = pdi.DepartmentID INNER JOIN
                Items AS i ON i.Id = pdi.ItemId INNER JOIN
                ItemCategories AS ic ON i.ItemCategoryId = ic.Id INNER JOIN
                Stocks AS s ON s.ItemId = pdi.ItemId ' + @where


                EXEC (@sql)

                END
                
                ");
        }
        
        public override void Down()
        {
            Sql("Drop PROCEDURE spDistribution");
        }
    }
}
