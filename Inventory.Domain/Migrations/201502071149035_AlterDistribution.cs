namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterDistribution : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProductDistributionItems", "DepartmentID", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProductDistributionItems", "DepartmentID", c => c.Int(nullable: false));
        }
    }
}
