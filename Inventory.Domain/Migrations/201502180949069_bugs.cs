namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bugs : DbMigration
    {
        public override void Up()
        {
            this.Down();
            AddColumn("dbo.PurchaseReceives", "Remarks", c => c.String());
            CreateIndex("dbo.ProductDistributionItems", "StockID");
            AddForeignKey("dbo.ProductDistributionItems", "StockID", "dbo.Stocks", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductDistributionItems", "StockID", "dbo.Stocks");
            DropIndex("dbo.ProductDistributionItems", new[] { "StockID" });
            DropColumn("dbo.PurchaseReceives", "Remarks");
        }
    }
}
