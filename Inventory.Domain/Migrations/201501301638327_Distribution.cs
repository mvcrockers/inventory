namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Distribution : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductDistributions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TranDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProductDistributions");
        }
    }
}
