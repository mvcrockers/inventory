namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class itemModelUpdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Items", "Code", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Items", "Code", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
