namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterStockTran : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StockTranItems", "SmallerUnitId", "dbo.UnitOfMeasures");
            DropIndex("dbo.StockTranItems", new[] { "SmallerUnitId" });
            AlterColumn("dbo.StockTranItems", "SmallerUnitId", c => c.Int());
            CreateIndex("dbo.StockTranItems", "SmallerUnitId");
            AddForeignKey("dbo.StockTranItems", "SmallerUnitId", "dbo.UnitOfMeasures", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StockTranItems", "SmallerUnitId", "dbo.UnitOfMeasures");
            DropIndex("dbo.StockTranItems", new[] { "SmallerUnitId" });
            AlterColumn("dbo.StockTranItems", "SmallerUnitId", c => c.Int(nullable: false));
            CreateIndex("dbo.StockTranItems", "SmallerUnitId");
            AddForeignKey("dbo.StockTranItems", "SmallerUnitId", "dbo.UnitOfMeasures", "Id", cascadeDelete: true);
        }
    }
}
