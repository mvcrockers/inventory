namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        GroupID = c.Int(nullable: false, identity: true),
                        GroupName = c.String(nullable: false, maxLength: 100, unicode: false),
                        IsActive = c.Short(nullable: false),
                        InitTransferLogId = c.Int(),
                        TransferLogId = c.Int(),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedBy = c.Int(),
                        CreateDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        UserTypeID = c.Int(),
                    })
                .PrimaryKey(t => t.GroupID)
                .ForeignKey("dbo.UserType", t => t.UserTypeID)
                .Index(t => t.UserTypeID);

            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        PermissionID = c.Int(nullable: false, identity: true),
                        GroupID = c.Int(nullable: false),
                        PermissionKey = c.String(nullable: false, maxLength: 200, unicode: false),
                    })
                .PrimaryKey(t => t.PermissionID)
                .ForeignKey("dbo.Groups", t => t.GroupID)
                .Index(t => t.GroupID);

            CreateTable(
                "dbo.UserType",
                c => new
                    {
                        UserTypeID = c.Int(nullable: false),
                        Code = c.String(nullable: false, maxLength: 50, unicode: false),
                        Name = c.String(nullable: false, maxLength: 100, unicode: false),
                        TransferlogID = c.Int(),
                        InitTransferlogID = c.Int(),
                        CreatedBy = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserTypeID);

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Groups", "UserTypeID", "dbo.UserType");
            DropForeignKey("dbo.Permissions", "GroupID", "dbo.Groups");
            DropIndex("dbo.Groups", new[] { "UserTypeID" });
            DropIndex("dbo.Permissions", new[] { "GroupID" });
            DropTable("dbo.UserType");
            DropTable("dbo.Permissions");
            DropTable("dbo.Groups");
        }
    }
}
