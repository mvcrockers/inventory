namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewPurchaseReceive : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PurchaseReceiveItems",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PurchaseReceiveId = c.Long(nullable: false),
                        SupplierId = c.Int(nullable: false),
                        ItemId = c.Int(nullable: false),
                        UnitOfMeasureId = c.Int(nullable: false),
                        ItemQty = c.Long(nullable: false),
                        ConversionFactor = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: false)
                .ForeignKey("dbo.PurchaseReceives", t => t.PurchaseReceiveId, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: false)
                .ForeignKey("dbo.UnitOfMeasures", t => t.UnitOfMeasureId, cascadeDelete: false)
                .Index(t => t.ItemId)
                .Index(t => t.PurchaseReceiveId)
                .Index(t => t.SupplierId)
                .Index(t => t.UnitOfMeasureId);
            
            CreateTable(
                "dbo.PurchaseReceives",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PurchaseNo = c.String(),
                        SupplierId = c.Int(nullable: false),
                        PurchaseOrderID = c.Long(),
                        PurchaseOrderNo = c.String(),
                        ReceiveDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: false)
                .Index(t => t.PurchaseOrderID)
                .Index(t => t.SupplierId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurchaseReceiveItems", "UnitOfMeasureId", "dbo.UnitOfMeasures");
            DropForeignKey("dbo.PurchaseReceiveItems", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.PurchaseReceives", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.PurchaseReceiveItems", "PurchaseReceiveId", "dbo.PurchaseReceives");
            DropForeignKey("dbo.PurchaseReceives", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseReceiveItems", "ItemId", "dbo.Items");
            DropIndex("dbo.PurchaseReceiveItems", new[] { "UnitOfMeasureId" });
            DropIndex("dbo.PurchaseReceiveItems", new[] { "SupplierId" });
            DropIndex("dbo.PurchaseReceives", new[] { "SupplierId" });
            DropIndex("dbo.PurchaseReceiveItems", new[] { "PurchaseReceiveId" });
            DropIndex("dbo.PurchaseReceives", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseReceiveItems", new[] { "ItemId" });
            DropTable("dbo.PurchaseReceives");
            DropTable("dbo.PurchaseReceiveItems");
        }
    }
}
