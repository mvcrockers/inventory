namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Triggers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                    CREATE TRIGGER trig_ItemInsert
                    ON Items
                    AFTER INSERT
                    AS

                    UPDATE Items
                    SET Code = (SELECT REPLICATE('0', 4 - LEN(Id)) + CAST(Id AS varchar) FROM INSERTED)
                    FROM INSERTED WHERE Items.Id = INSERTED.Id");

            Sql(@"
                    CREATE TRIGGER [dbo].[trig_DistributionInsert]
                    ON [dbo].[ProductDistributions]
                    AFTER INSERT
                    AS

                    UPDATE ProductDistributions
                    SET InvoiceNo = (SELECT REPLICATE('0', 4 - LEN(Id)) + CAST(Id AS varchar) FROM INSERTED)
                    FROM INSERTED WHERE ProductDistributions.Id = INSERTED.Id");

        }
        
        public override void Down()
        {
            Sql("DROP TRIGGER trig_ItemInsert");
            Sql("DROP TRIGGER [trig_DistributionInsert]");
        }
    }
}
