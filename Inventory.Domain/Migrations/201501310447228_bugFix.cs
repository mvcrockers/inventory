namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bugFix : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.ProductDistributions");
            CreateTable(
                "dbo.ProductDistributions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        InvoiceNo = c.String(),
                        TranDate = c.DateTime(nullable: false),
                        Remarks = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProductDistributions");
        }
    }
}
