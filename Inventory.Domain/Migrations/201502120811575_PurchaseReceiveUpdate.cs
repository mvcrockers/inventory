namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PurchaseReceiveUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseReceiveItems", "IsUnitLarger", c => c.Boolean(nullable: false,defaultValue:false));
            AddColumn("dbo.PurchaseReceiveItems", "IsPriceInLargerUnit", c => c.Boolean(nullable: false,defaultValue:false));
            AddColumn("dbo.PurchaseReceiveItems", "UnitPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2,defaultValue: 0));
            AddColumn("dbo.PurchaseReceiveItems", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AlterColumn("dbo.PurchaseReceiveItems", "ConversionFactor", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PurchaseReceiveItems", "ConversionFactor", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.PurchaseReceiveItems", "Price");
            DropColumn("dbo.PurchaseReceiveItems", "UnitPrice");
            DropColumn("dbo.PurchaseReceiveItems", "IsPriceInLargerUnit");
            DropColumn("dbo.PurchaseReceiveItems", "IsUnitLarger");
        }
    }
}
