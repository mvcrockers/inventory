namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PurchaseOrderModified : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseOrderItems", "UnitOfMeasureID", c => c.Int(nullable: false));
            AddColumn("dbo.PurchaseOrderItems", "ConversionFactor", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.PurchaseOrders", "ReceivedDate", c => c.DateTime());
            CreateIndex("dbo.PurchaseOrderItems", "UnitOfMeasureID");
            AddForeignKey("dbo.PurchaseOrderItems", "UnitOfMeasureID", "dbo.UnitOfMeasures", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurchaseOrderItems", "UnitOfMeasureID", "dbo.UnitOfMeasures");
            DropIndex("dbo.PurchaseOrderItems", new[] { "UnitOfMeasureID" });
            AlterColumn("dbo.PurchaseOrders", "ReceivedDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.PurchaseOrderItems", "ConversionFactor");
            DropColumn("dbo.PurchaseOrderItems", "UnitOfMeasureID");
        }
    }
}
