namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PurchaseOrderAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PurchaseOrderItems",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        PurchaseOrderID = c.Long(nullable: false),
                        SupplierID = c.Int(nullable: false),
                        ItemID = c.Int(nullable: false),
                        ItemQty = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Items", t => t.ItemID, cascadeDelete: false)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderID, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID, cascadeDelete: false)
                .Index(t => t.ItemID)
                .Index(t => t.PurchaseOrderID)
                .Index(t => t.SupplierID);
            
            CreateTable(
                "dbo.PurchaseOrders",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        OrderDate = c.DateTime(nullable: false),
                        ExpectedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurchaseOrderItems", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.PurchaseOrderItems", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrderItems", "ItemID", "dbo.Items");
            DropIndex("dbo.PurchaseOrderItems", new[] { "SupplierID" });
            DropIndex("dbo.PurchaseOrderItems", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrderItems", new[] { "ItemID" });
            DropTable("dbo.PurchaseOrders");
            DropTable("dbo.PurchaseOrderItems");
        }
    }
}
