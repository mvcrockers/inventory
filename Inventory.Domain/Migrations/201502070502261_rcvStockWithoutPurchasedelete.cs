namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rcvStockWithoutPurchasedelete : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ReceivedStockItems", "ItemId", "dbo.Items");
            DropForeignKey("dbo.ReceivedStockItems", "ReceivedStockId", "dbo.ReceivedStocks");
            DropForeignKey("dbo.ReceivedStockItems", "UnitOfMeasureId", "dbo.UnitOfMeasures");
            DropIndex("dbo.ReceivedStockItems", new[] { "ItemId" });
            DropIndex("dbo.ReceivedStockItems", new[] { "ReceivedStockId" });
            DropIndex("dbo.ReceivedStockItems", new[] { "UnitOfMeasureId" });
            DropTable("dbo.ReceivedStocks");
            DropTable("dbo.ReceivedStockItems");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ReceivedStockItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitOfMeasureId = c.Int(nullable: false),
                        ReceivedStockId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReceivedStocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VoucherNo = c.String(nullable: false),
                        ReceiptDate = c.DateTime(nullable: false),
                        VoucherCode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.ReceivedStockItems", "UnitOfMeasureId");
            CreateIndex("dbo.ReceivedStockItems", "ReceivedStockId");
            CreateIndex("dbo.ReceivedStockItems", "ItemId");
            AddForeignKey("dbo.ReceivedStockItems", "UnitOfMeasureId", "dbo.UnitOfMeasures", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ReceivedStockItems", "ReceivedStockId", "dbo.ReceivedStocks", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ReceivedStockItems", "ItemId", "dbo.Items", "Id", cascadeDelete: true);
        }
    }
}
