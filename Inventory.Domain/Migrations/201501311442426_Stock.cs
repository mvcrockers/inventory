namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Stock : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Stocks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ItemID = c.Int(nullable: false),
                        StockQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Items", t => t.ItemID, cascadeDelete: true)
                .Index(t => t.ItemID);
            
            CreateTable(
                "dbo.StockTranItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TransactionType = c.Short(nullable: false),
                        TranNo = c.String(),
                        Trandate = c.DateTime(nullable: false),
                        ParentTranID = c.Int(nullable: false),
                        ItemID = c.Int(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TranSide = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Items", t => t.ItemID, cascadeDelete: true)
                .Index(t => t.ItemID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StockTranItems", "ItemID", "dbo.Items");
            DropForeignKey("dbo.Stocks", "ItemID", "dbo.Items");
            DropIndex("dbo.StockTranItems", new[] { "ItemID" });
            DropIndex("dbo.Stocks", new[] { "ItemID" });
            DropTable("dbo.StockTranItems");
            DropTable("dbo.Stocks");
        }
    }
}
