namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class receivedStockModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StockReceiveWithoutPurchases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitOfMeasureId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: false)
                .ForeignKey("dbo.UnitOfMeasures", t => t.UnitOfMeasureId, cascadeDelete: false)
                .Index(t => t.ItemId)
                .Index(t => t.UnitOfMeasureId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StockReceiveWithoutPurchases", "UnitOfMeasureId", "dbo.UnitOfMeasures");
            DropForeignKey("dbo.StockReceiveWithoutPurchases", "ItemId", "dbo.Items");
            DropIndex("dbo.StockReceiveWithoutPurchases", new[] { "UnitOfMeasureId" });
            DropIndex("dbo.StockReceiveWithoutPurchases", new[] { "ItemId" });
            DropTable("dbo.StockReceiveWithoutPurchases");
        }
    }
}
