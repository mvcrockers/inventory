namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MenuModelAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Menus",
                c => new
                    {
                        MenuId = c.Int(nullable: false, identity: true),
                        MenuName = c.String(nullable: false),
                        URI = c.String(nullable: false),
                        RoleId = c.Int(nullable: false),
                        ParentMenuId = c.Int(),
                    })
                .PrimaryKey(t => t.MenuId)
                .ForeignKey("dbo.Menus", t => t.ParentMenuId)
                .Index(t => t.ParentMenuId);
            
            CreateTable(
                "dbo.MenuRoles",
                c => new
                    {
                        MenuId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MenuId, t.RoleId })
                .ForeignKey("dbo.Menus", t => t.MenuId, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.MenuId)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MenuRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.MenuRoles", "MenuId", "dbo.Menus");
            DropForeignKey("dbo.Menus", "ParentMenuId", "dbo.Menus");
            DropIndex("dbo.MenuRoles", new[] { "RoleId" });
            DropIndex("dbo.MenuRoles", new[] { "MenuId" });
            DropIndex("dbo.Menus", new[] { "ParentMenuId" });
            DropTable("dbo.MenuRoles");
            DropTable("dbo.Menus");
        }
    }
}
