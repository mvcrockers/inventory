namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UnitOfMeasureSpelling : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Items", "SmallerUnitId", "dbo.UnitOfMessures");
            DropForeignKey("dbo.Items", "LargerUnitId", "dbo.UnitOfMessures");
            DropTable("dbo.UnitOfMessures");

            CreateTable(
                "dbo.UnitOfMeasures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);

           
        }
        
        public override void Down()
        {
            DropTable("dbo.UnitOfMeasures");

            CreateTable(
                "dbo.UnitOfMessures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            
        }
    }
}
