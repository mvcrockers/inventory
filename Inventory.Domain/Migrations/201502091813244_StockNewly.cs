namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StockNewly : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Stocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        SmallerUnitId = c.Int(nullable: false),
                        LargerUnitId = c.Int(),
                        ConversionFactor = c.Decimal(precision: 18, scale: 2),
                        IsPriceInLargerUnit = c.Boolean(nullable: false),
                        UnitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StockQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: false)
                .ForeignKey("dbo.UnitOfMeasures", t => t.LargerUnitId)
                .ForeignKey("dbo.UnitOfMeasures", t => t.SmallerUnitId, cascadeDelete: false)
                .Index(t => t.ItemId)
                .Index(t => t.LargerUnitId)
                .Index(t => t.SmallerUnitId);
            
            AddColumn("dbo.Items", "IsPriceInLargerUnit", c => c.Boolean(nullable: false));
            AddColumn("dbo.StockTranItems", "StockId", c => c.Int());
            AddColumn("dbo.StockTranItems", "SmallerUnitId", c => c.Int(nullable: false));
            AddColumn("dbo.StockTranItems", "LargerUnitId", c => c.Int());
            AddColumn("dbo.StockTranItems", "ConversionFactor", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.StockTranItems", "IsPriceInLargerUnit", c => c.Boolean(nullable: false));
            AddColumn("dbo.StockTranItems", "UnitPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            CreateIndex("dbo.StockTranItems", "LargerUnitId");
            CreateIndex("dbo.StockTranItems", "SmallerUnitId");
            CreateIndex("dbo.StockTranItems", "StockId");
            AddForeignKey("dbo.StockTranItems", "LargerUnitId", "dbo.UnitOfMeasures", "Id");
            AddForeignKey("dbo.StockTranItems", "SmallerUnitId", "dbo.UnitOfMeasures", "Id", cascadeDelete: false);
            AddForeignKey("dbo.StockTranItems", "StockId", "dbo.Stocks", "Id");
            DropColumn("dbo.Items", "StockQty");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Items", "StockQty", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropForeignKey("dbo.StockTranItems", "StockId", "dbo.Stocks");
            DropForeignKey("dbo.StockTranItems", "SmallerUnitId", "dbo.UnitOfMeasures");
            DropForeignKey("dbo.StockTranItems", "LargerUnitId", "dbo.UnitOfMeasures");
            DropForeignKey("dbo.Stocks", "SmallerUnitId", "dbo.UnitOfMeasures");
            DropForeignKey("dbo.Stocks", "LargerUnitId", "dbo.UnitOfMeasures");
            DropForeignKey("dbo.Stocks", "ItemId", "dbo.Items");
            DropIndex("dbo.StockTranItems", new[] { "StockId" });
            DropIndex("dbo.StockTranItems", new[] { "SmallerUnitId" });
            DropIndex("dbo.StockTranItems", new[] { "LargerUnitId" });
            DropIndex("dbo.Stocks", new[] { "SmallerUnitId" });
            DropIndex("dbo.Stocks", new[] { "LargerUnitId" });
            DropIndex("dbo.Stocks", new[] { "ItemId" });
            DropColumn("dbo.StockTranItems", "UnitPrice");
            DropColumn("dbo.StockTranItems", "IsPriceInLargerUnit");
            DropColumn("dbo.StockTranItems", "ConversionFactor");
            DropColumn("dbo.StockTranItems", "LargerUnitId");
            DropColumn("dbo.StockTranItems", "SmallerUnitId");
            DropColumn("dbo.StockTranItems", "StockId");
            DropColumn("dbo.Items", "IsPriceInLargerUnit");
            DropTable("dbo.Stocks");
        }
    }
}
