namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _SPDistributionAlter : DbMigration
    {
        public override void Up()
        {
            Sql(@"ALTER PROCEDURE [dbo].[spDistribution] 
	                    -- Add the parameters for the stored procedure here
	                    @UnitID INT,
	                    @DepartmentID INT,
	                    @ItemCategoryID INT,
	                    @fromDate DATETIME,
	                    @toDate DATETIME
                    AS
                    BEGIN
	                    -- SET NOCOUNT ON added to prevent extra result sets from
	                    -- interfering with SELECT statements.
	                    SET NOCOUNT ON;

                        -- Insert statements for procedure here
                    declare  @sql varchar (MAX)
                    declare @where varchar (100)

                    set @where =''


                    if(@UnitID > 0)
                    begin
                        set @where = @where +' where u.Id='+CAST(@UnitID AS VARCHAR) 
                    end

                    if(@DepartmentID > 0)
                    begin
                        if(@where != '')
                        begin 
		                    set @where = @where +' and d.Id='+CAST(@DepartmentID AS VARCHAR)
                        end
                        else
                        begin
                            set @where = @where +' where d.Id='+CAST(@DepartmentID AS VARCHAR) 
                        end
   
                    end


                    if(@ItemCategoryID > 0)
                    begin
                        if(@where != '')
                        begin 
		                    set @where = @where +' and ic.Id='+CAST(@ItemCategoryID AS VARCHAR) 
                        end
                        else
                        begin
                            set @where = @where +' where ic.Id='+CAST(@ItemCategoryID AS VARCHAR)
                        end
   
                    END

                    if(@fromDate IS NOT NULL)
                    begin
                        if(@where != '')
                        begin 
		                    set @where = @where +' and pd.TranDate>='''+CAST(CAST(@fromDate AS DATE) AS VARCHAR)+''''
                        end
                        else
                        begin
                            set @where = @where +' where pd.TranDate>='''+CAST(CAST(@fromDate AS DATE) AS VARCHAR)+''''  
                        end
   
                    END

                    if(@toDate IS NOT NULL)
                    begin
                        if(@where != '')
                        begin 
		                    set @where = @where +' and pd.TranDate<='''+CAST(CAST(@toDate AS DATE) AS VARCHAR)+''''
                        end
                        else
                        begin
                            set @where = @where +' where pd.TranDate<='''+CAST(CAST(@toDate AS DATE) AS VARCHAR)+'''' 
                        end
   
                    end


                    set @sql ='SELECT 
			                    u.Name AS Unit, 
			                    d.Name AS Department,
			                    ic.Name AS Category, 
			                    i.Name AS Item, 
			                    pdi.Quantity * s.UnitPrice AS Amount
			                    FROM  ProductDistributionItems AS pdi
			                    INNER JOIN ProductDistributions AS pd ON pd.ID = pdi.ProductDistributionId 
			                    INNER JOIN Units AS u ON u.Id = pdi.UnitID 
			                    INNER JOIN Departments AS d ON d.Id = pdi.DepartmentID 
			                    INNER JOIN Items AS i ON i.Id = pdi.ItemId 
			                    INNER JOIN ItemCategories AS ic ON ic.Id = i.ItemCategoryId
			                    INNER JOIN Stocks AS s ON s.ItemId = pdi.ItemId ' + @where


                    EXEC (@sql)

                    END
                
                ");
        }
        
        public override void Down()
        {
            Sql(@"
                ALTER PROCEDURE spDistribution 
	                -- Add the parameters for the stored procedure here
	                @UnitID varchar(10),
	                @DepartmentID varchar(10),
	                @ItemID varchar(10)
	
                AS
                BEGIN
	                -- SET NOCOUNT ON added to prevent extra result sets from
	                -- interfering with SELECT statements.
	                SET NOCOUNT ON;

                    -- Insert statements for procedure here
                declare  @sql varchar (MAX)
                declare @where varchar (100)

                set @where =''


                if(@UnitID is Not Null)
                begin
                   set @where = @where +' where u.Id='+@UnitID 
                end

                if(@DepartmentID is Not Null)
                begin
                   if(@where != '')
                   begin 
		                set @where = @where +' and d.Id='+@DepartmentID 
                   end
                   else
                   begin
                     set @where = @where +' where d.Id='+@DepartmentID 
                   end
   
                end


                if(@ItemID is Not Null)
                begin
                   if(@where != '')
                   begin 
		                set @where = @where +' and i.Id='+@ItemID 
                   end
                   else
                   begin
                     set @where = @where +' where d.Id='+@ItemID 
                   end
   
                end


                set @sql ='SELECT u.Name AS Unit, d.Name AS Department, ic.Name AS Category, 
                       i.Name AS Item, pdi.Quantity * s.UnitPrice AS Amount
                FROM   ProductDistributionItems AS pdi INNER JOIN
                Units AS u ON u.Id = pdi.UnitID INNER JOIN
                Departments AS d ON d.Id = pdi.DepartmentID INNER JOIN
                Items AS i ON i.Id = pdi.ItemId INNER JOIN
                ItemCategories AS ic ON i.ItemCategoryId = ic.Id INNER JOIN
                Stocks AS s ON s.ItemId = pdi.ItemId ' + @where


                EXEC (@sql)

                END
                
                ");
        }
    }
}
