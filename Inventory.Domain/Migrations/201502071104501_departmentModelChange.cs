namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class departmentModelChange : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Departments", "UnitId", "dbo.Units");
            DropIndex("dbo.Departments", new[] { "UnitId" });
            CreateTable(
                "dbo.UnitDepartments",
                c => new
                    {
                        Unit_Id = c.Int(nullable: false),
                        Department_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Unit_Id, t.Department_Id })
                .ForeignKey("dbo.Units", t => t.Unit_Id, cascadeDelete: true)
                .ForeignKey("dbo.Departments", t => t.Department_Id, cascadeDelete: true)
                .Index(t => t.Unit_Id)
                .Index(t => t.Department_Id);
            
            DropColumn("dbo.Departments", "UnitId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Departments", "UnitId", c => c.Int(nullable: false));
            DropForeignKey("dbo.UnitDepartments", "Department_Id", "dbo.Departments");
            DropForeignKey("dbo.UnitDepartments", "Unit_Id", "dbo.Units");
            DropIndex("dbo.UnitDepartments", new[] { "Department_Id" });
            DropIndex("dbo.UnitDepartments", new[] { "Unit_Id" });
            DropTable("dbo.UnitDepartments");
            CreateIndex("dbo.Departments", "UnitId");
            AddForeignKey("dbo.Departments", "UnitId", "dbo.Units", "Id", cascadeDelete: true);
        }
    }
}
