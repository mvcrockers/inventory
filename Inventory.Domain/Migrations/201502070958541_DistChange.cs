namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DistChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductDistributionItems", "UnitOfMeasureID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductDistributionItems", "UnitOfMeasureID");
        }
    }
}
