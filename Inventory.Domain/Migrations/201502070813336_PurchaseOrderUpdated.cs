namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PurchaseOrderUpdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseOrders", "OrderCode", c => c.String());
            AddColumn("dbo.PurchaseOrders", "ReceivedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.PurchaseOrders", "IsReceived", c => c.Boolean(nullable: false));
            DropColumn("dbo.PurchaseOrders", "ExpectedDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PurchaseOrders", "ExpectedDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.PurchaseOrders", "IsReceived");
            DropColumn("dbo.PurchaseOrders", "ReceivedDate");
            DropColumn("dbo.PurchaseOrders", "OrderCode");
        }
    }
}
