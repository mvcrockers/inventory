namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Relations : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.ProductDistributionItems", "DepartmentID");
            CreateIndex("dbo.ProductDistributionItems", "UnitID");
            CreateIndex("dbo.ProductDistributionItems", "UnitOfMeasureID");
            AddForeignKey("dbo.ProductDistributionItems", "DepartmentID", "dbo.Departments", "Id");
            AddForeignKey("dbo.ProductDistributionItems", "UnitID", "dbo.Units", "Id", cascadeDelete: false);
            AddForeignKey("dbo.ProductDistributionItems", "UnitOfMeasureID", "dbo.UnitOfMeasures", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductDistributionItems", "UnitOfMeasureID", "dbo.UnitOfMeasures");
            DropForeignKey("dbo.ProductDistributionItems", "UnitID", "dbo.Units");
            DropForeignKey("dbo.ProductDistributionItems", "DepartmentID", "dbo.Departments");
            DropIndex("dbo.ProductDistributionItems", new[] { "UnitOfMeasureID" });
            DropIndex("dbo.ProductDistributionItems", new[] { "UnitID" });
            DropIndex("dbo.ProductDistributionItems", new[] { "DepartmentID" });
        }
    }
}
