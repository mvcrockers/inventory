namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeighnKey : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("dbo.Items", "LargerUnitId", "UnitOfMeasures");
            AddForeignKey("dbo.Items", "SmallerUnitId", "UnitOfMeasures");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Items", "SmallerUnitId", "dbo.UnitOfMessures");
            DropForeignKey("dbo.Items", "LargerUnitId", "dbo.UnitOfMessures");
           
        }
    }
}
