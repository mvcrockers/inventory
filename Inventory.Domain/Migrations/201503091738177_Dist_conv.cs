namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dist_conv : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductDistributionItems", "ConversionFactor", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductDistributionItems", "ConversionFactor");
        }
    }
}
