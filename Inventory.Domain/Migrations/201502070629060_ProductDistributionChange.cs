namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductDistributionChange : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ProductDistributionItems", "Price");
            DropColumn("dbo.ProductDistributionItems", "UnitOfMeasureId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductDistributionItems", "UnitOfMeasureId", c => c.Int(nullable: false));
            AddColumn("dbo.ProductDistributionItems", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
