namespace Inventory.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeInItem : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Items", "SmallerUnitId", "dbo.UnitOfMeasures");
            DropIndex("dbo.Items", new[] { "SmallerUnitId" });
            AlterColumn("dbo.Items", "SmallerUnitId", c => c.Int(nullable: false));
            CreateIndex("dbo.Items", "SmallerUnitId");
            AddForeignKey("dbo.Items", "SmallerUnitId", "dbo.UnitOfMeasures", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Items", "SmallerUnitId", "dbo.UnitOfMeasures");
            DropIndex("dbo.Items", new[] { "SmallerUnitId" });
            AlterColumn("dbo.Items", "SmallerUnitId", c => c.Int());
            CreateIndex("dbo.Items", "SmallerUnitId");
            AddForeignKey("dbo.Items", "SmallerUnitId", "dbo.UnitOfMeasures", "Id");
        }
    }
}
