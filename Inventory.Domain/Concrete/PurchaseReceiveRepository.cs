﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
   public class PurchaseReceiveRepository: IPurchaseReceiveRepository
    {
        private TMContext context = null;

        public PurchaseReceiveRepository(TMContext context)
        {
            this.context = context;
        }
        public IQueryable<PurchaseReceive> GetPurchaseReceives()
        {
            return context.PurchaseReceives.Include("Supplier")
                .Include(x=>x.PurchaseReceiveItems.Select(s=>s.Stock.Item));
            //return new List<PurchaseReceive>() as IQueryable<PurchaseReceive>;
        }

        public PurchaseReceive GetPurchaseByID(long purchaseReceiveID)
        {
            return context.PurchaseReceives.Include("Supplier").FirstOrDefault(x=>x.Id == purchaseReceiveID);
            //return new PurchaseReceive();
        }

        public PurchaseReceive InsertPurchaseReceive(PurchaseReceive oPurchaseReceive)
        {
 	       
            if (context.PurchaseReceives.Any(pr => pr.PurchaseNo.Trim().ToLower() == oPurchaseReceive.PurchaseNo.Trim().ToLower()))
            {
                throw new Exception("There is another Receipt with this 'Receipt No'.Please try with another one.");
            }
            if (oPurchaseReceive.PurchaseReceiveItems.Count==0)
            {
                throw new Exception("There is another Receipt with this 'Receipt No'.Please try with another one.");
            }

            context.PurchaseReceives.Add(oPurchaseReceive);

            context.SaveChanges();

            return oPurchaseReceive;

        }

        public PurchaseReceive UpdatePurchaseReceive(PurchaseReceive oNewParent)
        {
            PurchaseReceive OriginalParent = context.PurchaseReceives.Where(x => x.Id == oNewParent.Id).Include(p=>p.PurchaseReceiveItems).SingleOrDefault();
            if (OriginalParent != null)
            {
                if (context.PurchaseReceives.Any(x => x.Id != oNewParent.Id && x.PurchaseNo.Trim().ToLower() == oNewParent.PurchaseNo.Trim().ToLower()))
                {
                    throw new Exception("There is another Receipt with this 'Receipt No'.Please try with another one.");
                }

                var parentEntry = context.Entry(OriginalParent);
                parentEntry.CurrentValues.SetValues(oNewParent);


                foreach (var childItem in oNewParent.PurchaseReceiveItems)
                {
                    var originalChildItem = OriginalParent.PurchaseReceiveItems.SingleOrDefault(c => c.Id == childItem.Id && c.Id != 0);
                    // Is original child item with same ID in DB?
                    if (originalChildItem != null)
                    {
                        // Yes -> Update scalar properties of child item
                        var childEntry = context.Entry(originalChildItem);
                        childEntry.CurrentValues.SetValues(childItem);
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert
                        childItem.Id = 0;
                        OriginalParent.PurchaseReceiveItems.Add(childItem);
                    }
                }

                foreach (var originalChildItem in OriginalParent.PurchaseReceiveItems.Where(c => c.Id != 0).ToList())
                {
                    // Are there child items in the DB which are NOT in the
                    // new child item collection anymore?
                    if (!oNewParent.PurchaseReceiveItems.Any(c => c.Id == originalChildItem.Id))
                        // Yes -> It's a deleted child item -> Delete
                        context.PurchaseReceiveItems.Remove(originalChildItem);
                }

                
                context.SaveChanges();

                
            }
            else
            {
                throw new Exception("This Receipt doesn't belong to this database.");
            }

            return OriginalParent;
        }


        public void DeletePurchaseReceive(long purchaseReceiveID)
        {
            PurchaseReceive pReceive = context.PurchaseReceives.Find(purchaseReceiveID);
            context.PurchaseReceives.Remove(pReceive);
        }
    }
}
