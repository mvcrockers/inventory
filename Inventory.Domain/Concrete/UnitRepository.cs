﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
   public class UnitRepository: IUnitRepository
    {
        private TMContext context = null;

        public UnitRepository(TMContext context)
        {
            this.context = context;
        }
        

        public IQueryable<Unit> GetUnits()
        {
            return context.Units;
        }

        public Unit GetUnitByID(int unitId)
        {
            return context.Units.Find(unitId);
        }

        public void InsertUnit(Unit oUnit)
        {
            if(context.Units.Where(x => x.Code.Trim().ToLower() == oUnit.Code.Trim().ToLower()).Any())
            {
                throw new Exception("Unit with this code exists.");
            }
            if (context.Units.Where(x => x.Name.Trim().ToLower() == oUnit.Name.Trim().ToLower()).Any())
            {
                throw new Exception("Unit with this name exists.");
            }
            context.Units.Add(oUnit);
        }

        public void DeleteUnit(int unitId)
        {
            Unit unit = context.Units.Find(unitId);
            context.Units.Remove(unit);
        }

        public void UpdateUnit(Unit oUnit)
        {
            if (context.Units.Where(x => x.Code.Trim().ToLower() == oUnit.Code.Trim().ToLower() && x.Id != oUnit.Id).Any())
            {
                throw new Exception("Another Unit with this code exists.");
            }
            if (context.Units.Where(x => x.Name.Trim().ToLower() == oUnit.Name.Trim().ToLower() && x.Id!=oUnit.Id).Any())
            {
                throw new Exception("Another Unit with this name exists.");
            }

            context.Entry(oUnit).State = EntityState.Modified;
        }
    }
}
