﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
   public class RoleRepository: IRoleRepository
    {
        private TMContext context = null;

        public RoleRepository(TMContext context)
        {
            this.context = context;
        }
        

        public IQueryable<Role> GetRoles()
        {
            return context.Roles;
        }

        public Role GetRoleByID(int RoleId)
        {
            return context.Roles.Find(RoleId);
        }

        public void InsertRole(Role oRole)
        {
            context.Roles.Add(oRole);
        }

        public void DeleteRole(int RoleId)
        {
            Role Role = context.Roles.Find(RoleId);
            context.Roles.Remove(Role);
        }

        public void UpdateRole(Role oRole)
        {
            context.Entry(oRole).State = EntityState.Modified;
        }
    }
}
