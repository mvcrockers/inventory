﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
   public class PermissionRepository: IPermissionRepository
    {
       private TMContext context = new TMContext();

        public void DeletePermissionByGroup(int GroupID)
        {
            IEnumerable<Permission> oPermission = context.Permissions.Where(x=>x.GroupID==GroupID);
            context.Permissions.RemoveRange(oPermission);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
