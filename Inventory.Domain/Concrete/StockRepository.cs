﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain.Concrete
{

    internal class StockRepository : IStockRepository
    {
        private TMContext context = null;

        public StockRepository(TMContext context)
        {
            this.context = context;
        }

        #region Get Functions
        public Stock GetStock(int ItemId, int? smallerUnitId, int? largerUnitId, decimal unitPrice, bool IsPriceInLargerUnit, decimal? ConversionFactor)
        {
            Stock oStock = null;

            oStock = context.Stocks.FirstOrDefault(x => x.ItemId == ItemId && x.SmallerUnitId == smallerUnitId && x.LargerUnitId == largerUnitId
                                           && x.UnitPrice == unitPrice && x.ConversionFactor == ConversionFactor && x.IsPriceInLargerUnit == IsPriceInLargerUnit);

            return oStock;
        }
        
        public IEnumerable<Stock> GetStocks(int itemId)
        {
            return context.Stocks.Where(x => x.ItemId == itemId).Include(x => x.SmallerUnit).Include(x => x.LargerUnit);
        }
        public Stock GetStock(int id)
        {
            return context.Stocks.Find(id);
        }

        #endregion
        public void CreateStock(Item oItem)
        {
            Stock oStock = GetStock(oItem.Id, oItem.SmallerUnitId, oItem.LargerUnitId, oItem.UnitPrice, oItem.IsPriceInLargerUnit, oItem.ConversionFactor);
            if (oStock == null)
            {
                context.Stocks.Add(new Stock()
                {
                    ItemId = oItem.Id,
                    SmallerUnitId = oItem.SmallerUnitId,
                    LargerUnitId = oItem.LargerUnitId,
                    UnitPrice = oItem.UnitPrice,
                    StockQty = 0,
                    IsPriceInLargerUnit = oItem.IsPriceInLargerUnit,
                    ConversionFactor = oItem.ConversionFactor

                });
            }
        }
        public void Insert(ICollection<StockTranItem> Trans)
        {
            foreach (StockTranItem tran in Trans)
            {
                Stock oStock = context.Stocks.Find(tran.StockId);

                // If there is no stock for that item throw Error
                if (oStock == null)
                {
                    throw new Exception(string.Format("Could Not Find Stock for an Item!"));
                }

                oStock.StockQty += tran.TranSide == TranSideEnum.Increase ? tran.QuantityInSmallerUnit : tran.QuantityInSmallerUnit * (-1);

                if (oStock.StockQty < 0)
                    throw new Exception("Cannot distribute,Not Enough Stock for the Item!");

                context.Entry(oStock).State = EntityState.Modified;
            }
            context.StockTranItems.AddRange(Trans.AsEnumerable());
        }

        public void Update(ICollection<StockTranItem> Trans)
        {
            //Add New TranItems to Stock
            foreach (StockTranItem tran in Trans)
            {
                Stock Stock = context.Stocks.Find(tran.StockId);

                if (Stock == null)
                    throw new Exception("Stock Not Found for the Item");


                Stock.StockQty += tran.TranSide == TranSideEnum.Increase ? tran.QuantityInSmallerUnit : tran.QuantityInSmallerUnit * (-1);
                context.Entry(Stock).State = EntityState.Modified;
            }

            StockTranItem firstTransaction = Trans.First();
            List<StockTranItem> prevTrans = context.StockTranItems.Where
                                                    (
                                                        x => x.TransactionType == firstTransaction.TransactionType
                                                        && x.ParentTranID == firstTransaction.ParentTranID
                                                    ).ToList();
            // Reverse prev trans from Stock
            foreach (var tran in prevTrans)
            {
                Stock Stock = context.Stocks.Find(tran.StockId);

                if (Stock == null)
                    throw new Exception(string.Format("Stock Not Found for the Item:{0} !"));

                Stock.StockQty += tran.TranSide == TranSideEnum.Increase ? tran.QuantityInSmallerUnit * (-1) : tran.QuantityInSmallerUnit;//Reverse Stock
                context.Entry(Stock).State = EntityState.Modified;
            }

            context.StockTranItems.RemoveRange(prevTrans);
            context.StockTranItems.AddRange(Trans);

            // At last check Out of stock errors
            foreach (var tran in Trans)
            {
                Stock currntTranStock = context.Stocks.Find(tran.StockId);

                if (currntTranStock != null && currntTranStock.StockQty < 0)
                {
                    throw new Exception(string.Format("Not Enough Stock for the Item:{0} !"));

                }

                // RemoveGarbageStocks(oItem);
            }
            foreach (var tran in prevTrans)
            {
                Stock prevTranStock = context.Stocks.Find(tran.StockId);


                if (prevTranStock != null && prevTranStock.StockQty < 0)
                {
                    throw new Exception(string.Format("Not Enough Stock for the Item:{0} !"));

                }
                // RemoveGarbageStocks(oItem);
            }
        }

        public void Delete(TransactionTypeEnum tranType, int ParentTranId)
        {
            List<StockTranItem> trans = context.StockTranItems.Where(x => x.TransactionType == tranType && x.ParentTranID == ParentTranId).ToList();
            foreach (StockTranItem tran in trans)
            {
                Stock Stock = context.Stocks.Find(tran.StockId);

                if (Stock == null)
                    throw new Exception("Stock Not Found for the Item");

                Stock.StockQty += tran.TranSide == TranSideEnum.Increase ? tran.QuantityInSmallerUnit * (-1) : tran.QuantityInSmallerUnit;//Reverse Stock

                if (Stock.StockQty < 0)
                    throw new Exception("Not enough Stock!");

                context.Entry(Stock).State = EntityState.Modified;
            }
            context.StockTranItems.RemoveRange(trans);
        }

        public void DeleteStocksByItemID(int itemID)
        {
            var removableStocks = context.Stocks.Where(x => x.ItemId ==itemID);

            foreach (Stock item in removableStocks)
            {
                context.Stocks.Remove(item);
            }
        }

        // Unused Functions

        //private void RemoveGarbageStocks(Item oItem)
        //{
        //   var removableStocks = context.Stocks.Where(x => x.ItemId == oItem.Id && x.StockQty == 0 &&
        //                         (x.ConversionFactor != oItem.ConversionFactor || x.IsPriceInLargerUnit != oItem.IsPriceInLargerUnit ||
        //                          x.LargerUnitId != oItem.LargerUnitId || x.SmallerUnitId != oItem.SmallerUnitId ||
        //                          x.UnitPrice != oItem.UnitPrice));

        //   foreach (Stock item in removableStocks)
        //   {
        //       context.Stocks.Remove(item);
        //   }
        //}
        //public void Update(ICollection<StockTranItem> Trans)
        //{
        //    //Update New Stock
        //    foreach (var tran in Trans)
        //    {
        //        Item oItem = context.Items.Find(tran.ItemID);
        //        //oItem.StockQty += tran.TranSide == TranSideEnum.Increase ? tran.Quantity : tran.Quantity * (-1);
        //        context.Entry(oItem).State = EntityState.Modified;
        //    }

        //    StockTranItem firstTransaction = Trans.First();
        //    List<StockTranItem> prevTrans = context.StockTranItems.Where
        //                                            (
        //                                                x => x.TransactionType == firstTransaction.TransactionType
        //                                                && x.ParentTranID == firstTransaction.ParentTranID
        //                                            ).ToList();
        //   // Reverse prev trans
        //    foreach (var tran in prevTrans)
        //    {
        //        Item oItem = context.Items.Find(tran.ItemID);
        //        //oItem.StockQty += tran.TranSide == TranSideEnum.Increase ? tran.Quantity * (-1) : tran.Quantity;//Reverse Stock
        //        context.Entry(oItem).State = EntityState.Modified;
        //    }
        //    context.StockTranItems.RemoveRange(prevTrans);
        //    context.StockTranItems.AddRange(Trans);

        //    foreach (var tran in Trans)
        //    {
        //        Item oItem = context.Items.Find(tran.ItemID);
        //        //if (oItem.StockQty < 0)
        //        //    throw new Exception("Not Enough Stock!");
        //    }
        //    foreach (var tran in prevTrans)
        //    {
        //        Item oItem = context.Items.Find(tran.ItemID);
        //        //if (oItem.StockQty < 0)
        //        //    throw new Exception("Not Enough Stock!");
        //    }
        //}
    }
}
