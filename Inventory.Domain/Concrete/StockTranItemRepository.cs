﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain.Concrete
{

    internal class StockTranItemRepository : IStockTranItemRepository
    {
        private TMContext context = null;

        public StockTranItemRepository(TMContext context)
        {
            this.context = context;
        }
        
        public List<StockTranItem> GetStockTranItems(TransactionTypeEnum tranType, int ParentTranId)
        {
            List<StockTranItem> TranItems = null;

            TranItems = context.StockTranItems.Where(x => x.TransactionType == tranType && x.ParentTranID == ParentTranId).ToList();

            return TranItems;
        
        }
    }
}
