﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain.Concrete
{
    internal class UnitOfMeasureRepository : IUnitOfMeasureRepository
    {
        private TMContext context = null;

        public UnitOfMeasureRepository(TMContext context)
        {
            this.context = context;
        }
        

        public IQueryable<UnitOfMeasure> GetUnitOfMeasures()
        {
            return context.UnitOfMeasures;
        }

        public UnitOfMeasure GetUnitOfMeasureByID(int deptId)
        {
            return context.UnitOfMeasures.Find(deptId);
        }

        public void InsertUnitOfMeasure(UnitOfMeasure oUnitOfMeasure)
        {
            UnitOfMeasure existingItem = null;               

            existingItem = context.UnitOfMeasures.Where(x => x.Name.ToLower() == oUnitOfMeasure.Name.ToLower()).FirstOrDefault();
            if (existingItem!=null)
                throw new Exception("UOM with this name exists.");

            context.UnitOfMeasures.Add(oUnitOfMeasure);
        }

        public void DeleteUnitOfMeasure(int deptId)
        {
            UnitOfMeasure UnitOfMeasure = context.UnitOfMeasures.Find(deptId);
            context.UnitOfMeasures.Remove(UnitOfMeasure);
        }

        public void UpdateUnitOfMeasure(UnitOfMeasure oUnitOfMeasure)
        {
            UnitOfMeasure existingItem = null;

            existingItem = context.UnitOfMeasures.Where(x => x.Name.ToLower() == oUnitOfMeasure.Name.ToLower() && x.Id != oUnitOfMeasure.Id).FirstOrDefault();
            if (existingItem != null)
                throw new Exception("UOM with this name already exists.");

            
            context.Entry(oUnitOfMeasure).State = EntityState.Modified;
        }
    }
}
