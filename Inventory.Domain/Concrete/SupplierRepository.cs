﻿using System.Data.Entity;
using System.Linq;

namespace Inventory.Domain
{
    public class SupplierRepository : ISupplierRepository
    {
        private TMContext context = null;

        public SupplierRepository(TMContext context)
        {
            this.context = context;
        }
        

        public IQueryable<Supplier> GetSuppliers()
        {
            return context.Suppliers.OrderBy(x=>x.Name);
        }

        public Supplier GetSupplierByID(int supplierId)
        {
            return context.Suppliers.Find(supplierId);
        }

        public void InsertSupplier(Supplier oSupplier)
        {
            context.Suppliers.Add(oSupplier);
        }

        public void DeleteSupplier(int supplierId)
        {
            Supplier supplier = context.Suppliers.Find(supplierId);
            context.Suppliers.Remove(supplier);
        }

        public void UpdateSupplier(Supplier oSupplier)
        {
            context.Entry(oSupplier).State = EntityState.Modified;
        }
    }
}
