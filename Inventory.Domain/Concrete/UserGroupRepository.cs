﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
   public class UserGroupRepository: IUserGroupRepository
    {
        private TMContext context = new TMContext();
        public IQueryable<Group> GetUserGroup()
        {
            return context.Groups;
        }

        public Group GetUserGroupByID(int id)
        {
            return context.Groups.Find(id);
        }

        public void InsertUserGroup(Group UserGroup)
        {
            context.Groups.Add(UserGroup);
        }

        public void DeleteUserGroup(int UserGroupID)
        {
            Group UserGroup = context.Groups.Find(UserGroupID);
            context.Groups.Remove(UserGroup);
        }

        public void UpdateUserGroup(Group UserGroup)
        { 
            context.Entry(UserGroup).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
