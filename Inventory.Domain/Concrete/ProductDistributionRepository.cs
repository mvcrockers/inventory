﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
   public class ProductDistributionRepository: IProductDistributionRepository
    {
        private TMContext context = null;

        public ProductDistributionRepository(TMContext context)
        {
            this.context = context;
        }
        public IQueryable<ProductDistribution> GetProductDistributions()
        {
            return context.ProductDistributions;
        }

        public ProductDistribution GetProductDistributionByID(int ProductDistributionID)
        {
            return context.ProductDistributions.Find(ProductDistributionID);
        }

        public ProductDistribution InsertProductDistribution(ProductDistribution oProductDistribution)
        {
            context.ProductDistributions.Add(oProductDistribution);
            context.SaveChanges();
            return oProductDistribution;
        }


        public void DeleteProductDistribution(int ProductDistributionID)
        {
            ProductDistribution Odist = context.ProductDistributions.Find(ProductDistributionID);
            context.ProductDistributions.Remove(Odist);
        }

        public ProductDistribution UpdateProductDistribution(ProductDistribution oNewParent)
        {
            ProductDistribution OriginalParent = context.ProductDistributions.Where(x => x.ID == oNewParent.ID).Include(p => p.ProductDistributionItems).SingleOrDefault();
            if (OriginalParent != null)
            {
                if (context.ProductDistributions.Any(x => x.ID != oNewParent.ID && x.InvoiceNo.Trim().ToLower() == oNewParent.InvoiceNo.ToLower()))
                {
                    throw new Exception("There is another Receipt with this 'Receipt No'.Please try with another one.");
                }

                var parentEntry = context.Entry(OriginalParent);
                parentEntry.CurrentValues.SetValues(oNewParent);


                foreach (var childItem in oNewParent.ProductDistributionItems)
                {
                    var originalChildItem = OriginalParent.ProductDistributionItems.SingleOrDefault(c => c.ID == childItem.ID && c.ID != 0);
                    // Is original child item with same ID in DB?
                    if (originalChildItem != null)
                    {
                        // Yes -> Update scalar properties of child item
                        var childEntry = context.Entry(originalChildItem);
                        childEntry.CurrentValues.SetValues(childItem);
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert
                        childItem.ID = 0;
                        OriginalParent.ProductDistributionItems.Add(childItem);
                    }
                }

                foreach (var originalChildItem in OriginalParent.ProductDistributionItems.Where(c => c.ID != 0).ToList())
                {
                    // Are there child items in the DB which are NOT in the
                    // new child item collection anymore?
                    if (!oNewParent.ProductDistributionItems.Any(c => c.ID == originalChildItem.ID))
                        // Yes -> It's a deleted child item -> Delete
                        context.ProductDistributionItems.Remove(originalChildItem);
                }


                context.SaveChanges();


            }
            else
            {
                throw new Exception("This Receipt doesn't belong to this database.");
            }

            return OriginalParent;
        }
    }
}
