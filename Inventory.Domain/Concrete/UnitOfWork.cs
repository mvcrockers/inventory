﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventory.Domain.Concrete;
using System.Data.Entity;

namespace Inventory.Domain
{
    public class UnitOfWork : IUnitOfWork
    {
        private TMContext _context = null;
        private DbContextTransaction contextTransaction = null;

        private bool _disposed = false;
        private IUserRepository _userRepository;
        private IRoleRepository _roleRepository;
        private IUnitRepository _unitRepository;
        private IDepartmentRepository _departmentRepository;
        private IItemCategoryRepository _itemCategoryRepository;
        private IUnitOfMeasureRepository _unitOfMeasureRepository;
        private IItemRepository _itemRepository;
        private ISupplierRepository _supplierRepository;
        private IPurchaseOrderRepository _purchaseOrderRepository;
        private IStockRepository _stockRepository;
        private IPurchaseReceiveRepository _purchaseReceiveRepository;
        private IProductDistributionRepository _ProductDistributionRepository;
        public UnitOfWork()
        {
            _context = new TMContext();
        }

        public UnitOfWork(TMContext context)
        {
            this._context = context;
        }

        public IProductDistributionRepository ProductDistributionRepository
        {
            get
            {
                if (this._ProductDistributionRepository == null)
                {
                    this._ProductDistributionRepository = new ProductDistributionRepository(_context);
                }
                return _ProductDistributionRepository;
            }
        }

        public IStockRepository StockRepository
        {
            get
            {
                if (this._stockRepository == null)
                {
                    this._stockRepository = new StockRepository(_context);
                }
                return _stockRepository;
            }
        }
        public IItemRepository ItemRepository
        {
            get
            {
                if (this._itemRepository == null)
                {
                    this._itemRepository = new ItemRepository(_context);
                }
                return _itemRepository;
            }
        }
        public IUnitOfMeasureRepository UnitOfMeasureRepository
        {
            get
            {
                if (this._unitOfMeasureRepository == null)
                {
                    this._unitOfMeasureRepository = new UnitOfMeasureRepository(_context);
                }
                return _unitOfMeasureRepository;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (this._userRepository == null)
                {
                    this._userRepository = new UserRepository(_context);
                }
                return _userRepository;
            }
        }

        public IItemCategoryRepository ItemCategoryRepository
        {
            get
            {
                if (this._itemCategoryRepository == null)
                {
                    this._itemCategoryRepository = new ItemCategoryRepository(_context);
                }
                return _itemCategoryRepository;
            }
        }

        public IRoleRepository RoleRepository
        {
            get
            {
                if (this._roleRepository == null)
                {
                    this._roleRepository = new RoleRepository(_context);
                }
                return _roleRepository;
            }
        }

        public IUnitRepository UnitRepository
        {
            get
            {
                if (this._unitRepository == null)
                {
                    this._unitRepository = new UnitRepository(_context);
                }
                return _unitRepository;
            }
        }

        public IDepartmentRepository DepartmentRepository
        {
            get
            {
                if (this._departmentRepository == null)
                {
                    this._departmentRepository = new DepartmentRepository(_context);
                }
                return _departmentRepository;
            }
        }

        public ISupplierRepository SupplierRepository
        {
            get
            {
                if (this._supplierRepository == null)
                {
                    this._supplierRepository = new SupplierRepository(_context);
                }
                return _supplierRepository;
            }
        }

        public IPurchaseOrderRepository PurchaseOrderRepository
        {
            get
            {
                if (this._purchaseOrderRepository == null)
                {
                    this._purchaseOrderRepository = new PurchaseOrderRepository(_context);
                }
                return _purchaseOrderRepository;
            }
        }

        public IPurchaseReceiveRepository PurchaseReceiveRepository
        {
            get
            {
                if (this._purchaseReceiveRepository == null)
                {
                    this._purchaseReceiveRepository = new PurchaseReceiveRepository(_context);
                }
                return _purchaseReceiveRepository;
            }
        }

       
        public void Save()
        {
            try
            {  
                _context.SaveChanges();
                
            }
            catch (Exception e)
            {
                if (contextTransaction != null)
                {
                    contextTransaction.Rollback();
                    contextTransaction = null;
                }
                throw e;
            }
            
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                   
                }
            }
            this._disposed = true;
        }


        public void BeginTransaction()
        {
            contextTransaction = _context.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            if (contextTransaction != null)
            {
                contextTransaction.Commit();
                contextTransaction = null;
             
            }
        }

        public void RollBackTransaction()
        {
            if (contextTransaction != null)
            {
                contextTransaction.Rollback();
                contextTransaction = null;
            }
        }
    }
}
