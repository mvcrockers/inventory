﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain.Concrete
{
    internal class ItemRepository : IItemRepository
    {
        private TMContext context = null;

        public ItemRepository(TMContext context)
        {
            this.context = context;
        }


        public IQueryable<Item> GetItems(string searchString)
        {
            if(string.IsNullOrEmpty(searchString))
                return context.Items.OrderBy(x=>x.Name);
            else 
                return context.Items.Where(x=>x.Name.StartsWith(searchString));
        }

        public Item GetItemByID(int deptId)
        {
            return context.Items.Find(deptId);
        }

        public Item InsertItem(Item oItem)
        {
            Item existingItem = null;
                        
               existingItem=context.Items.Where(x => x.Code.ToLower() == oItem.Code.ToLower()).FirstOrDefault();
            if(existingItem!=null)
               throw new Exception("Item with this code exists.") ;

            existingItem = context.Items.Where(x => x.Name.ToLower() == oItem.Name.ToLower()).FirstOrDefault();
            if (existingItem!=null)
                throw new Exception("Item with this name exists.");

            context.Items.Add(oItem);
            context.SaveChanges();
            return oItem;
        }

        public void DeleteItem(int deptId)
        {
            Item Item = context.Items.Find(deptId);
            context.Items.Remove(Item);
        }

        public Item UpdateItem(Item oItem)
        {
            Item existingItem = null;

            existingItem = context.Items.Where(x => (x.Code.ToLower() == oItem.Code.ToLower() && x.Id!=oItem.Id)).FirstOrDefault();
            if (existingItem != null)
                throw new Exception("Item with this code already exists.");

            existingItem = context.Items.Where(x => x.Name.ToLower() == oItem.Name.ToLower() && x.Id != oItem.Id).FirstOrDefault();
            if (existingItem != null)
                throw new Exception("Item with this name already exists.");

            
            context.Entry(oItem).State = EntityState.Modified;

            context.SaveChanges();

            return oItem;
        }
    }
}
