﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
   public class UserRepository: IUserRepository
    {
        private TMContext context = null;

        public UserRepository(TMContext context)
        {
            this.context = context;
        }

        public IQueryable<User> GetUsers()
        {
            return context.Users;
        }

        public User GetUserByID(int UserId)
        {
            return context.Users.Find(UserId);
        }

        public void InsertUser(User insertedUser)
        {
            if(context.Users.Where(Usr => Usr.Username.Trim().ToLower() == insertedUser.Username.Trim().ToLower()).Any())
            {
                throw new Exception("There is another user with this 'User Name'.Please try with another one.");
            }

            if(context.Users.Where(Usr => Usr.Email.Trim().ToLower() == insertedUser.Email.Trim().ToLower()).Any())
            {
                throw new Exception("There is another user with this 'Email'.Please try with another one.");
            }

            if(string.IsNullOrEmpty(insertedUser.Password))
            {
                throw new Exception("Please Enter a Password.");
            }

            string hashedPassword = Crypto.HashPassword(insertedUser.Password.Trim());
            if(hashedPassword.Length > 128)
            {
                throw new Exception("Invalid Password.Please try with another one.");
            }

            insertedUser.Password = hashedPassword;

            if(insertedUser.Roles.Count > 0)
            {
                Role selectedRole = insertedUser.Roles.First();
                Role oRole = context.Roles.FirstOrDefault(x => x.RoleId == selectedRole.RoleId);
                if(oRole != null)
                {
                    insertedUser.Roles.Clear();
                    insertedUser.Roles.Add(oRole);
                }

            }

            context.Users.Add(insertedUser);

        }

        public void DeleteUser(int UserId)
        {
            User User = context.Users.Find(UserId);
            context.Users.Remove(User);
        }

        public void UpdateUser(User updatedUser)
        {
            
            User oUser = context.Users.Where(x => x.UserId==updatedUser.UserId).SingleOrDefault();
            if(oUser!=null)
            {
                if(context.Users.Where(Usr => Usr.Username.Trim().ToLower() == updatedUser.Username.Trim().ToLower() && updatedUser.UserId!= Usr.UserId).Any())
                {
                    throw new Exception("There is another user with this 'User Name'.Please try with another one.");
                }

                if(context.Users.Where(Usr => Usr.Email.Trim().ToLower() == updatedUser.Email.Trim().ToLower() && updatedUser.UserId != Usr.UserId).Any())
                {
                    throw new Exception("There is another user with this 'Email'.Please try with another one.");
                }

                oUser.UserId = updatedUser.UserId;
                oUser.Username = updatedUser.Username;
                oUser.FirstName = updatedUser.FirstName;
                oUser.LastName = updatedUser.LastName;
                oUser.Email = updatedUser.Email;
                if(!string.IsNullOrEmpty(updatedUser.Password))
                {
                    string hashedPassword = Crypto.HashPassword(updatedUser.Password.Trim());
                    if(hashedPassword.Length > 128)
                    {
                        throw new Exception("Invalid Password.Please try with another one.");
                    }
                    oUser.Password = hashedPassword;
                }

                if(updatedUser.Roles.Count > 0)
                {
                    Role selectedRole = updatedUser.Roles.First();
                    Role oRole = context.Roles.FirstOrDefault(x => x.RoleId == selectedRole.RoleId);
                    if(oRole!=null)
                    {
                        oUser.Roles.Clear();
                        oUser.Roles.Add(oRole);
                    }
                    
                }


            }
            //context.Entry(User).State = EntityState.Modified;
        }

        
    }
}
