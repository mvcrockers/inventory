﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain.Concrete
{
    public class DepartmentRepository : IDepartmentRepository
    {
        private TMContext context = null;

        public DepartmentRepository(TMContext context)
        {
            this.context = context;
        }
        

        public IQueryable<Department> GetDepartments()
        {
            return context.Departments;
        }

        public Department GetDepartmentByID(int deptId)
        {
            return context.Departments.Find(deptId);
        }

        public void InsertDepartment(Department oDepartment)
        {
            context.Departments.Add(oDepartment);
        }

        public void DeleteDepartment(int deptId)
        {
            Department department = context.Departments.Find(deptId);
            context.Departments.Remove(department);
        }

        public void UpdateDepartment(Department oDepartment)
        {
            context.Entry(oDepartment).State = EntityState.Modified;
        }
    }
}
