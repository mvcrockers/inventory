﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain.Concrete
{
    internal class ItemCategoryRepository : IItemCategoryRepository
    {
        private TMContext context = null;

        public ItemCategoryRepository(TMContext context)
        {
            this.context = context;
        }
        

        public IQueryable<ItemCategory> GetItemCategories()
        {
            return context.ItemCategories;
        }

        public ItemCategory GetItemCategoryByID(int deptId)
        {
            return context.ItemCategories.Find(deptId);
        }

        public void InsertItemCategory(ItemCategory oItemCategory)
        {
            ItemCategory existingItem = null;
                        
               existingItem=context.ItemCategories.Where(x => x.Code.ToLower() == oItemCategory.Code.ToLower()).FirstOrDefault();
            if(existingItem!=null)
               throw new Exception("Category with this code exists.") ;

            existingItem = context.ItemCategories.Where(x => x.Name.ToLower() == oItemCategory.Name.ToLower()).FirstOrDefault();
            if (existingItem!=null)
                throw new Exception("Category with this name exists.");

            context.ItemCategories.Add(oItemCategory);
        }

        public void DeleteItemCategory(int deptId)
        {
            ItemCategory ItemCategory = context.ItemCategories.Find(deptId);
            context.ItemCategories.Remove(ItemCategory);
        }

        public void UpdateItemCategory(ItemCategory oItemCategory)
        {
            ItemCategory existingItem = null;

            existingItem = context.ItemCategories.Where(x => (x.Code.ToLower() == oItemCategory.Code.ToLower() && x.Id!=oItemCategory.Id)).FirstOrDefault();
            if (existingItem != null)
                throw new Exception("Category with this code already exists.");

            existingItem = context.ItemCategories.Where(x => x.Name.ToLower() == oItemCategory.Name.ToLower() && x.Id != oItemCategory.Id).FirstOrDefault();
            if (existingItem != null)
                throw new Exception("Category with this name already exists.");

            
            context.Entry(oItemCategory).State = EntityState.Modified;
        }
    }
}
