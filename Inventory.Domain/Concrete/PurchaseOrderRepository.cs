﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
   public class PurchaseOrderRepository: IPurchaseOrderRepository
    {
        private TMContext context = null;

        public PurchaseOrderRepository(TMContext context)
        {
            this.context = context;
        }

        public IQueryable<PurchaseOrder> GetPurchaseOrders()
        {
            return context.PurchaseOrders;
        }

        public PurchaseOrder GetPurchaseOrderByID(long purchaseOrderID)
        {
            return context.PurchaseOrders.Find(purchaseOrderID);
        }

        public void InsertPurchaseOrder(PurchaseOrder oPurchaseOrder)
        {
            context.PurchaseOrders.Add(oPurchaseOrder);
        }

        public void DeletePurchaseOrder(long purchaseOrderID)
        {
            PurchaseOrder oPurchaseOrder = context.PurchaseOrders.Find(purchaseOrderID);
            context.PurchaseOrders.Remove(oPurchaseOrder);
        }

        public void UpdatePurchaseOrder(PurchaseOrder oNewParent)
        {
            //context.Entry(oNewParent).State = EntityState.Modified;

            PurchaseOrder OriginalParent = context.PurchaseOrders.Where(x => x.ID == oNewParent.ID).Include(p => p.PurchaseOrderItems).SingleOrDefault();
            if (OriginalParent != null)
            {
                if (context.PurchaseOrders.Any(x => x.ID != oNewParent.ID && x.OrderCode.Trim().ToLower() == oNewParent.OrderCode.Trim().ToLower()))
                {
                    throw new Exception("There is another Order with this 'Order Code'.Please try with another one.");
                }

                var parentEntry = context.Entry(OriginalParent);
                parentEntry.CurrentValues.SetValues(oNewParent);


                foreach (var childItem in oNewParent.PurchaseOrderItems)
                {
                    var originalChildItem = OriginalParent.PurchaseOrderItems.SingleOrDefault(c => c.ID == childItem.ID && c.ID != 0);
                    // Is original child item with same ID in DB?
                    if (originalChildItem != null)
                    {
                        // Yes -> Update scalar properties of child item
                        var childEntry = context.Entry(originalChildItem);
                        childEntry.CurrentValues.SetValues(childItem);
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert
                        childItem.ID = 0;
                        OriginalParent.PurchaseOrderItems.Add(childItem);
                    }
                }

                foreach (var originalChildItem in OriginalParent.PurchaseOrderItems.Where(c => c.ID != 0).ToList())
                {
                    // Are there child items in the DB which are NOT in the
                    // new child item collection anymore?
                    if (!oNewParent.PurchaseOrderItems.Any(c => c.ID == originalChildItem.ID))
                        // Yes -> It's a deleted child item -> Delete
                        context.PurchaseOrderItems.Remove(originalChildItem);
                }

                context.SaveChanges();
            }
            else
            {
                throw new Exception("This Order doesn't belong to this database.");
            }
        }

        public IQueryable<PurchaseOrderItem> GetPurchaseOrderItems(long purchaseOrderID)
        {
            return context.PurchaseOrderItems.Where(x => x.PurchaseOrderID == purchaseOrderID);
        }

        public PurchaseOrderItem GetPurchaseOrderItemByID(long purchaseOrderItemID)
        {
            return context.PurchaseOrderItems.Find(purchaseOrderItemID);
        }

        public void InsertPurchaseOrderItem(PurchaseOrderItem oPurchaseOrderItem)
        {
            context.PurchaseOrderItems.Add(oPurchaseOrderItem);
        }

        public void DeletePurchaseOrderItem(long purchaseOrderItemID)
        {
            PurchaseOrderItem oPurchaseOrderItem = context.PurchaseOrderItems.Find(purchaseOrderItemID);
            context.PurchaseOrderItems.Remove(oPurchaseOrderItem);
        }

        public void UpdatePurchaseOrderItem(PurchaseOrderItem oPurchaseOrderItem)
        {
            context.Entry(oPurchaseOrderItem).State = EntityState.Modified;
        }
    }
}
