﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public class EFUserTypeRepository : IUserTypeRepository
    {
        private TMContext context = new TMContext();
        public IQueryable<UserType> UserTypes
        {
            get { return context.UserTypes; }
        }

        public UserType GetUserTypeByID(int id)
        {
            return context.UserTypes.Find(id);
        }

        public void InsertUserType(UserType UserType)
        {
            context.UserTypes.Add(UserType);
        }

        public void DeleteUserType(int UserTypeID)
        {
            UserType UserType = context.UserTypes.Find(UserTypeID);
            context.UserTypes.Remove(UserType);
        }

        public void UpdateUserType(UserType UserType)
        {
            context.Entry(UserType).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public IQueryable<UserType> GetUserType()
        {
            return context.UserTypes.ToList().AsQueryable();
        }
    }
}
