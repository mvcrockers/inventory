﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{

    public enum RptRenderType
    {
        Pdf = 1,
        Excel = 2
    }

    public enum RptType
    {
        UnitWise=1,
        ItemWise=2
    }
    public enum EnumItemType
    {
        Spendable =1,
        Non_Spendable =2
    }
    public enum TranSideEnum : short
    {
        Increase = 1,
        Decrease = 2
    }
   
    public enum TransactionTypeEnum : short
    {
        PurchaseViaOrder = 1,
        DirectPurchase=2,
        ReceiveWithoutPurchase=3,
        Distrution=4
    }

    public enum ReceivedStockTypeEnum : short
    {
        Direct_Purchase = 1,
        Without_Purchase =2
    }
}
