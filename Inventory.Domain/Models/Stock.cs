﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public class Stock
    {
        public Stock()
        {
        }

        public int Id { get; set; }
        public int ItemId { set; get; }

        public int SmallerUnitId
        {
            get;
            set;
        }
        public int? LargerUnitId
        {
            get;
            set;
        }

        public decimal? ConversionFactor
        {
            get;
            set;
        }

        public bool IsPriceInLargerUnit
        {
            get;
            set;
        }

        public decimal UnitPrice
        {
            get;
            set;
        }

        public decimal StockQty { get; set; }
        
        public Item Item { get; set; }

        public virtual UnitOfMeasure LargerUnit
        {
            get;
            set;
        }

        public virtual UnitOfMeasure SmallerUnit
        {
            get;
            set;
        }
    }
}
