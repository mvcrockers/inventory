﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public class User
    {

        public virtual int UserId
        {
            get;
            set;
        }

        [Required]
        [Display(Name = "User Id")]
        public virtual String Username
        {
            get;
            set;
        }

        [Required]
        public virtual String Email
        {
            get;
            set;
        }

        [Required, DataType(DataType.Password)]
        public virtual String Password
        {
            get;
            set;
        }

        [Display(Name = "First Name")]
        public virtual String FirstName
        {
            get;
            set;
        }
        [Display(Name = "Last Name")]
        public virtual String LastName
        {
            get;
            set;
        }



        [DataType(DataType.MultilineText)]
        public virtual String Comment
        {
            get;
            set;
        }


        public virtual Boolean IsApproved
        {
            get;
            set;
        }
        public virtual int PasswordFailuresSinceLastSuccess
        {
            get;
            set;
        }
        public virtual DateTime? LastPasswordFailureDate
        {
            get;
            set;
        }
        public virtual DateTime? LastActivityDate
        {
            get;
            set;
        }
        public virtual DateTime? LastLockoutDate
        {
            get;
            set;
        }
        public virtual DateTime? LastLoginDate
        {
            get;
            set;
        }
        public virtual String ConfirmationToken
        {
            get;
            set;
        }
        public virtual DateTime? CreateDate
        {
            get;
            set;
        }
        public virtual Boolean IsLockedOut
        {
            get;
            set;
        }
        public virtual DateTime? LastPasswordChangedDate
        {
            get;
            set;
        }
        public virtual String PasswordVerificationToken
        {
            get;
            set;
        }
        public virtual DateTime? PasswordVerificationTokenExpirationDate
        {
            get;
            set;
        }

        public virtual ICollection<Role> Roles
        {
            get;
            set;
        }

    }
}
