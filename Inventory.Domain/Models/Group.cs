namespace Inventory.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Group
    {
        public Group()
        {
            Permissions = new HashSet<Permission>();
            checkedFiles = new string[0];
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupID { get; set; }

        [Required]
        [StringLength(100)]
        public string GroupName { get; set; }

        public short IsActive { get; set; }

        public int? InitTransferLogId { get; set; }

        public int? TransferLogId { get; set; }

        public int CreatedBy { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? UserTypeID { get; set; }

        public virtual ICollection<Permission> Permissions { get; set; }

        public string[] checkedFiles { get; set ; }

        public virtual UserType UserType {get; set;}
    }
}
