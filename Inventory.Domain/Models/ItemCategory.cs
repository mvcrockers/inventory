﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
   public class ItemCategory
    {
        public int Id
        {
            get;
            set;
        }

        [Required]
        [StringLength(50)]
        public String Code
        {
            get;
            set;
        }

        [Required]
        [StringLength(100)]
        public String Name
        {
            get;
            set;
        }

    }
}
