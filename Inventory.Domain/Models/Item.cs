﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
   public class Item
    {
        public int Id
        {
            get;
            set;
        }

        //[Required]
        [StringLength(50)]
        public String Code
        {
            get;
            set;
        }

        [Required]
        [StringLength(100)]
        public String Name
        {
            get;
            set;
        }
        [Required]
        [Display(Name = "Item Type")]
        public EnumItemType ItemType
        {
            get;
            set;
        }

       [Display(Name = "Unit Price")]
        public decimal UnitPrice
        {
            get;
            set;
        }

       [Display(Name = "Price Change Date")]
        public DateTime? PriceChangeDate
        {
            get;
            set;
        }

       
       [Display(Name = "Conversion Factor")]
        public decimal? ConversionFactor
        {
            get;
            set;
        }

       [Display(Name = "Larger Unit")]
        public int? LargerUnitId
        {
            get;
            set;
        }

       [Display(Name= "Smaller Unit")]
       [Required] 
       public int SmallerUnitId
        {
            get;
            set;
        }

       [Display(Name = "Category")]
        public int? ItemCategoryId
        {
            get;
            set;
        }

       public bool IsPriceInLargerUnit
       {
           get;
           set;
       }
        public virtual ItemCategory ItemCategory
        {
            get;
            set;
        }

        public virtual UnitOfMeasure LargerUnit
        {
            get;
            set;
        }

        public virtual UnitOfMeasure SmallerUnit
        {
            get;
            set;
        }

       [NotMapped]
        public bool KeepPrevStock { get; set; }

    }
}
