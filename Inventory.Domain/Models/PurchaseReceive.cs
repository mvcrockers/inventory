﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public class PurchaseReceive
    {
        public long Id { get; set; }

        [Display(Name="Purchase No")]
        public string PurchaseNo { get; set; }
        [Display(Name = "Receive as")]
        public ReceivedStockTypeEnum ReceivedStockType { get; set; }

        public int SupplierId { get; set; }

        public long? PurchaseOrderID { get; set; }

        public string PurchaseOrderNo { get; set; }
        [Display(Name="Receive Date")]
        public DateTime ReceiveDate { get; set; }

        public PurchaseOrder PurchaseOrder { get; set; }
        public Supplier Supplier { get; set; }
        public string Remarks { get; set; }
        public virtual ICollection<PurchaseReceiveItem> PurchaseReceiveItems { get; set; }
        [NotMapped]
        public decimal TotalAmount { get; set; }
        [NotMapped]
        public bool IsSelected { get; set; }
    }

    public class PurchaseReceiveItem
    {        
        public long Id { get; set; }
        public long PurchaseReceiveId { get; set; }
        public int SupplierId { get; set; }
        public int StockID { get; set; }
        public int UnitOfMeasureId { get; set; }
        public long ItemQty { get; set; }
        public decimal ConversionFactor { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Price { get; set; }
        public virtual PurchaseReceive PurchaseReceive { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual UnitOfMeasure UnitOfMeasure { get; set; }
        public virtual Stock Stock { get; set; }
        [NotMapped]
        public int index { get; set; }
        [NotMapped]
        public string ItemName { get; set; }
        [NotMapped]
        public string UnitName { get; set; }
    }
}
