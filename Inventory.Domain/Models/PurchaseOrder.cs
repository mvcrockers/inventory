﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public class PurchaseOrder
    {
        [Key]
        public long ID { get; set; }

        [Display(Name = "Order Code")]
        public string OrderCode { get; set; }

        [Display(Name = "Order Date")]
        public DateTime OrderDate { get; set; }

        [Display(Name = "Received Date")]
        public DateTime? ReceivedDate { get; set; }

        public Boolean IsReceived { get; set; }

        public virtual ICollection<PurchaseOrderItem> PurchaseOrderItems { get; set; }
    }

    public class PurchaseOrderItem
    {
        [Key]
        public long ID { get; set; }

        public long PurchaseOrderID { get; set; }

        public int SupplierID { get; set; }

        public int ItemID { get; set; }

        public int ItemQty { get; set; }

        public int UnitOfMeasureID { get; set; }

        public decimal ConversionFactor { get; set; }

        [ForeignKey("PurchaseOrderID")]
        public virtual PurchaseOrder PurchaseOrder { get; set; }

        [ForeignKey("SupplierID")]
        public virtual Supplier Supplier { get; set; }

        [ForeignKey("ItemID")]
        public virtual Item Item { get; set; }

        [ForeignKey("UnitOfMeasureID")]
        public virtual UnitOfMeasure UnitOfMeasure { get; set; }
    }
}