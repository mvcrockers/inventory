﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public class Parent
    {
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Comments { get; set; }
        public IEnumerable<ChildItems> Children { get; set; }

        public Parent()
        {
            Children = new List<ChildItems>() { new ChildItems() { Id = 1, ChildField1 = "b", ChildField2 = "bb" }, new ChildItems() { Id = 2, ChildField1 = "a", ChildField2 = "aa" }, new ChildItems() { Id = 3, ChildField1 = "c", ChildField2 = "cc" } };
        }
    }

    public class ChildItems
    {
        public int Id { get; set; }
        public string ChildField1 { get; set; }
        public string ChildField2 { get; set; }
       

    }
}
