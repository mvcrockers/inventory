﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public class StockTranItem
    {
        public StockTranItem()
        {
        }

        public int ID { get; set; }
        public int TranID { set; get; }
        public int ParentTranID { set; get; }
        public int StockId { set; get; }
        public string ParentTranNo { get; set; }
        public TransactionTypeEnum TransactionType { get; set; }
        public DateTime Trandate { set; get; }
        public decimal QuantityInSmallerUnit { set; get; }
        public TranSideEnum TranSide { set; get; }    
        public Stock Stock { get; set; }
    }
}
