﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public class Menu
    {
        public virtual int MenuId { get; set; }

        [Required]
        public virtual string MenuName { get; set; }

        [Required]
        public virtual String URI { get; set; }

        public virtual int RoleId { get; set; }

        public virtual int? ParentMenuId { get; set; }

        public Menu ParentMenu { get; set; }

        public virtual ICollection<Menu> ChildMenuItems { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}
