﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Inventory.Domain
{
    public class Supplier
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please provide a Supplier Name")]
        [StringLength(100)]
        [Display(Name = "Supplier")]
        
        public string Name { get; set; }

        public string Address { get; set; }
        [Required]
        public string MobileNo { get; set; }
    }
}
