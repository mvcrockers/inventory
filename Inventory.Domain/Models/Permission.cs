namespace Inventory.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Permission
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionID { get; set; }

        public int GroupID { get; set; }

        [Required]
        [StringLength(200)]
        public string PermissionKey { get; set; }

        public virtual Group Group { get; set; }
    }
}
