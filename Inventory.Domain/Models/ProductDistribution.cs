﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public class ProductDistribution
    {
        public  ProductDistribution()
        {
            ProductDistributionItems = new List<ProductDistributionItem>();
        }
        public int ID { get; set; }
        [Display(Name="Invoice No")]
        public string InvoiceNo { get; set; }
        [Display(Name = "Transaction Date")]
        [Required]
        public DateTime TranDate { get; set; }
        public string Remarks { get; set; }
        public virtual ICollection<ProductDistributionItem> ProductDistributionItems { get; set; }
        [NotMapped]
        public decimal TotalAmount{get; set;}
    }

    public class ProductDistributionItem
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Select an Unit")]
        
        [Display(Name="Unit")]
        
        public int UnitID { get; set; }

        [Display(Name = "Department")]
        public int? DepartmentID { get; set; }

        [Display(Name = "Item")]
        [Required(ErrorMessage = "Select an Item")]
        public int ItemId { get; set; }

        public int UnitOfMeasureID { get; set; }

        [Required(ErrorMessage = "Please provide quantity")]
        public decimal Quantity { get; set; }
        [Required(ErrorMessage = "Enter Price")]
        public int ProductDistributionId { get; set; }
        [ForeignKey("UnitID")]
        public virtual Unit Unit { get; set; }
         [ForeignKey("DepartmentID")]
        public virtual Department Department { get; set; }
        public virtual Item Item { get; set; }
        [ForeignKey("UnitOfMeasureID")]
        public virtual UnitOfMeasure UnitOfMeasure { get; set; }
        public virtual ProductDistribution ProductDistribution { get; set; }
        public int StockID { get; set; }
        public virtual Stock Stock { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }

        #region Not Mapped

        [NotMapped]
        public int index { get;set;}

        [NotMapped]
        public string UnitName { get; set; }

        [NotMapped]
        public string DeptName { get; set; }

        [NotMapped]
        public string ItemName { get; set; }

        [NotMapped]
        public string UnitMessureName { get; set; }
        
        public decimal ConversionFactor { get; set; }
        #endregion

    }
}
