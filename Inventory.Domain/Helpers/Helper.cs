﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Domain
{
    public class Helper
    {
        /// <summary>
        /// Returns Price in UOM to the corresponding "givenUOMID" Parameter.
        /// </summary>
        /// <param name="givenUomID">ID of the UOM in which the price to return.</param>
        /// <param name="stock">Selected Stock of the Product.</param>
        public static decimal CalculateUnitPrice(int givenUomID, Stock stock)
        {
            decimal unitPrice = 0;

            if (stock.IsPriceInLargerUnit)
                unitPrice = givenUomID == stock.LargerUnitId ? stock.UnitPrice : stock.UnitPrice / stock.ConversionFactor.Value;
            else
                unitPrice = givenUomID == stock.SmallerUnitId ? stock.UnitPrice : stock.UnitPrice * stock.ConversionFactor.Value;

            return unitPrice;
        }
    }
}
