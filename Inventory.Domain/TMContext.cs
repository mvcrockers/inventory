namespace Inventory.Domain
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Inventory.Domain.Migrations;

    public partial class TMContext : DbContext
    {
        public TMContext()
            : base("name=TMContext")
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<
            //TMContext, Configuration>());
        }

        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Menu> Menus { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<ItemCategory> ItemCategories { get; set; }
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<UnitOfMeasure> UnitOfMeasures { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public virtual DbSet<PurchaseOrderItem> PurchaseOrderItems { get; set; }
        public virtual DbSet<ProductDistribution> ProductDistributions { get; set; }
        public virtual DbSet<ProductDistributionItem> ProductDistributionItems { get; set; }

        public virtual DbSet<StockTranItem> StockTranItems { get; set; }

        public virtual DbSet<PurchaseReceive> PurchaseReceives { get; set; }

        public virtual DbSet<PurchaseReceiveItem> PurchaseReceiveItems { get; set; }

        public virtual DbSet<Stock> Stocks { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Group>()
                .Property(e => e.GroupName)
                .IsUnicode(false);

            modelBuilder.Entity<Group>()
                .HasMany(e => e.Permissions)
                .WithRequired(e => e.Group)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Permission>()
                .Property(e => e.PermissionKey)
                .IsUnicode(false);

            modelBuilder.Entity<UserType>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<UserType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Menu>()
                .HasMany(r => r.Roles)
                .WithMany(m => m.Menus)
                .Map(c =>
                {
                    c.MapLeftKey("MenuId");
                    c.MapRightKey("RoleId");
                    c.ToTable("MenuRoles");
                });

            modelBuilder.Entity<Menu>()
                .HasOptional(e => e.ParentMenu)
                .WithMany(r => r.ChildMenuItems)
                .HasForeignKey(p => p.ParentMenuId);

            modelBuilder.Entity<Unit>()
                .HasMany(c => c.Departments)
                .WithMany(i => i.Units)
                .Map(t => 
                    {   
                        t.MapLeftKey("Unit_Id");
                        t.MapRightKey("Department_Id");
                        t.ToTable("UnitDepartments");
                    });
            //modelBuilder.Entity<PurchaseOrderItem>()
            //    .HasRequired(p => p.PurchaseOrder)
            //    .WithMany(pi => pi.PurchaseOrderItems)
            //    .HasForeignKey(p => p.PurchaseOrderID);
        }
    }
}
